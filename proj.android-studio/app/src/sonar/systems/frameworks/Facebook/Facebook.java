package sonar.systems.frameworks.Facebook;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import sonar.systems.frameworks.BaseClass.Framework;
import android.app.Activity;
import android.net.Uri;


public class Facebook extends Framework
{

	private Activity activity;

	public Facebook()
	{

	}

	@Override
	public void SetActivity(Activity activity)
	{
		this.activity = activity;
	}

	@Override
	public void onStart()
	{

	}

	@Override
	public void onStop()
	{

	}
	
	@Override
	public void Share(String name, String link, String description, String caption, String imagePath)
	{
		ShareLinkContent content = new ShareLinkContent.Builder()
				.setContentUrl(Uri.parse(link))
				.setContentDescription(description)
				.setContentTitle(description)
				.build();
		ShareDialog.show(this.activity, content);
	}
}
