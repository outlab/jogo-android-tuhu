/*
 * MusicInfo.h
 *
 *  Created on: 2015. 12. 13.
 *      Author: Jimmy
 */

#ifndef MUSICINFO_H_
#define MUSICINFO_H_

#include "common.h"

#define MUSIC1_COUNT 		62
#define MUSIC2_COUNT 		48
#define MUSIC3_COUNT 		39


class MusicInfo {
public:
	float _music1[MUSIC1_COUNT * 2];
	float _music2[MUSIC2_COUNT * 2];
	float _music3[MUSIC3_COUNT * 2];

public:
	MusicInfo();
	virtual ~MusicInfo();

public:

	void init();
};

#endif /* MUSICINFO_H_ */
