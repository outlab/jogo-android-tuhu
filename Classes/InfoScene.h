/*
 * InfoScene.h
 *
 *  Created on: 2015. 12. 16.
 *      Author: Jimmy
 */

#ifndef INFOSCENE_H_
#define INFOSCENE_H_

#include "common.h"

class InfoScene : public Layer {
private:
    Size _screenSize;
    float _margin;

    Sprite* background;
    Sprite* texto2;
    Sprite* texto1;
    
    Size visibleSize;
    Vec2 origin;
    Vec2 _ptStart;
    Vec2 _ptTouch;
    
    bool _isButtonMoved;

    void backMenu(Ref* pSender);
    void showSobre(Ref* pSender);
    void showTecnica(Ref* pSender);
    void esteApp(Ref* pSender);

public:
	InfoScene();
	virtual ~InfoScene();

public:
	virtual bool init();

	static Scene* scene();

	CREATE_FUNC(InfoScene);

	void onButtonTouch(Ref* pSender, Widget::TouchEventType type);
    void onTouchMoved(Touch *touch, Event *event);
    bool onTouchBegan(Touch *touch, Event *event);
    void onTouchEnded(Touch *touch, Event *event);
};

#endif /* INFOSCENE_H_ */
