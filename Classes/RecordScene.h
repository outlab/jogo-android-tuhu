/*
 * RecordScene.h
 *
 *  Created on: 2015. 12. 10.
 *      Author: Jimmy
 */

#ifndef RECORDSCENE_H_
#define RECORDSCENE_H_

#include "common.h"

typedef struct _guitar_line {
	Node* _node;
	Rect _rect;
} GUITAR_LINE;

class RecordScene : public Layer {
private:
    Size _screenSize;
    float _margin;
    Node* _boy;
    Node* _icoMusic;
    Node* _icoMute;
    Button* _selInstrument;
    Node* _instruments;
    Node* _piano;
    int _currentInstrument;	// 0: Piano, 1: Dram, 2: Guitar
    bool _isPlayBgMusic;
    Rect _rcIconMusic;

    Vector<Node*> _pianoKeys;
    Vector<Node*> _pianoHalfKeys;
    Vector<Node*> _touchedKeys;

    Node* _dram;
    Vector<Node*> _dramKeys;
    bool _isDramLongPlayed;

    Node* _guitar;
    std::vector<GUITAR_LINE*> _guitarLines;

    float _middlePos;
    Node* _cartLeft;
    Node* _cartRight;

    Button* _btnBack;
    Button* _btnBackOfRecord;

    Button* _btnRecord;
    Node* _recording;
    Node* _recReady3;
    Node* _recReady2;
    Node* _recReady1;

    Node* _recordBoy;
    cocostudio::timeline::ActionTimeline* _recordBoyAction;

    Node* _bgRecord;

    void selectInstrument();
    void readyStart(float dt);
    void playStart(float dt);
    void backMenu();
    void playDramLongSound(float dt);
    void playGuitar(const std::vector<Touch*>& touches, bool bIsMove);

    void readyRecord();
    void readyRecord2(float dt);
    void readyRecord3(float dt);
    void readyRecord4(float dt);
    void startRecord(float dt);
    void timeoutRecord(float dt);
    void stopRecord();
    void stopRecordAfter(float dt);
    void openCart();
    void closeCart();

public:
	RecordScene();
	virtual ~RecordScene();

public:
	virtual bool init();

	static Scene* scene();

	CREATE_FUNC(RecordScene);

	void onTouchButtons(Ref* pSender, Widget::TouchEventType type);

	virtual bool onTouchBegan(Touch *touch, Event *unused_event);
	virtual void onTouchMoved(Touch *touch, Event *unused_event);
	virtual void onTouchEnded(Touch *touch, Event *unused_event);

	void onTouchesBegan(const std::vector<Touch*>& touches, Event *unused_event);
	void onTouchesMoved(const std::vector<Touch*>& touches, Event *unused_event);
	void onTouchesEnded(const std::vector<Touch*>& touches, Event *unused_event);
};

#endif /* RECORDSCENE_H_ */
