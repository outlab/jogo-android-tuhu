#include "SequenceMemory.h"
//#include "MenuScene.cpp"
#include "MenuScene.h"
#include "SimonGame.cpp"
#include "SimonGame.h"
#include "SimpleAudioEngine.h"

bool teste;
bool moveuBt;
int currentScore;
int auxBool;
int mNumber;
int musicNumber2;
float backFromGame;
float adjustYScale;

Node* musicONE;
Node* musicTWO;
Node* musicTHREE;
Node* musicFOUR;
Node* musicFIVE;
Node* musicSIX;


USING_NS_CC;
SequenceMemory::SequenceMemory()
{

    r=0;
    spinL = 0;
    moveuBt = false;
    musicONE = nullptr;
    musicTWO = nullptr;
    musicTHREE = nullptr;
    musicFOUR = nullptr;
    musicFIVE = nullptr;
    musicSIX = nullptr;
    blackBG = nullptr;
    fourStrings = nullptr;
    fourBlowing = nullptr;
    def1 = nullptr;
    musicNumber = 0;
    stringOrBlow = 0;
    teste = false;
    blackScreeOpen = false;
    backFromGame = false;
    star1 = nullptr;
    star2 = nullptr;
    star3 = nullptr;
    star1G = nullptr;
    star2G = nullptr;
    star3G = nullptr;
    currentScore = 0;
    nome1 = nullptr;
    nome2 = nullptr;
    nome3 = nullptr;
    nome4 = nullptr;
    nome5 = nullptr;
    nome6 = nullptr;
}

Scene* SequenceMemory::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = SequenceMemory::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}
Scene* SequenceMemory::createScene(int musicNumber, int StringOrBlow,bool varTrue)
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = SequenceMemory::create();
    auxBool = varTrue;
    mNumber = musicNumber;
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

Scene* SequenceMemory::createCustomScene(int musicnumber, Size visibleSize)
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = SequenceMemory::create();
    musicNumber2 = musicnumber;
    backFromGame = true;
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool SequenceMemory::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() ){
        return false;}

    
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    _screenSize = Director::getInstance()->getWinSize();
    auto glview = Director::getInstance()->getOpenGLView();
    auto frameSize = glview->getFrameSize();
    
    
    def1 = UserDefault::getInstance();
    highScoreMusic1 = def1->getIntegerForKey("HIGHSCOREM1",0);
    highScoreMusic2 = def1->getIntegerForKey("HIGHSCOREM2",0);
    highScoreMusic3 = def1->getIntegerForKey("HIGHSCOREM3",0);
    highScoreMusic4 = def1->getIntegerForKey("HIGHSCOREM4",0);
    highScoreMusic5 = def1->getIntegerForKey("HIGHSCOREM5",0);
    highScoreMusic6 = def1->getIntegerForKey("HIGHSCOREM6",0);
    
        
    if(frameSize.width/frameSize.height < 1.5)
        adjustYScale = 0.45;
    
   /* __String* temp = __String::createWithFormat("%f",frameSize.width);

     texto = Label::createWithTTF(temp->getCString(),"fonts/Marker Felt.ttf",visibleSize.height*0.1);
    texto->setColor(Color3B::WHITE);
    texto->setPosition(origin.x + visibleSize.width/2,origin.y +visibleSize.height/2);
    this->addChild(texto,4);*/

    auto background = Sprite::create("SubMenuSong/bg_levels.png");
    background->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2));
    background->setScale(_screenSize.width/(background->getContentSize().width));
    this->addChild(background,0);
    
    leftLight = Sprite::create("SubMenuSong/Luz2.png");
    leftLight->setPosition(Vec2(origin.x + (_screenSize.width/30),origin.y + (_screenSize.height/4) ));
    leftLight->setScale(0.57f);
    
    pivot = Sprite::create("SubMenuSong/vazio.png");
    pivot->setPosition(Vec2(origin.x + visibleSize.width/3.5,origin.y + (visibleSize.height/4)*2.6));
    pivot->addChild(leftLight);
    this->addChild(pivot);

    mask2 = Sprite::create("SubMenuSong/Luz2.png");
    mask2->setPosition(Vec2(origin.x + (_screenSize.width/20),origin.y + (_screenSize.height/4) ));
    mask2->setScale(0.57f);
    
    mask3 = Sprite::create("SubMenuSong/vazio.png");
    mask3->setPosition(Vec2(visibleSize.width-(origin.x + visibleSize.width/3.5),origin.y + (visibleSize.height/4)*2.6));
    mask3->addChild(mask2);
    this->addChild(mask3);
    mask3->setRotation(-35);

    cortinas = Sprite::create("SubMenuSong/cortina.png");
    cortinas->setAnchorPoint(Vec2(0.5f,1));
    cortinas->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height));
    cortinas->setScale(_screenSize.width/(cortinas->getContentSize().width));
    this->addChild(cortinas,3);

    this->schedule(schedule_selector(SequenceMemory::rotateWheels),TIME_UPDATE_LIGHT);
    this->schedule(schedule_selector(SequenceMemory::spinBright),TIME_UPDATE_LIGHT);
    this->scheduleOnce(schedule_selector(SequenceMemory::changeInstrument),0);
    this->scheduleUpdate();

    backButton = MenuItemImage::create("SubMenuSong/botaomenu.png","SubMenuSong/botaomenu.png",CC_CALLBACK_1(SequenceMemory::loadMenuScene,this));
    backButton->setPosition(Vec2(_screenSize.width*0.007 + backButton->getContentSize().width/2, origin.y + visibleSize.height - _screenSize.height*0.013 -backButton->getContentSize().height/2));
    backButton->setScale(_screenSize.width*0.00045);
    if(frameSize.width/frameSize.height < 1.5)
        backButton->setScaleY(adjustYScale);
    backButton->setTag(7);
    auto menuBackButton = Menu::create(backButton,NULL);
    menuBackButton->setPosition(Point::ZERO);
    this->addChild(menuBackButton,3);

    backButton2 = MenuItemImage::create("SubMenuSong/botao_voltar.png","SubMenuSong/botao_voltar.png",CC_CALLBACK_1(SequenceMemory::backToScene,this));
    backButton2->setPosition(Vec2(_screenSize.width*0.007 + backButton->getContentSize().width/2, origin.y + visibleSize.height - _screenSize.height*0.013 -backButton->getContentSize().height/2));
    backButton2->setScale(0);
    if(frameSize.width/frameSize.height < 1.5)
        backButton2->setScaleY(adjustYScale);
    backButton2->setTag(8);
    auto menuBackButton2 = Menu::create(backButton2,NULL);
    menuBackButton2->setPosition(Point::ZERO);
    this->addChild(menuBackButton2,5);

    musicONE = Sprite::create("SubMenuSong/mes.png");//MenuItemImage::create("SubMenuSong/mes.png","SubMenuSong/mes.png","SubMenuSong/mestrancado.png",CC_CALLBACK_1(SequenceMemory::GoToMusic1,this));
    musicONE->setPosition(Vec2(origin.x + (visibleSize.width/4),origin.y + visibleSize.height/2));
    musicONE->setScale((_screenSize.height/1.8)/musicONE->getContentSize().height);
    if(frameSize.width/frameSize.height < 1.5)
        musicONE->setScaleY(adjustYScale);
    this->addChild(musicONE,2);

    if(highScoreMusic1 >= 3)
        musicTWO = Sprite::create("SubMenuSong/ponte.png");//,"SubMenuSong/ponte.png","SubMenuSong/pontetrancada.png",CC_CALLBACK_1(SequenceMemory::GoToMusic2,this));
    else
        musicTWO = Sprite::create("SubMenuSong/pontetrancada.png");
    musicTWO->setPosition(Vec2(origin.x + (visibleSize.width/4)*2,origin.y + visibleSize.height/2));
    musicTWO->setScale((_screenSize.height/1.8)/musicTWO->getContentSize().height);
    if(frameSize.width/frameSize.height < 1.5)
        musicTWO->setScaleY(adjustYScale);
    //auto menuTWO = Menu::create(musicTWO,NULL);
    //menuTWO->setPosition(Point::ZERO);
    this->addChild(musicTWO,2);

    if(highScoreMusic2 >= 3)
        musicTHREE = Sprite::create("SubMenuSong/danca.png");//,"SubMenuSong/danca.png","SubMenuSong/dancatrancada.png",CC_CALLBACK_1(SequenceMemory::GoToMusic3,this));
    else
        musicTHREE = Sprite::create("SubMenuSong/dancatrancada.png");
    musicTHREE->setPosition(Vec2(origin.x + (visibleSize.width/4)*3,origin.y + visibleSize.height/2));
    musicTHREE->setTag(3);
    musicTHREE->setScale((_screenSize.height/1.8)/musicTHREE->getContentSize().height);
    if(frameSize.width/frameSize.height < 1.5)
        musicTHREE->setScaleY(adjustYScale);
   // auto menuTHREE = Menu::create(musicTHREE,NULL);
   // menuTHREE->setPosition(Point::ZERO);
    this->addChild(musicTHREE,2);

    if(highScoreMusic3 >= 3)
        musicFOUR = Sprite::create("SubMenuSong/atche-destrancada.png");//,"SubMenuSong/atche-destrancada.png","SubMenuSong/atchetrancada.png",CC_CALLBACK_1(SequenceMemory::GoToMusic4,this));
    else
        musicFOUR = Sprite::create("SubMenuSong/atchetrancada.png");
    musicFOUR->setPosition(Vec2(origin.x + (visibleSize.width/4)*4,origin.y + visibleSize.height/2));
    musicFOUR->setTag(4);
    musicFOUR->setScale((_screenSize.height/1.8)/musicFOUR->getContentSize().height);
    if(frameSize.width/frameSize.height < 1.5)
        musicFOUR->setScaleY(adjustYScale);
    //auto menuFOUR = Menu::create(musicFOUR,NULL);
    //menuFOUR->setPosition(Point::ZERO);
    this->addChild(musicFOUR,2);

    if(highScoreMusic4 >= 3)
        musicFIVE = Sprite::create("SubMenuSong/destrancada-nesta-rua.png");//,"SubMenuSong/destrancada-nesta-rua.png","SubMenuSong/trancada--nesta-rua.png",CC_CALLBACK_1(SequenceMemory::GoToMusic5,this));
    else
        musicFIVE = Sprite::create("SubMenuSong/trancada--nesta-rua.png");
    musicFIVE->setPosition(Vec2(origin.x + (visibleSize.width/4)*5,origin.y + visibleSize.height/2));
    musicFIVE->setTag(5);
    musicFIVE->setScale((_screenSize.height/1.8)/musicFIVE->getContentSize().height);
    if(frameSize.width/frameSize.height < 1.5)
        musicFIVE->setScaleY(adjustYScale);
   // auto menuFIVE = Menu::create(musicFIVE,NULL);
    //menuFIVE->setPosition(Point::ZERO);
    this->addChild(musicFIVE,2);

    if(highScoreMusic5 >= 3)
        musicSIX = Sprite::create("SubMenuSong/destrancada-condessa.png");//,"SubMenuSong/destrancada-condessa.png","SubMenuSong/trancada--condessa.png",CC_CALLBACK_1(SequenceMemory::GoToMusic6,this));
    else
    {
        musicSIX = Sprite::create("SubMenuSong/trancada--condessa.png");
    }
    musicSIX->setPosition(Vec2(origin.x + (visibleSize.width/4)*6,origin.y + visibleSize.height/2));
    musicSIX->setTag(6);
    musicSIX->setScale((_screenSize.height/1.8)/musicSIX->getContentSize().height);
    if(frameSize.width/frameSize.height < 1.5)
        musicSIX->setScaleY(adjustYScale);
    //auto menuSIX = Menu::create(musicSIX,NULL);
    //menuSIX->setPosition(Point::ZERO);
    this->addChild(musicSIX,2);
    
    blackBG = Sprite::create("SubMenuSong/blackBG.png");
    blackBG->setAnchorPoint(Vec2(0.5f,1));
    blackBG->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height));
    blackBG->setScale(_screenSize.width/blackBG->getContentSize().width);
    blackBG->setScaleY(1.2);
    blackBG->setOpacity(0);
    this->addChild(blackBG,4);

    fourStrings = MenuItemImage::create("SubMenuSong/Quarteto_cordas.png","SubMenuSong/Quarteto_cordas_pressed.png",CC_CALLBACK_1(SequenceMemory::goToGameSceneString,this));
    fourStrings->setAnchorPoint(Vec2(0.5,0.0));
    fourStrings->setPosition(Vec2(origin.x + (visibleSize.width/3)*1, origin.y + _screenSize.height + fourStrings->getContentSize().height));
    fourStrings->setScale(_screenSize.width*0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        fourStrings->setScaleY(adjustYScale);
    auto menuStrings = Menu::create(fourStrings,NULL);
    menuStrings->setPosition(Point::ZERO);
    this->addChild(menuStrings,5);

    fourBlowing = MenuItemImage::create("SubMenuSong/Quarteto_sopros.png","SubMenuSong/Quarteto_sopros_pressed.png","SubMenuSong/quinteto.png",CC_CALLBACK_1(SequenceMemory::goToGameSceneBlow,this));
    fourBlowing->setAnchorPoint(Vec2(0.5,0.0));
    fourBlowing->setPosition(Vec2(origin.x + (visibleSize.width/3)*2, origin.y + _screenSize.height + fourBlowing->getContentSize().height));
    fourBlowing->setScale(_screenSize.width*0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        fourBlowing->setScaleY(adjustYScale);
    auto menuBlow = Menu::create(fourBlowing,NULL);
    menuBlow->setPosition(Point::ZERO);
    this->addChild(menuBlow,5);

    goldLight1 = Sprite::create("SubMenuSong/Efeito_quarteto.png");
    goldLight1->setPosition(Vec2(origin.x + visibleSize.width/3.85, origin.y + _screenSize.height/3.2f));
    goldLight1->setScale(_screenSize.width*0.00035);
    if(frameSize.width/frameSize.height < 1.5)
        goldLight1->setScaleY(adjustYScale);
    goldLight1->setOpacity(0);
    fourStrings->addChild(goldLight1,-1);

    goldLight2 = Sprite::create("SubMenuSong/Efeito_quarteto.png");
    goldLight2->setPosition(Vec2(origin.x + visibleSize.width/3.85, origin.y + _screenSize.height/3.2f));
    goldLight2->setScale(_screenSize.width*0.00035);
    if(frameSize.width/frameSize.height < 1.5)
        goldLight2->setScaleY(adjustYScale);
    goldLight2->setOpacity(0);
    fourBlowing->addChild(goldLight2,-1);

    nome1 = Sprite::create("SubMenuSong/QuantosDiasTemOMes.png");
    nome1->setPosition(Vec2(origin.x + _screenSize.width/2, origin.y + visibleSize.height + nome1->getContentSize().height));
    this->addChild(nome1,6);
    nome2 = Sprite::create("SubMenuSong/LaNaPonteDaVinhaca.png");
    nome2->setPosition(Vec2(origin.x + _screenSize.width/2, origin.y + visibleSize.height + nome1->getContentSize().height));
    this->addChild(nome2,6);
    nome3 = Sprite::create("SubMenuSong/AdancadaCarranquinha.png");
    nome3->setPosition(Vec2(origin.x + _screenSize.width/2, origin.y + visibleSize.height + nome1->getContentSize().height));
    this->addChild(nome3,6);
    nome4 = Sprite::create("SubMenuSong/Texto--Atche.png");
    nome4->setPosition(Vec2(origin.x + _screenSize.width/2, origin.y + visibleSize.height + nome1->getContentSize().height));
    this->addChild(nome4,6);
    nome5 = Sprite::create("SubMenuSong/Nesta-rua-texto.png");
    nome5->setPosition(Vec2(origin.x + _screenSize.width/2, origin.y + visibleSize.height + nome1->getContentSize().height));
    this->addChild(nome5,6);
    nome6 = Sprite::create("SubMenuSong/texto-condessa.png");
    nome6->setPosition(Vec2(origin.x + _screenSize.width/2, origin.y + visibleSize.height + nome1->getContentSize().height));
    this->addChild(nome6,6);
    
    star1 = Sprite::create("SubMenuSong/musicStarSilver.png");
    star1->setScale(_screenSize.width*0.00019);
    star1->setPosition(Vec2(origin.x + _screenSize.width/2 - star1->getContentSize().width*_screenSize.width*0.0003, origin.y + visibleSize.height + star1->getContentSize().height*_screenSize.width*0.00019*1.5));
    //star1->setScaleY(adjustYScale);
    this->addChild(star1,6);

    star2 = Sprite::create("SubMenuSong/musicStarSilver.png");
    star2->setScale(_screenSize.width*0.00019);
    //star2->setScaleY(adjustYScale);
    star2->setPosition(Vec2(origin.x + _screenSize.width/2, origin.y + visibleSize.height + star1->getContentSize().height*_screenSize.width*0.00019*1.5));
    this->addChild(star2,6);

    star3 = Sprite::create("SubMenuSong/musicStarSilver.png");
    star3->setScale(_screenSize.width*0.00019);
    star3->setPosition(Vec2(origin.x + _screenSize.width/2 + star1->getContentSize().width*_screenSize.width*0.0003, origin.y + visibleSize.height + star1->getContentSize().height*_screenSize.width*0.00019*1.5));
    //star3->setScaleY(adjustYScale);
    this->addChild(star3,6);

    star1G = Sprite::create("SubMenuSong/musicStarGold.png");
    //star1G->setScale(0); //star1G->setScale(_screenSize.width*0.00025);
    //star1G->setScaleY(adjustYScale);
    star1G->setPosition(Vec2(origin.x + star1->getContentSize().width/2, origin.y + star1->getContentSize().height/2));
    star1->addChild(star1G,2);

    star2G = Sprite::create("SubMenuSong/musicStarGold.png");
    //star2G->setScale(0); //star2G->setScale(_screenSize.width*0.00025);
    //star2G->setScaleY(adjustYScale);
    star2G->setPosition(Vec2(origin.x + star1->getContentSize().width/2, origin.y + star1->getContentSize().height/2));
    star2->addChild(star2G,2);

    star3G = Sprite::create("SubMenuSong/musicStarGold.png");
    //star3G->setScale(0); //star3G->setScale(_screenSize.width*0.00025);
    //star3G->setScaleY(adjustYScale);
    star3G->setPosition(Vec2(origin.x + star1->getContentSize().width/2, origin.y + star1->getContentSize().height/2));
    star3->addChild(star3G,2);
    
    
    auto starMusic1 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic1->setScale(_screenSize.width*0.00022);
    //starMusic1->setScaleY(adjustYScale);
    starMusic1->setPosition(Vec2(musicONE->getContentSize().width*0.25,musicONE->getContentSize().height*0.95));
    musicONE->addChild(starMusic1,2);
    
    auto starMusic2 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic2->setScale(_screenSize.width*0.00022);
    //starMusic2->setScaleY(adjustYScale);
    starMusic2->setPosition(Vec2(musicONE->getContentSize().width*0.5,musicONE->getContentSize().height*0.95));
    musicONE->addChild(starMusic2,2);
    
    auto starMusic3 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic3->setScale(_screenSize.width*0.00022);
    //starMusic3->setScaleY(adjustYScale);
    starMusic3->setPosition(Vec2(musicONE->getContentSize().width*0.75,musicONE->getContentSize().height*0.95));
    musicONE->addChild(starMusic3,2);
    auto starMusic1G = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic1G->setScale(_screenSize.width*0.00022);
    //starMusic1G->setScaleY(adjustYScale);
    starMusic1G->setPosition(Vec2(musicONE->getContentSize().width*0.25,musicONE->getContentSize().height*0.95));
    musicONE->addChild(starMusic1G,2);
    
    auto starMusic2G = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic2G->setScale(_screenSize.width*0.00022);
    //starMusic2G->setScaleY(adjustYScale);
    starMusic2G->setPosition(Vec2(musicONE->getContentSize().width*0.5,musicONE->getContentSize().height*0.95));
    musicONE->addChild(starMusic2G,2);
    
    auto starMusic3G = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic3G->setScale(_screenSize.width*0.00022);
    //starMusic3G->setScaleY(adjustYScale);
    starMusic3G->setPosition(Vec2(musicONE->getContentSize().width*0.75,musicONE->getContentSize().height*0.95));
    musicONE->addChild(starMusic3G,2);
    
    if(highScoreMusic1 == 2)
        starMusic3G->setScale(0);
    else if(highScoreMusic1 == 1)
    {
        starMusic3G->setScale(0);
        starMusic2G->setScale(0);
    }
    else if(highScoreMusic1 == 0)
    {
        starMusic3G->setScale(0);
        starMusic2G->setScale(0);
        starMusic1G->setScale(0);
    }

    
    
    auto starMusic12 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic12->setScale(_screenSize.width*0.00022);
    //starMusic12->setScaleY(adjustYScale);
    starMusic12->setPosition(Vec2(musicONE->getContentSize().width*0.25,musicONE->getContentSize().height*0.95));
    musicTWO->addChild(starMusic12,2);
    
    auto starMusic22 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic22->setScale(_screenSize.width*0.00022);
    //starMusic22->setScaleY(adjustYScale);
    starMusic22->setPosition(Vec2(musicONE->getContentSize().width*0.5,musicONE->getContentSize().height*0.95));
    musicTWO->addChild(starMusic22,2);
    
    auto starMusic32 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic32->setScale(_screenSize.width*0.00022);
    //starMusic32->setScaleY(adjustYScale);
    starMusic32->setPosition(Vec2(musicONE->getContentSize().width*0.75,musicONE->getContentSize().height*0.95));
    musicTWO->addChild(starMusic32,2);
    auto starMusic1G2 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic1G2->setScale(_screenSize.width*0.00022);
    //starMusic1G2->setScaleY(adjustYScale);
    starMusic1G2->setPosition(Vec2(musicONE->getContentSize().width*0.25,musicONE->getContentSize().height*0.95));
    musicTWO->addChild(starMusic1G2,2);
    
    auto starMusic2G2 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic2G2->setScale(_screenSize.width*0.00022);
    //starMusic2G2->setScaleY(adjustYScale);
    starMusic2G2->setPosition(Vec2(musicONE->getContentSize().width*0.5,musicONE->getContentSize().height*0.95));
    musicTWO->addChild(starMusic2G2,2);
    
    auto starMusic3G2 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic3G2->setScale(_screenSize.width*0.00022);
    //starMusic3G2->setScaleY(adjustYScale);
    starMusic3G2->setPosition(Vec2(musicONE->getContentSize().width*0.75,musicONE->getContentSize().height*0.95));
    musicTWO->addChild(starMusic3G2,2);

    
    if(highScoreMusic2 == 2)
        starMusic3G2->setScale(0);
    else if(highScoreMusic2 == 1)
    {
        starMusic3G2->setScale(0);
        starMusic2G2->setScale(0);
    }
    else if(highScoreMusic2 == 0)
    {
        starMusic3G2->setScale(0);
        starMusic2G2->setScale(0);
        starMusic1G2->setScale(0);
    }
    
    
    auto starMusic13 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic13->setScale(_screenSize.width*0.00022);
    //starMusic13->setScaleY(adjustYScale);
    starMusic13->setPosition(Vec2(musicONE->getContentSize().width*0.25,musicONE->getContentSize().height*0.95));
    musicTHREE->addChild(starMusic13,2);
    
    auto starMusic23 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic23->setScale(_screenSize.width*0.0002);
    //starMusic23->setScaleY(adjustYScale);
    starMusic23->setPosition(Vec2(musicONE->getContentSize().width*0.5,musicONE->getContentSize().height*0.95));
    musicTHREE->addChild(starMusic23,2);
    
    auto starMusic33 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic33->setScale(_screenSize.width*0.00022);
    //starMusic33->setScaleY(adjustYScale);
    starMusic33->setPosition(Vec2(musicONE->getContentSize().width*0.75,musicONE->getContentSize().height*0.95));
    musicTHREE->addChild(starMusic33,2);
    auto starMusic1G3 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic1G3->setScale(_screenSize.width*0.00022);
    //starMusic1G3->setScaleY(adjustYScale);
    starMusic1G3->setPosition(Vec2(musicONE->getContentSize().width*0.25,musicONE->getContentSize().height*0.95));
    musicTHREE->addChild(starMusic1G3,2);
    
    auto starMusic2G3 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic2G3->setScale(_screenSize.width*0.00022);
    //starMusic2G3->setScaleY(adjustYScale);
    starMusic2G3->setPosition(Vec2(musicONE->getContentSize().width*0.5,musicONE->getContentSize().height*0.95));
    musicTHREE->addChild(starMusic2G3,2);
    
    auto starMusic3G3 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic3G3->setScale(_screenSize.width*0.00022);
    //starMusic3G3->setScaleY(adjustYScale);
    starMusic3G3->setPosition(Vec2(musicONE->getContentSize().width*0.75,musicONE->getContentSize().height*0.95));
    musicTHREE->addChild(starMusic3G3,2);

    if(highScoreMusic3 == 2)
        starMusic3G3->setOpacity(0);
    else if(highScoreMusic3 == 1)
    {
        starMusic3G3->setOpacity(0);
        starMusic2G3->setOpacity(0);
    }
    else if(highScoreMusic3 == 0)
    {
        starMusic3G3->setOpacity(0);
        starMusic2G3->setOpacity(0);
        starMusic1G3->setOpacity(0);
    }
    
    
    auto starMusic14 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic14->setScale(_screenSize.width*0.00022);
    //starMusic1->setScaleY(adjustYScale);
    starMusic14->setPosition(Vec2(musicONE->getContentSize().width*0.25,musicONE->getContentSize().height*0.95));
    musicFOUR->addChild(starMusic14,2);
    
    auto starMusic24 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic24->setScale(_screenSize.width*0.00022);
    //starMusic2->setScaleY(adjustYScale);
    starMusic24->setPosition(Vec2(musicONE->getContentSize().width*0.5,musicONE->getContentSize().height*0.95));
    musicFOUR->addChild(starMusic24,2);
    
    auto starMusic34 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic34->setScale(_screenSize.width*0.00022);
    //starMusic3->setScaleY(adjustYScale);
    starMusic34->setPosition(Vec2(musicONE->getContentSize().width*0.75,musicONE->getContentSize().height*0.95));
    musicFOUR->addChild(starMusic34,2);
    
    auto starMusic1G4 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic1G4->setScale(_screenSize.width*0.00022);
    //starMusic1G->setScaleY(adjustYScale);
    starMusic1G4->setPosition(Vec2(musicONE->getContentSize().width*0.25,musicONE->getContentSize().height*0.95));
    musicFOUR->addChild(starMusic1G4,2);
    
    auto starMusic2G4 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic2G4->setScale(_screenSize.width*0.00022);
    //starMusic2G->setScaleY(adjustYScale);
    starMusic2G4->setPosition(Vec2(musicONE->getContentSize().width*0.5,musicONE->getContentSize().height*0.95));
    musicFOUR->addChild(starMusic2G4,2);
    
    auto starMusic3G4 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic3G4->setScale(_screenSize.width*0.00022);
    //starMusic3G->setScaleY(adjustYScale);
    starMusic3G4->setPosition(Vec2(musicONE->getContentSize().width*0.75,musicONE->getContentSize().height*0.95));
    musicFOUR->addChild(starMusic3G4,2);
    
    if(highScoreMusic4 == 2)
        starMusic3G4->setScale(0);
    else if(highScoreMusic4 == 1)
    {
        starMusic3G4->setScale(0);
        starMusic2G4->setScale(0);
    }
    else if(highScoreMusic4 == 0)
    {
        starMusic3G4->setScale(0);
        starMusic2G4->setScale(0);
        starMusic1G4->setScale(0);
    }
    
    
    auto starMusic15 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic15->setScale(_screenSize.width*0.00022);
    //starMusic1->setScaleY(adjustYScale);
    starMusic15->setPosition(Vec2(musicONE->getContentSize().width*0.25,musicONE->getContentSize().height*0.95));
    musicFIVE->addChild(starMusic15,2);
    
    auto starMusic25 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic25->setScale(_screenSize.width*0.00022);
    //starMusic2->setScaleY(adjustYScale);
    starMusic25->setPosition(Vec2(musicONE->getContentSize().width*0.5,musicONE->getContentSize().height*0.95));
    musicFIVE->addChild(starMusic25,2);
    
    auto starMusic35 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic35->setScale(_screenSize.width*0.00022);
    //starMusic3->setScaleY(adjustYScale);
    starMusic35->setPosition(Vec2(musicONE->getContentSize().width*0.75,musicONE->getContentSize().height*0.95));
    musicFIVE->addChild(starMusic35,2);
    
    auto starMusic1G5 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic1G5->setScale(_screenSize.width*0.00022);
    //starMusic1G->setScaleY(adjustYScale);
    starMusic1G5->setPosition(Vec2(musicONE->getContentSize().width*0.25,musicONE->getContentSize().height*0.95));
    musicFIVE->addChild(starMusic1G5,2);
    
    auto starMusic2G5 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic2G5->setScale(_screenSize.width*0.00022);
    //starMusic2G->setScaleY(adjustYScale);
    starMusic2G5->setPosition(Vec2(musicONE->getContentSize().width*0.5,musicONE->getContentSize().height*0.95));
    musicFIVE->addChild(starMusic2G5,2);
    
    auto starMusic3G5 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic3G5->setScale(_screenSize.width*0.00022);
    //starMusic3G->setScaleY(adjustYScale);
    starMusic3G5->setPosition(Vec2(musicONE->getContentSize().width*0.75,musicONE->getContentSize().height*0.95));
    musicFIVE->addChild(starMusic3G5,2);
    
    if(highScoreMusic5 == 2)
        starMusic3G5->setScale(0);
    else if(highScoreMusic5 == 1)
    {
        starMusic3G5->setScale(0);
        starMusic2G5->setScale(0);
    }
    else if(highScoreMusic5 == 0)
    {
        starMusic3G5->setScale(0);
        starMusic2G5->setScale(0);
        starMusic1G5->setScale(0);
    }
    
    
    auto starMusic16 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic16->setScale(_screenSize.width*0.00022);
    //starMusic1->setScaleY(adjustYScale);
    starMusic16->setPosition(Vec2(musicONE->getContentSize().width*0.25,musicONE->getContentSize().height*0.95));
    musicSIX->addChild(starMusic16,2);
    
    auto starMusic26 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic26->setScale(_screenSize.width*0.00022);
    //starMusic2->setScaleY(adjustYScale);
    starMusic26->setPosition(Vec2(musicONE->getContentSize().width*0.5,musicONE->getContentSize().height*0.95));
    musicSIX->addChild(starMusic26,2);
    
    auto starMusic36 = Sprite::create("SubMenuSong/musicStarSilver.png");
    starMusic36->setScale(_screenSize.width*0.00022);
    //starMusic3->setScaleY(adjustYScale);
    starMusic36->setPosition(Vec2(musicONE->getContentSize().width*0.75,musicONE->getContentSize().height*0.95));
    musicSIX->addChild(starMusic36,2);
    
    auto starMusic1G6 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic1G6->setScale(_screenSize.width*0.00022);
    //starMusic1G->setScaleY(adjustYScale);
    starMusic1G6->setPosition(Vec2(musicONE->getContentSize().width*0.25,musicONE->getContentSize().height*0.95));
    musicSIX->addChild(starMusic1G6,2);
    
    auto starMusic2G6 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic2G6->setScale(_screenSize.width*0.00022);
    //starMusic2G->setScaleY(adjustYScale);
    starMusic2G6->setPosition(Vec2(musicONE->getContentSize().width*0.5,musicONE->getContentSize().height*0.95));
    musicSIX->addChild(starMusic2G6,2);
    
    auto starMusic3G6 = Sprite::create("SubMenuSong/musicStarGold.png");
    starMusic3G6->setScale(_screenSize.width*0.00022);
    //starMusic3G->setScaleY(adjustYScale);
    starMusic3G6->setPosition(Vec2(musicONE->getContentSize().width*0.75,musicONE->getContentSize().height*0.95));
    musicSIX->addChild(starMusic3G6,2);
    
    if(highScoreMusic6 == 2)
        starMusic3G6->setScale(0);
    else if(highScoreMusic6 == 1)
    {
        starMusic3G6->setScale(0);
        starMusic2G6->setScale(0);
    }
    else if(highScoreMusic6 == 0)
    {
        starMusic3G6->setScale(0);
        starMusic2G6->setScale(0);
        starMusic1G6->setScale(0);
    }
    
    auto touchListener = EventListenerTouchOneByOne::create( );
    touchListener->setSwallowTouches( true );
    touchListener->onTouchBegan = CC_CALLBACK_2( SequenceMemory::onTouchBegan, this );
    touchListener->onTouchMoved = CC_CALLBACK_2(SequenceMemory::onTouchMoved, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(SequenceMemory::onTouchEnded, this);
    Director::getInstance( )->getEventDispatcher( )->addEventListenerWithSceneGraphPriority( touchListener, this );
    
        return true;
}
void SequenceMemory::changeButtons(float dt)
{
    if(backFromGame)
    {
        if(musicNumber2 == 3)
        {
            musicONE->setPositionX(0);
            musicTWO->setPositionX(visibleSize.width/4);
            musicTHREE->setPositionX((visibleSize.width/4)*2);
            musicFOUR->setPositionX((visibleSize.width/4)*3);
            musicFIVE->setPositionX((visibleSize.width/4)*4);
            musicSIX->setPositionX((visibleSize.width/4)*5);
            
        }
        if(musicNumber2 == 4)
        {
            musicONE->setPositionX(-visibleSize.width/4);
            musicTWO->setPositionX(0);
            musicTHREE->setPositionX((visibleSize.width/4));
            musicFOUR->setPositionX((visibleSize.width/4)*2);
            musicFIVE->setPositionX((visibleSize.width/4)*3);
            musicSIX->setPositionX((visibleSize.width/4)*4);

        }
        if(musicNumber2 == 5)
        {
            musicONE->setPositionX(-(visibleSize.width/4)*2);
            musicTWO->setPositionX(-(visibleSize.width/4));
            musicTHREE->setPositionX(0);
            musicFOUR->setPositionX((visibleSize.width/4));
            musicFIVE->setPositionX((visibleSize.width/4)*2);
            musicSIX->setPositionX((visibleSize.width/4)*3);
        }
        if(musicNumber2 == 6)
        {
            musicONE->setPositionX(-(visibleSize.width/4)*2);
            musicTWO->setPositionX(-(visibleSize.width/4));
            musicTHREE->setPositionX(0);
            musicFOUR->setPositionX((visibleSize.width/4));
            musicFIVE->setPositionX((visibleSize.width/4)*2);
            musicSIX->setPositionX((visibleSize.width/4)*3);
        }

    }

}
void SequenceMemory::changeInstrument(float dt)
{
    if(auxBool == true)
    {
        def1 = UserDefault::getInstance();
        highScoreMusic1 = def1->getIntegerForKey("HIGHSCOREM1",0);
        highScoreMusic2 = def1->getIntegerForKey("HIGHSCOREM2",0);
        highScoreMusic3 = def1->getIntegerForKey("HIGHSCOREM3",0);
        highScoreMusic4 = def1->getIntegerForKey("HIGHSCOREM4",0);
        highScoreMusic5 = def1->getIntegerForKey("HIGHSCOREM5",0);
        highScoreMusic6 = def1->getIntegerForKey("HIGHSCOREM6",0);
        musicNumber = mNumber;
        if(musicNumber == 1)
        {
            currentScore = highScoreMusic1;
            if(currentScore < 2)
                fourBlowing->setEnabled(false);
            else
                fourBlowing->setEnabled(true);
        }
        else if(musicNumber == 2)
        {
            currentScore = highScoreMusic2;
            if(currentScore < 2)
                fourBlowing->setEnabled(false);
            else
                fourBlowing->setEnabled(true);
        }
        else if(musicNumber == 3)
        {
            currentScore = highScoreMusic3;
            if(currentScore < 2)
                fourBlowing->setEnabled(false);
            else
                fourBlowing->setEnabled(true);
        }
        else if(musicNumber == 4)
        {
            currentScore = highScoreMusic4;
            if(currentScore < 2)
                fourBlowing->setEnabled(false);
            else
                fourBlowing->setEnabled(true);
        }
        else if(musicNumber == 5)
        {
            currentScore = highScoreMusic5;
            if(currentScore < 2)
                fourBlowing->setEnabled(false);
            else
                fourBlowing->setEnabled(true);
        }
        else if(musicNumber == 6)
        {
            currentScore = highScoreMusic6;
            if(currentScore < 2)
                fourBlowing->setEnabled(false);
            else
                fourBlowing->setEnabled(true);
        }
        blackScreeOpen = true;
        SequenceMemory::animChoseInstrument(0);
    }
    else
    {
        if(backFromGame)
            SequenceMemory::changeButtons(0);
    }
}
void SequenceMemory::animChoseInstrument(float dt)
{
    blackScreeOpen = true;
    auto ScaleToButton = ScaleTo::create(1,_screenSize.width*0.00045);
    auto fadeIn = FadeIn::create(0.5);
    auto moveName = MoveTo::create(0.5f,Vec2(origin.x + _screenSize.width/2, origin.y + visibleSize.height - _screenSize.height*0.013 -backButton->getContentSize().height/2));
    auto moveStar1 = MoveTo::create(0.5f,Vec2(origin.x + _screenSize.width/2 - star1->getContentSize().width*_screenSize.width*0.0003, origin.y + visibleSize.height - _screenSize.height*0.013 -backButton->getContentSize().height/2 - star1->getContentSize().height*_screenSize.width*0.00019*1.5));
    auto moveStar2 = MoveTo::create(0.5f,Vec2(origin.x + _screenSize.width/2, origin.y + visibleSize.height - _screenSize.height*0.013 -backButton->getContentSize().height/2 - star1->getContentSize().height*_screenSize.width*0.00019*1.5));
    auto moveStar3 = MoveTo::create(0.5f,Vec2(origin.x + _screenSize.width/2 + star1->getContentSize().width*_screenSize.width*0.0003, origin.y + visibleSize.height - _screenSize.height*0.013 -backButton->getContentSize().height/2 - star1->getContentSize().height*_screenSize.width*0.00019*1.5));
    auto moveButton1 = MoveTo::create(1.5f,Vec2(origin.x + (visibleSize.width/3)*1, origin.y + visibleSize.height/2 - fourStrings->getContentSize().height/4));
    auto moveButton2 = MoveTo::create(1.5f,Vec2(origin.x + (visibleSize.width/3)*2, origin.y + visibleSize.height/2 - fourStrings->getContentSize().height/4));

    if(currentScore == 1)
    {
        star2G->setScale(0);
        star3G->setScale(0);
    }
    else if (currentScore == 2)
    {        
        star3G->setScale(0);
    }
    else if (currentScore == 3)
    {
    }
    else if(currentScore == 0)
    {
        star1G->setScale(0);
        star2G->setScale(0);
        star3G->setScale(0);
    }    

    backButton->runAction(EaseElasticOut::create(ScaleTo::create(0.2f,0)));
    backButton2->runAction(Sequence::create(DelayTime::create(0.4f),EaseElasticOut::create(ScaleToButton),nullptr));
    blackBG->runAction(Sequence::create(fadeIn,DelayTime::create(3),nullptr));
    fourStrings->runAction(EaseElasticOut::create(moveButton1));
    fourBlowing->runAction(EaseElasticOut::create(moveButton2));
    star1->runAction(moveStar1);
    star2->runAction(moveStar2);
    star3->runAction(moveStar3);

    if(musicNumber == 1)
        nome1->runAction(moveName);
    else if(musicNumber == 2)
        nome2->runAction(moveName);
    else if(musicNumber == 3)
        nome3->runAction(moveName);
    else if(musicNumber == 4)
        nome4->runAction(moveName);
    else if(musicNumber == 5)
        nome5->runAction(moveName);
    else if (musicNumber == 6)
        nome6->runAction(moveName);

}
void SequenceMemory::goToGameSceneString(Ref* pSender)
{
    stringOrBlow = 1;
    auto OpenGame = CallFunc::create([this]()
    {
        auto simonScene = SimonGame::createScene(musicNumber,stringOrBlow);
        Director::getInstance()->replaceScene(simonScene); 
    });
    auto fadeIn = FadeIn::create(0.05);
    auto scale = ScaleTo::create(0.05,_screenSize.width*0.0008);
    auto sequenciadeAnim = Sequence::create(fadeIn,scale,DelayTime::create(0.08f),OpenGame,nullptr);
    goldLight1->runAction(sequenciadeAnim);
}
void SequenceMemory::goToGameSceneBlow(Ref* pSender)
{
    stringOrBlow = 2;
    auto OpenGame = CallFunc::create([this]()
    {
        if(currentScore >= 2)
        {
            auto simonScene = SimonGame::createScene(musicNumber,stringOrBlow);
            Director::getInstance()->replaceScene(simonScene); 
        }
    });

    auto fadeIn = FadeIn::create(0.05);
    auto scale = ScaleTo::create(0.05,_screenSize.width*0.0008);
    auto sequenciadeAnim = Sequence::create(fadeIn,scale,DelayTime::create(0.08f),OpenGame,nullptr);
    
    if(currentScore >= 2)
        goldLight2->runAction(sequenciadeAnim);
}

void SequenceMemory::GoToMusic1(Ref* pSender)
{

}
void SequenceMemory::GoToMusic2(Ref* pSender)
{

}
void SequenceMemory::GoToMusic3(Ref* pSender)
{   
    
}
void SequenceMemory::GoToMusic4(Ref* pSender)
{   
    
}
void SequenceMemory::GoToMusic5(Ref* pSender)
{   
    
}
void SequenceMemory::GoToMusic6(Ref* pSender)
{   
   
}
void SequenceMemory::teste2(Touch *touch, Event *event)
{
    auto target = static_cast<Sprite*>(event->getCurrentTarget());
    Point locationInNode = target->convertToNodeSpace(touch->getLocation());
    log("%f",locationInNode.x);
}

bool SequenceMemory::onTouchBegan(Touch *touch, Event *event)
{
   /* auto target = static_cast<Sprite*>(event->getCurrentTarget());
    Point locationInNode = target->convertToNodeSpace(touch->getLocation());
    log("%f",locationInNode.x);*/
    if(!blackScreeOpen)
    {
        _ptStart = touch->getLocation();
        
        if(musicONE->getBoundingBox().containsPoint(_ptStart))
        {
            //log("TRUE");
        }
        if(_ptStart.x < _screenSize.width)
        {
            //fourStrings->selected();
        }
        _ptTouch = _ptStart;
        _isButtonMoved = false;
        return true;
    }
}
void SequenceMemory::onTouchMoved(Touch *touch, Event *event)
{
    if(!blackScreeOpen)
    {
        Vec2 ptCurrent = touch->getLocation();
        if(!_isButtonMoved && fabsf(ptCurrent.x - _ptStart.x) > 1.0f)
            _isButtonMoved = true;
        
        float fDistance = ptCurrent.x - _ptStart.x;
        //log("%f",fDistance);
        if(fDistance < 0 && (musicSIX->getPositionX() <= origin.x + (visibleSize.width/4)*3.15))
            fDistance = 0;
        if(fDistance > 0 && (musicONE->getPositionX() >= (visibleSize.width/4)))
            fDistance = 0;
        musicONE->setPosition(musicONE->getPositionX() + fDistance, musicONE->getPositionY());
        musicTWO->setPosition(musicTWO->getPositionX() + fDistance, musicTWO->getPositionY());
        musicTHREE->setPosition(musicTHREE->getPositionX() + fDistance, musicTHREE->getPositionY());
        musicFOUR->setPosition(musicFOUR->getPositionX() + fDistance, musicFOUR->getPositionY());
        musicFIVE->setPosition(musicFIVE->getPositionX() + fDistance, musicFIVE->getPositionY());
        musicSIX->setPosition(musicSIX->getPositionX() + fDistance, musicSIX->getPositionY());
        
        _ptStart = ptCurrent;
    }
}
void SequenceMemory::onTouchEnded(Touch *touch, Event *event)
{
    if(!blackScreeOpen)
    {
        if(!_isButtonMoved)
        {
            if(musicONE->getBoundingBox().containsPoint(_ptStart))
            {
                nome1->setOpacity(255);
                nome2->setOpacity(0);
                nome3->setOpacity(0);
                nome4->setOpacity(0);
                nome5->setOpacity(0);
                nome6->setOpacity(0);
                
                currentScore = highScoreMusic1;
                musicNumber = 1;
                if(currentScore < 2)
                    fourBlowing->setEnabled(false);
                else
                    fourBlowing->setEnabled(true);
                SequenceMemory::animChoseInstrument(0);
            }
            else if(musicTWO->getBoundingBox().containsPoint(_ptStart))
            {
                if(highScoreMusic1 == 3)
                {
                    nome1->setOpacity(0);
                    nome2->setOpacity(255);
                    nome3->setOpacity(0);
                    nome4->setOpacity(0);
                    nome5->setOpacity(0);
                    nome6->setOpacity(0);
                    
                    currentScore = highScoreMusic2;
                    musicNumber = 2;
                    if(currentScore < 2)
                        fourBlowing->setEnabled(false);
                    else
                        fourBlowing->setEnabled(true);
                    SequenceMemory::animChoseInstrument(0);
                }
            }
            else if(musicTHREE->getBoundingBox().containsPoint(_ptStart))
            {
                if(highScoreMusic2 == 3)
                {
                    nome2->setOpacity(0);
                    nome3->setOpacity(255);
                    nome1->setOpacity(0);
                    nome4->setOpacity(0);
                    nome5->setOpacity(0);
                    nome6->setOpacity(0);
                    currentScore = highScoreMusic3;
                    musicNumber = 3;
                    if(currentScore < 2)
                        fourBlowing->setEnabled(false);
                    else
                        fourBlowing->setEnabled(true);
                    SequenceMemory::animChoseInstrument(0);
                }
            }
            else if(musicFOUR->getBoundingBox().containsPoint(_ptStart))
            {
                if(highScoreMusic3 == 3)
                {
                    nome4->setOpacity(255);
                    nome2->setOpacity(0);
                    nome1->setOpacity(0);
                    nome3->setOpacity(0);
                    nome5->setOpacity(0);
                    nome6->setOpacity(0);
                    
                    currentScore = highScoreMusic4;
                    musicNumber = 4;
                    if(currentScore < 2)
                        fourBlowing->setEnabled(false);
                    else
                        fourBlowing->setEnabled(true);
                    SequenceMemory::animChoseInstrument(0);
                }
            }
            else if(musicFIVE->getBoundingBox().containsPoint(_ptStart))
            {
                if(highScoreMusic4 == 3)
                {
                    nome2->setOpacity(0);
                    nome1->setOpacity(0);
                    nome3->setOpacity(0);
                    nome4->setOpacity(0);
                    nome6->setOpacity(0);
                    nome5->setOpacity(255);
                    
                    currentScore = highScoreMusic5;
                    musicNumber = 5;
                    if(currentScore < 2)
                        fourBlowing->setEnabled(false);
                    else
                        fourBlowing->setEnabled(true);
                    SequenceMemory::animChoseInstrument(0);
                }
            }
            else if(musicSIX->getBoundingBox().containsPoint(_ptStart))
            {
                if(highScoreMusic5 == 3)
                {
                    nome2->setOpacity(0);
                    nome1->setOpacity(0);
                    nome3->setOpacity(0);
                    nome4->setOpacity(0);
                    nome5->setOpacity(0);
                    nome6->setOpacity(255);
                    
                    currentScore = highScoreMusic6;
                    musicNumber = 6;
                    if(currentScore < 2)
                        fourBlowing->setEnabled(false);
                    else
                        fourBlowing->setEnabled(true);
                    SequenceMemory::animChoseInstrument(0);
                }
            }
        }
    }
}


void SequenceMemory::rotateWheels(float dt)
{
    r += -1;
    pivot->setRotation(r);
    mask3->setRotation(-r+0.6);

}
void SequenceMemory::spinBright(float dt)
{
    spinL += 0.15f;
    goldLight1->setRotation(spinL);
    goldLight2->setRotation(spinL);
}

void SequenceMemory::loadMenuScene(Ref* pSender)
{
    if(!blackScreeOpen)
    {
        auto scene = MenuScene::scene();
        Director::getInstance()->replaceScene(scene);
    }
}
void SequenceMemory::backToScene(Ref* pSender)
{
    blackScreeOpen = false;
    //auto ScaleToButton = ScaleTo::create(0.1,0);
    auto fadeOUT = FadeOut::create(0.5);
    auto moveName = MoveTo::create(0.5f,Vec2(origin.x + _screenSize.width/2, origin.y + visibleSize.height + nome1->getContentSize().height));
    auto moveStar1 = MoveTo::create(0.5f,Vec2(origin.x + _screenSize.width/2 - star1->getContentSize().width*_screenSize.width*0.0003, origin.y + visibleSize.height + star1->getContentSize().height*_screenSize.width*0.00019*1.5));
    auto moveStar2 = MoveTo::create(0.5f,Vec2(origin.x + _screenSize.width/2, origin.y + visibleSize.height + star1->getContentSize().height*_screenSize.width*0.00019*1.5));
    auto moveStar3 = MoveTo::create(0.5f,Vec2(origin.x + _screenSize.width/2 + star1->getContentSize().width*_screenSize.width*0.0003, origin.y + visibleSize.height + star1->getContentSize().height*_screenSize.width*0.00019*1.5));
    auto moveButton1 = MoveTo::create(0.8f,Vec2(origin.x + (visibleSize.width/3)*1, origin.y + _screenSize.height + fourStrings->getContentSize().height));
    auto moveButton2 = MoveTo::create(0.8f,Vec2(origin.x + (visibleSize.width/3)*2, origin.y + _screenSize.height + fourStrings->getContentSize().height));
    
    star1->runAction(moveStar1);
    star2->runAction(moveStar2);
    star3->runAction(moveStar3);
    blackBG->runAction(fadeOUT);
    backButton2->setScale(0);//runAction(ScaleToButton);
    fourStrings->runAction(moveButton1);
    fourBlowing->runAction(moveButton2);
    backButton->runAction(EaseElasticOut::create(ScaleTo::create(0.15,_screenSize.width*0.00045)));

    
    if(musicNumber == 1)
        nome1->runAction(moveName);
    else if(musicNumber == 2)
        nome2->runAction(moveName);
    else if(musicNumber == 3)
        nome3->runAction(moveName);
    else if(musicNumber == 4)
        nome4->runAction(moveName);
    else if(musicNumber == 5)
        nome5->runAction(moveName);
    else if (musicNumber == 6)
        nome6->runAction(moveName);
}

void SequenceMemory::loadThisScene(Ref* pSender)
{
    if(blackScreeOpen)
    {
        auxBool = false;
        mNumber = 0;
        auto scene = SequenceMemory::createScene();
        Director::getInstance()->replaceScene(scene);
    }
}

void SequenceMemory::update(float dt)
{
    if(blackScreeOpen)
    {
        if(!teste)
        {
            {
                if(fourStrings->isSelected() )
                {
                    auto fadeIn = FadeIn::create(0.05);
                    auto scale = ScaleTo::create(0.05,_screenSize.width*0.0008);
                    auto sequenciadeAnim = Sequence::create(fadeIn,scale,DelayTime::create(0.08f),nullptr);
                    goldLight1->runAction(sequenciadeAnim);
                    teste = true;
                }
            }
            if(fourBlowing->isSelected() )
            {
                auto fadeIn = FadeIn::create(0.05);
                auto scale = ScaleTo::create(0.05,_screenSize.width*0.0008);
                auto sequenciadeAnim = Sequence::create(fadeIn,scale,DelayTime::create(0.08f),nullptr);
                if(currentScore >= 2)
                    goldLight2->runAction(sequenciadeAnim);
                teste = true;
            }
        }
        
        if(!fourStrings->isSelected() )
        {
            auto fadeout = FadeOut::create(0.01);
            auto scale = ScaleTo::create(0.01,_screenSize.width*0.0004);
            auto sequenciadeAnim = Sequence::create(fadeout,scale,DelayTime::create(0.08f),nullptr);
            goldLight1->runAction(sequenciadeAnim);
            teste = false;
        }
        if(!fourBlowing->isSelected() )
        {
            auto fadeout = FadeOut::create(0.01);
            auto scale = ScaleTo::create(0.01,_screenSize.width*0.0004);
            auto sequenciadeAnim = Sequence::create(fadeout,scale,DelayTime::create(0.08f),nullptr);
            if(currentScore >=2)
                goldLight2->runAction(sequenciadeAnim);
            teste = false;
        }
    }
}