/*
 * MenuScene.h
 *
 *  Created on: 2015. 12. 7.
 *      Author: Jimmy
 */

#ifndef MENUSCENE_H_
#define MENUSCENE_H_

#include "common.h"

using namespace cocos2d;

class MenuScene : public Layer {
private:
    Size _screenSize;
    float _margin;
    float r;
    float s;
    bool scaleDirection;

    Node* _trem;
    Node* _vagao1;
    Node* _vagao2;
    Sprite* vagao3;
    Sprite* wheel1Vagao3;
    Sprite* wheel2Vagao3;
    Sprite* gradeVagao3;

    MenuItemImage* playGenius;

    float _tremAngle;
    Vec2 _ptStart;
    Vec2 _ptTouch;
    bool _isTremMoved;

    void showInfo();
    void showProfile();
    void showPenta();

    void touchPiano();
    void touchDram();

    void initStars(Vec2* pPos);
    Action* createStarAction(Vec2* pPos, int dirX, int dirY, float fActTime, float fDelayTime);

    void onTremFrameEvent(cocostudio::timeline::Frame* frame);
    void GoToMainMenuScene(cocos2d::Ref *sender);
    void rotateWheels(float dt);
    void scaleCar3(float dt);
    void GoToSimonsGameScene(cocos2d::Ref *sender);
public:

	MenuScene();
	virtual ~MenuScene();

public:
	virtual bool init();

	static Scene* scene();

	CREATE_FUNC(MenuScene);

    void moveCloud(float dt);
    void onButtonTouch(Ref* pSender, Widget::TouchEventType type);

    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchMoved(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);
};

#endif /* MENUSCENE_H_ */
