/*
 * MenuScene.cpp
 *
 *  Created on: 2015. 12. 7.
 *      Author: Jimmy
 */

#include "MenuScene.h"
#include "PianoScene.h"
#include "PianoScene.cpp"
#include "RecordScene.h"
#include "RecordScene.cpp"
#include "LearnScene.h"
#include "LearnScene.cpp"
#include "InfoScene.h"
#include "InfoScene.cpp"
#include "SequenceMemory.h"
#include "SequenceMemory.cpp"
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include "SonarFrameworks.h"

USING_NS_CC;

MenuScene::MenuScene() {
	// TODO Auto-generated constructor stub
	_margin = 20.0f;
	_tremAngle = 0.0f;
    r = 0;
    s = 0;
    scaleDirection = true;
    playGenius = nullptr;
	_trem = nullptr;
	_vagao1 = nullptr;
	_vagao2 = nullptr;
	vagao3 = nullptr;
	wheel1Vagao3 = nullptr;
	wheel2Vagao3 = nullptr;
	gradeVagao3 = nullptr;
	_isTremMoved = false;

}

MenuScene::~MenuScene() {
	// TODO Auto-generated destructor stub
}

Scene* MenuScene::scene() {
	Scene *scene = Scene::create();

	MenuScene *layer = MenuScene::create();
	scene->addChild(layer);

	return scene;
}

bool MenuScene::init() {
	if(!Layer::init())
		return false;
	EventDispatcher* dispatcher = Director::getInstance()->getEventDispatcher();
	auto positionListener = EventListenerTouchOneByOne::create();
	positionListener->setSwallowTouches(true);
	positionListener->onTouchBegan = CC_CALLBACK_2(MenuScene::onTouchBegan, this);
	positionListener->onTouchMoved = CC_CALLBACK_2(MenuScene::onTouchMoved, this);
	positionListener->onTouchEnded = CC_CALLBACK_2(MenuScene::onTouchEnded, this);
	dispatcher->addEventListenerWithSceneGraphPriority(positionListener, this);

	_screenSize = Director::getInstance()->getWinSize();

	auto background = Sprite::create("Menu/Ceu.png", Rect(255, 1, APP_WIDTH, APP_HEIGHT));
	background->setPosition(Vec2(_screenSize.width * 0.5f, _screenSize.height * 0.5f));
	this->addChild(background, 0);


	auto title = Sprite::create("Menu/logo-tuhu.png");
	title->setPosition(Vec2(_screenSize.width * 0.5f, _screenSize.height * 0.98f - title->getContentSize().height));
	title->setScale(_screenSize.width*0.00065);
	this->addChild(title, 10);

	moveCloud(0.0f);
	this->schedule(schedule_selector(MenuScene::moveCloud), TIME_CLOUD_MOVE);
	this->schedule(schedule_selector(MenuScene::rotateWheels),TIME_UPDATE_WHEELS);
	//this->schedule(schedule_selector(MenuScene::scaleCar3),TIME_UPDATE_WHEELS);

	auto btInfo = Button::create();
    	btInfo->setTouchEnabled(true);
    	btInfo->loadTextures("Menu/info_normal.png", "Menu/info_selected.png");
    	btInfo->setPosition(Vec2(_margin + btInfo->getContentSize().width * 0.5f, _screenSize.height - _margin - btInfo->getContentSize().height * 0.5f));
        btInfo->cocos2d::Node::setScale(_screenSize.height*.0015);
    	btInfo->addTouchEventListener(CC_CALLBACK_2(MenuScene::onButtonTouch, this));
    	this->addChild(btInfo, 2, TAG_BUTTON_INFO);

    	auto btPenta = Button::create();
    	btPenta->setTouchEnabled(true);
    	btPenta->loadTextures("Menu/pentagrama.png", "Menu/pentagrama.png");
    	Vec2 pentaPos = Vec2(_screenSize.width - _margin - btInfo->getContentSize().width * 0.8f, _screenSize.height - _margin - btPenta->getContentSize().height * 0.3f);
    	btPenta->setPosition(pentaPos);
        btPenta->cocos2d::Node::setScale(_screenSize.height*.0008);
        //btPenta->cocos2d::Node::setScaleX(1.0001);
    	btPenta->addTouchEventListener(CC_CALLBACK_2(MenuScene::onButtonTouch, this));
    	this->addChild(btPenta, 2, TAG_BUTTON_PENTA);

    	auto btProfile = Button::create();
    	btProfile->setTouchEnabled(true);
    	btProfile->loadTextures("Menu/facebook.png", "Menu/facebook.png");
    	btProfile->setPosition(Vec2(_screenSize.width - _margin - btInfo->getContentSize().width - btPenta->getContentSize().width * 0.6,_screenSize.height - _margin - btPenta->getContentSize().height * 0.3f));
        btProfile->cocos2d::Node::setScale(_screenSize.height*.0008);
        //btProfile->cocos2d::Node::setScaleX(1.0001);
    	btProfile->addTouchEventListener(CC_CALLBACK_2(MenuScene::onButtonTouch, this));
    	this->addChild(btProfile, 5, TAG_BUTTON_PROFILE);

	initStars(&pentaPos);

	auto menuScene = CSLoader::createNode("Menu/MenuScene.csb");
	auto grand = menuScene->getChildByTag(30);
	grand->setPosition(Vec2(_screenSize.width * 0.5f, -0.6f * grand->getContentSize().height));
	auto actGrand = RepeatForever::create(RotateBy::create(0.1f, 1.0f));
	grand->runAction(actGrand);
	this->addChild(grand, 5);

	for(float angle = 0.0f; angle < 360.0f; angle += 30.0f) {
		auto tree = Sprite::create("Menu/arvore.png", ((int)(angle / 30.0f)) % 2 == 0 ? Rect(13, 2, 569, 489) : Rect(0, 496, 570, 464));
		tree->setAnchorPoint(Vec2(0.5f, -1.8f));
		tree->setRotation(angle);
		tree->setPosition(Vec2(_screenSize.width * 0.5f, -0.6f * grand->getContentSize().height - _margin));
		auto actTree = RepeatForever::create(RotateBy::create(0.2f, 1.0f));
		tree->runAction(actTree);
		this->addChild(tree, 4);
	}

	_trem = menuScene->getChildByTag(53);
	auto realtrem = _trem->getChildByTag(226);
	_trem->setPosition(Vec2(_screenSize.width * 0.5f, -0.6f * grand->getContentSize().height));
	cocostudio::timeline::ActionTimeline* action = CSLoader::createTimeline("Menu/Tram.csb");
	realtrem->getChildByTag(157)->runAction(action);
	action->gotoFrameAndPlay(0, 480, true);
	action->setFrameEventCallFunc(CC_CALLBACK_1(MenuScene::onTremFrameEvent, this));

	realtrem->getChildByTag(157)->getChildByTag(179)->setOpacity(0);
	realtrem->getChildByTag(157)->getChildByTag(180)->setOpacity(0);
	realtrem->getChildByTag(157)->getChildByTag(181)->setOpacity(0);
	realtrem->getChildByTag(157)->getChildByTag(182)->setOpacity(0);

	_trem->setRotation(90.0f);

	this->addChild(_trem, 8);

	_vagao1 = menuScene->getChildByTag(99);
	auto realvagao1 = _vagao1->getChildByTag(100);
	_vagao1->setPosition(Vec2(_screenSize.width * 0.5f, -0.6f * grand->getContentSize().height));
	action = CSLoader::createTimeline("Menu/Vagao1.csb");
	realvagao1->runAction(action);
	action->gotoFrameAndPlay(0, 60, true);
	_vagao1->setRotation(113.0f);
	this->addChild(_vagao1, 7);

	_vagao2 = menuScene->getChildByTag(106);
	auto realvagao2 = _vagao2->getChildByTag(107);
	_vagao2->setPosition(Vec2(_screenSize.width * 0.5f, -0.6f * grand->getContentSize().height));
	action = CSLoader::createTimeline("Menu/Vagao2.csb");
	realvagao2->runAction(action);
	action->gotoFrameAndPlay(0, 60, true);
	_vagao2->setRotation(136.0f);
	this->addChild(_vagao2, 6);

    playGenius = MenuItemImage::create("Menu/vagao3.png","Menu/vagao3.png",CC_CALLBACK_1(MenuScene::GoToSimonsGameScene,this));
	playGenius->setPosition(Vec2(_screenSize.width * 0.47f,_screenSize.height*0.32f));
	playGenius->setScale(_screenSize.width *0.00036);
    auto menu = Menu::create(playGenius,NULL);
    menu->setPosition(Point::ZERO);
    _vagao2->addChild(menu,-1);

    wheel1Vagao3 = Sprite::create("Menu/roda.png");
    wheel1Vagao3->setPosition(Vec2(_screenSize.width * 0.4f,_screenSize.height*0.105f));
    wheel1Vagao3->setScale(1.05f);
    playGenius->addChild(wheel1Vagao3);

    wheel2Vagao3 = Sprite::create("Menu/roda.png");
    wheel2Vagao3->setPosition(Vec2(_screenSize.width * 0.15f,_screenSize.height*0.29f));
    wheel2Vagao3->setScale(1.05f);
    playGenius->addChild(wheel2Vagao3);

    gradeVagao3 = Sprite::create("Menu/grade_madeira.png");
    gradeVagao3->setPosition(Vec2(_screenSize.width * 0.28f,_screenSize.height*0.227f));
    playGenius->addChild(gradeVagao3);

	_trem->runAction(RotateTo::create(2.0f, -23.0f));
	_vagao1->runAction(RotateTo::create(2.0f, 0.0f));
	_vagao2->runAction(RotateTo::create(2.0f, 23.0f));

	SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(1.0f);
	SimpleAudioEngine::getInstance()->playBackgroundMusic("audio_fundo.ogg", true);

	return true;
}

void MenuScene::onTremFrameEvent(cocostudio::timeline::Frame* frame) {
	float fRnd = CCRANDOM_0_1();
	_trem->getChildByTag(226)->getChildByTag(157)->getChildByTag(170)->getChildByTag(178)->setOpacity(fRnd > 0.5f ? 255 : 0);
	SimpleAudioEngine::getInstance()->playEffect(fRnd > 0.5f ? "Sons/Tuhu_Trem.ogg" : "Sons/Tuhu_Trem_clique.ogg");

	auto act1 = Sequence::create(
			MoveBy::create(0.0f, Vec2(_margin * -0.5f, _margin * -8.0f)),
			FadeIn::create(0.0f),
			MoveBy::create(0.8f, Vec2(_margin * 0.5f, _margin * 8.0f)),
			FadeOut::create(0.2f),
			nullptr
			);
	auto act2 = Sequence::create(
			DelayTime::create(0.2f),
			MoveBy::create(0.0f, Vec2(0.0f, _margin * -8.0f)),
			FadeIn::create(0.0f),
			MoveBy::create(0.8f, Vec2(0.0f, _margin * 8.0f)),
			FadeOut::create(0.2f),
			nullptr
			);
	auto act3 = Sequence::create(
			DelayTime::create(0.4f),
			MoveBy::create(0.0f, Vec2(_margin * 0.5f, _margin * -8.0f)),
			FadeIn::create(0.0f),
			MoveBy::create(0.8f, Vec2(_margin * -0.5f, _margin * 8.0f)),
			FadeOut::create(0.2f),
			nullptr
			);
	auto act4 = Sequence::create(
			DelayTime::create(0.6f),
			MoveBy::create(0.0f, Vec2(_margin * -0.0f, _margin * -8.0f)),
			FadeIn::create(0.0f),
			MoveBy::create(0.8f, Vec2(_margin * 0.0f, _margin * 8.0f)),
			FadeOut::create(0.2f),
			nullptr
			);

	//);

	_trem->getChildByTag(226)->getChildByTag(157)->getChildByTag(179)->runAction(act1);
	_trem->getChildByTag(226)->getChildByTag(157)->getChildByTag(180)->runAction(act2);
	_trem->getChildByTag(226)->getChildByTag(157)->getChildByTag(181)->runAction(act3);
	_trem->getChildByTag(226)->getChildByTag(157)->getChildByTag(182)->runAction(act4);
}

void MenuScene::initStars(Vec2* pPos) {
	auto star1 = Sprite::create("Menu/botao.png", Rect(3, 0, 24, 26));
	star1->setPosition(Vec2(pPos->x - _margin, pPos->y + _margin));
	auto act1 = createStarAction(pPos, -1, 1, 0.5f, 0.2f);
	star1->runAction(act1);
	this->addChild(star1, 3);

	auto star2 = Sprite::create("Menu/botao.png", Rect(5, 31, 19, 19));
	star2->setPosition(Vec2(pPos->x + _margin, pPos->y - _margin));
	auto act2 = createStarAction(pPos, 1, -1, 0.5f, 0.2f);
	star2->runAction(act2);
	this->addChild(star2, 3);

	auto star3 = Sprite::create("Menu/botao.png", Rect(9, 57, 12, 12));
	star3->setPosition(Vec2(pPos->x + _margin, pPos->y + _margin));
	auto act3 = createStarAction(pPos, 1, 1, 0.4f, 0.3f);
	star3->runAction(act3);
	this->addChild(star3, 3);

	auto star4 = Sprite::create("Menu/botao.png", Rect(10, 78, 9, 10));
	star4->setPosition(Vec2(pPos->x - _margin, pPos->y - _margin));
	auto act4 = createStarAction(pPos, -1, -1, 0.4f, 0.3f);
	star4->runAction(act4);
	this->addChild(star4, 3);
}

bool MenuScene::onTouchBegan(Touch* touch, Event* unused_event) {
	_ptStart = touch->getLocation();
	_ptTouch = _ptStart;
	_isTremMoved = false;
	return true;
}
void MenuScene::onTouchMoved(Touch* touch, Event* unused_event) {
	Vec2 ptCurrent = touch->getLocation();
	if(!_isTremMoved && fabsf(ptCurrent.x - _ptStart.x) > 1.0f)
		_isTremMoved = true;

	float fDistance = ptCurrent.x - _ptStart.x;
	_tremAngle += fDistance / 20.0f;
	if(_tremAngle < -45.0f)
		_tremAngle = -45.0f;
	else if(_tremAngle > 45.0f)
		_tremAngle = 45.0f;

	_trem->setRotation(_tremAngle - 23.0f);
	_vagao1->setRotation(_tremAngle);
	_vagao2->setRotation(_tremAngle + 23.0f);

	_ptStart = ptCurrent;
}
void MenuScene::onTouchEnded(Touch* touch, Event* unused_event) {
	if(!_isTremMoved) {
		Vec2 ptCurrent = touch->getLocation();

		if(_vagao1->getBoundingBox().containsPoint(ptCurrent))
		{
			touchPiano();
		}
		else if(_vagao2->getBoundingBox().containsPoint(ptCurrent))
		{
			touchDram();
		}
		else if(vagao3->getBoundingBox().containsPoint(ptCurrent))
		{
		touchPiano();
		}
	}
	_isTremMoved = false;
}
void MenuScene::touchPiano() {
	auto pScene = PianoScene::scene();
	auto pTran = TransitionFade::create(0.5f, pScene);
	SimpleAudioEngine::getInstance()->stopAllEffects();
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("Effect/bt.ogg");
	Director::getInstance()->replaceScene(pTran);
}
void MenuScene::touchDram() {
	auto pScene = RecordScene::scene();
	auto pTran = TransitionFade::create(0.5f, pScene);
	SimpleAudioEngine::getInstance()->stopAllEffects();
	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("Effect/bt.ogg");
	Director::getInstance()->replaceScene(pTran);
}

Action* MenuScene::createStarAction(Vec2* pPos, int dirX, int dirY, float fActTime, float fDelayTime) {
	FiniteTimeAction* moveStar = Sequence::create(
		MoveTo::create(0.0f, Vec2(pPos->x + dirX * _margin, pPos->y + dirY * _margin)),
		Spawn::create(
			EaseInOut::create(FadeIn::create(fActTime), 2),
			MoveTo::create(fActTime, Vec2(pPos->x + dirX * _margin * 1.5f, pPos->y + dirY * _margin * 1.5f)),
			nullptr
		),
		Spawn::create(
			EaseInOut::create(FadeOut::create(fActTime), 2),
			MoveTo::create(fActTime, Vec2(pPos->x + dirX * _margin * 2.0f, pPos->y + dirY * _margin * 2.0f)),
			nullptr
		),
		DelayTime::create(fDelayTime),
		nullptr
	);
	return (Action*)RepeatForever::create((ActionInterval*)moveStar);
}

void MenuScene::onButtonTouch(Ref* pSender, Widget::TouchEventType type) {
	Button* pButton = (Button*)pSender;
	unsigned int tag = pButton->getTag();
	switch(type) {
	case Widget::TouchEventType::BEGAN:
		break;
	case Widget::TouchEventType::MOVED:
		break;
	case Widget::TouchEventType::ENDED:
		if(tag == TAG_BUTTON_INFO) {
			showInfo();
		} else if(tag == TAG_BUTTON_PROFILE) {
			showProfile();
		} else if(tag == TAG_BUTTON_PENTA) {
			showPenta();
		}
		break;
	case Widget::TouchEventType::CANCELED:
		break;
	default:
		break;
	}
}

void MenuScene::showInfo() {
	log("show info!");

	auto pScene = InfoScene::scene();
	auto pTran = TransitionMoveInB::create(0.5f, pScene);
	SimpleAudioEngine::getInstance()->stopAllEffects();
	SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	SimpleAudioEngine::getInstance()->playEffect("Effect/bt.ogg");

	Director::getInstance()->pushScene(pTran);

}
void MenuScene::showProfile() {
	log("show profile!");
	SonarCocosHelper::Facebook::Share("TEST SHARING","https://play.google.com/store/apps/details?id=com.weway.tuhu","Venha aprender música com o Tuhu","","");
	/*
	SimpleAudioEngine::getInstance()->playEffect("Effect/pop.ogg");
	JniMethodInfo methodInfo;
	if(!JniHelper::getStaticMethodInfo(methodInfo, "com/everyplay/Everyplay/Everyplay", "showEveryplay", "()Z")) {
		return;
	}

	methodInfo.env->CallStaticBooleanMethod(methodInfo.classID, methodInfo.methodID);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
	*/
}
void MenuScene::showPenta() {
	auto pScene = LearnScene::scene();
	auto pTran = TransitionFade::create(0.5f, pScene);
	SimpleAudioEngine::getInstance()->stopAllEffects();
	SimpleAudioEngine::getInstance()->playEffect("Effect/bt.ogg");
	Director::getInstance()->replaceScene(pTran);
}
void MenuScene::GoToMainMenuScene(cocos2d::Ref *sender)
{
   auto pScene = PianoScene::scene();
   	auto pTran = TransitionFade::create(0.5f, pScene);
   	SimpleAudioEngine::getInstance()->stopAllEffects();
   	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
   	SimpleAudioEngine::getInstance()->playEffect("Effect/bt.ogg");
   	Director::getInstance()->replaceScene(pTran);
}
void MenuScene::GoToSimonsGameScene(cocos2d::Ref *sender)
{
    auto scene = SequenceMemory::createScene();
    auto pTran = TransitionFade::create(0.5f, scene);
    Director::getInstance()->replaceScene(pTran);
}
void MenuScene::rotateWheels(float dt)
{
    r += -1;
    wheel1Vagao3->setRotation(r);
    wheel2Vagao3->setRotation(r);
}
void MenuScene::scaleCar3(float dt)
{
  /*  if(scaleDirection)
    {
        s+= 0.01f;
    }
    if(!scaleDirection)
    {
        s-= 0.05f;
    }
    playGenius->setScale(1 + s);
    if((1+s) > 1.1f)
    {
        scaleDirection = false;
    }
    if((1+s) < 1)
    {
        scaleDirection = true;
    }*/

}
void MenuScene::moveCloud(float dt) {
	auto cloud = Sprite::create("Menu/Ceu.png", Rect(0, 0, 242, 1024));
	cloud->setRotation(-90);
	float cloudY = _screenSize.height * 1.0f - cloud->getContentSize().width * 0.5f;
	cloudY = cloudY + cloud->getContentSize().width * CCRANDOM_0_1() * 0.75f;
	cloud->setPosition(Vec2(_screenSize.width + cloud->getContentSize().width, cloudY));
	this->addChild(cloud, 1);

	float duration = TIME_CLOUD_MOVE + 10 * CCRANDOM_0_1();

	auto moveTo = MoveTo::create(duration, Vec2(-1 * cloud->getContentSize().width * 2, cloudY));
	cloud->runAction(moveTo);
}
