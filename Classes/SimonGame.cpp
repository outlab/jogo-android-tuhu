#include "SimonGame.h"
#include "SimpleAudioEngine.h"
#include "SoundNoteaa.h"
#include "SoundNoteaa.cpp"
#include "MusicList.h"
#include "MusicList.cpp"
#include "MenuScene.h"
#include "SequenceMemory.h"

USING_NS_CC;
using namespace std;

float sizeProgress;
bool falouLegal;
float scaleValue;
bool L1L2;
bool waveIsVisible;
bool verifyScaleLimit;
int scaleWaveIn;
int scaleAux;
int numeroDeErros;
int numeroDeEstrelasPontos;
int musicNumber;
int instrumentNumber;
int armazenamentoTemporarioMusica;
int armazenamentoTemporarioInstrumentos;
float tempoUltimaNota;

vector<int> intervalosMusica01Cordas; //DANÇA DA CARRANQUINHA
vector<int> sequenciaNotas01Cordas;

vector<int> intervalosMusica02QDTM; //QUANTOS DIAS TEM O MES
vector<int> sequenciaNotas02QDTM;

vector<int> intervalosMusicaLNPDV; //LA NA PONTE DA VINHACA
vector<int> sequenciaNotasLNPDV;

vector<int> intervalosMusicaCondessa; //CONDESSA
vector<int> sequenciaNotasCondessa;

vector<int> intervalosMusicaNRua; //NESTA RUA
vector<int> sequenciaNotasNestaRua;

vector<int> intervalosMusicaAtche; //ATCHE
vector<int> sequenciaNotasAtche;

vector<int> sequenciaMusicaAtual;
vector<int> intervalosMusica;
vector<int> playerSequence;

Size frameSize;

SimonGame::SimonGame()
{
    L1L2 = false;
    //waveIsVisible = false;
    verifyScaleLimit = false;
    scaleWaveIn = 0;
    scaleValue = 0;
    tempoUltimaNota = 0;
    scaleAux = 1;
    sizeProgress = 0;
    falouLegal = false;
    paused = false;
    tuhuLegal = nullptr;
    congratStar1 = nullptr;
    congratStar2 = nullptr;
    congratStar3 = nullptr;
    muitoBem = nullptr;
    suaVez = nullptr;
    ops = nullptr;
    rightSwing = true;
    cortinaDir = nullptr;
    cortinaEsc = nullptr;
    tocaAudio = nullptr;
    animaCortina = nullptr;
    background = nullptr;
    firstPlay = true;
    doneSequence = false;
    wrongSound = false;
    finishedMusic = false;
    flagTextAngle = true;
    backButton = nullptr;
    numeroDeErros = 0;
    numeroDeEstrelasPontos = 0;
    intervalosMusicaLNPDV = {6,4,5,6,4,5};
    intervalosMusica01Cordas = {5,3,5,3,5,3,5,3};
    intervalosMusicaAtche = {4,4,4,5,4,4,4,5};
    intervalosMusica02QDTM = {4,4,4,3};
    intervalosMusicaNRua = {6,6,6,5,6,6,6,4,6,6,6,4};
    intervalosMusicaCondessa = {6,6,7,5,5,6,5,6,8,5,5,6};
    
//    C - 1
//    D - 2
//    E - 3
//    F - 4
//    G - 5
//    A - 6
//    B - 7
    sequenciaNotas02QDTM = {4,5,6,6,7,6,5,5,7,5,6,4,5,3,4};
    sequenciaNotas01Cordas = {5,1,1,1,1,5,7,7,7,7,7,7,1,7,6,5,5,6,6,6,6,5,7,7,7,7,7,7,3,2,1,1};
    sequenciaNotasAtche = {1,1,3,7,2,1,7,6,6,6,6,4,2,1,7,6,6,1,1,3,7,2,1,7,6,6,6,6,4,2,1,7,6,6};
    sequenciaNotasLNPDV = {3,4,5,3,1,6,5,3,1,6,5,3,4,2,3,3,4,5,3,1,6,5,3,1,6,5,3,4,2,1};
    sequenciaNotasCondessa = {4,5,6,4,7,6,5,3,5,5,5,7,6,5,4,3,3,4,4,6,7,1,2,7,7,7,7,2,1,7,6,5,5,6,6,6,7,1,7,2,1,7,5,7,7,7,2,1,7,6,5,5,6,6,6,7,1,2,7,7,7,7,2,1,7,6,5,5,4,4};
    sequenciaNotasNestaRua = {5,5,1,1,5,3,1,5,7,6,5,2,5,5,2,2,7,5,6,5,3,2,1,5,5,1,1,3,2,1,7,6,5,7,6,5,4,5,2,7,5,4,3,2,1,5,5,1,1,3,2,1,7,6,5,7,6,5,4,5,2,7,5,4,3,2,1};
    
    
    positionCheck = 0; // VERIFY THE SOUND POSITION INTO EACH BLOCK
    clickCount = 0;
    angleIncrement = 0;
}


Scene* SimonGame::createScene(int Musica,int StringOrBlow)
{
    armazenamentoTemporarioMusica = Musica;
    armazenamentoTemporarioInstrumentos = StringOrBlow;
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = SimonGame::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool SimonGame::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    musicNumber = armazenamentoTemporarioMusica;
    instrumentNumber = armazenamentoTemporarioInstrumentos;
    SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    _screenSize = Director::getInstance()->getWinSize();
    _ListaDeMusicas.init();

    auto glview = Director::getInstance()->getOpenGLView();
    frameSize = glview->getFrameSize();

    
    keyIndex = 0;
    keyIndexAux = 1;
    keySoundinBlock = 0; //GOES FROM 1 TO BLOCK SOUND NUMBER
    keyIndexFinal = 0; 

   /* __String* temp = __String::createWithFormat("%d - %d",keyIndex, positionCheck);

    texto = Label::createWithTTF(temp->getCString(),"fonts/Marker Felt.ttf",visibleSize.height*0.1);
    texto->setColor(Color3B::WHITE);
    texto->setPosition(origin.x + visibleSize.width/2,origin.y +visibleSize.height/2);
    this->addChild(texto,4);*/


    star1 = MenuItemImage::create("SimonGame/estrela_prata.png","SimonGame/estrela_dourada.png","SimonGame/estrela_dourada.png",CC_CALLBACK_1(SimonGame::playSoundStar1,this));
    star1->setAnchorPoint(Vec2(0.5,1.0));
    star1->setPosition(Vec2(origin.x + (_screenSize.width/6), origin.y + visibleSize.height + star1->getContentSize().height));// - star1->getContentSize().height));
    star1->setTag(1);
    star1->setScale(_screenSize.width* 0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        star1->setScaleY(0.45);
    menu1 = Menu::create(star1,NULL);
    menu1->setPosition(Point::ZERO);
    this->addChild(menu1,2);

    star2 = MenuItemImage::create("SimonGame/estrela_prata.png","SimonGame/estrela_dourada.png","SimonGame/estrela_dourada.png",CC_CALLBACK_1(SimonGame::playSoundStar2,this));
    star2->setAnchorPoint(Vec2(0.5,1.0));
    star2->setPosition(Vec2(origin.x + (_screenSize.width/6)*2, origin.y + visibleSize.height + star1->getContentSize().height));// - star1->getContentSize().height));
    star2->setTag(2);
    star2->setScale(_screenSize.width* 0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        star2->setScaleY(0.45);
    menu2 = Menu::create(star2,NULL);
    menu2->setPosition(Point::ZERO);
    this->addChild(menu2,2);

    star3 = MenuItemImage::create("SimonGame/estrela_prata.png","SimonGame/estrela_dourada.png","SimonGame/estrela_dourada.png",CC_CALLBACK_1(SimonGame::playSoundStar3,this));
    star3->setAnchorPoint(Vec2(0.5,1.0));
    star3->setPosition(Vec2(origin.x + (_screenSize.width/6)*3, origin.y + visibleSize.height + star1->getContentSize().height));// - star1->getContentSize().height));
    star3->setTag(3);
    star3->setScale(_screenSize.width* 0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        star3->setScaleY(0.45);
    menu3 = Menu::create(star3,NULL);
    menu3->setPosition(Point::ZERO);
    this->addChild(menu3,2);

    star4 = MenuItemImage::create("SimonGame/estrela_prata.png","SimonGame/estrela_dourada.png","SimonGame/estrela_dourada.png",CC_CALLBACK_1(SimonGame::playSoundStar4,this));
    star4->setAnchorPoint(Vec2(0.5,1.0));
    star4->setPosition(Vec2(origin.x + (_screenSize.width/6)*4, origin.y + visibleSize.height + star1->getContentSize().height));// - star1->getContentSize().height));
    star4->setTag(4);
    star4->setScale(_screenSize.width* 0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        star4->setScaleY(0.45);
    menu4 = Menu::create(star4,NULL);
    menu4->setPosition(Point::ZERO);
    this->addChild(menu4,1);

    star5 = MenuItemImage::create("SimonGame/estrela_prata.png","SimonGame/estrela_dourada.png","SimonGame/estrela_dourada.png",CC_CALLBACK_1(SimonGame::playSoundStar5,this));
    star5->setAnchorPoint(Vec2(0.5,1.0));
    star5->setPosition(Vec2(origin.x + (_screenSize.width/6)*5, origin.y + visibleSize.height + star1->getContentSize().height));// - star1->getContentSize().height));
    star5->setTag(5);
    star5->setScale(_screenSize.width* 0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        star5->setScaleY(0.45);
    menu5 = Menu::create(star5,NULL);
    menu5->setPosition(Point::ZERO);
    this->addChild(menu5,1);

    star6 = MenuItemImage::create("SimonGame/estrela_prata.png","SimonGame/estrela_dourada.png","SimonGame/estrela_dourada.png",CC_CALLBACK_1(SimonGame::playSoundStar6,this));
    star6->setAnchorPoint(Vec2(0.5,1.0));
    star6->setPosition(Vec2(origin.x + (_screenSize.width/6)*4, origin.y + visibleSize.height + star1->getContentSize().height));// - star1->getContentSize().height));
    star6->setTag(6);
    star6->setScale(_screenSize.width* 0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        star6->setScaleY(0.45);
    menu6 = Menu::create(star6,NULL);
    menu6->setPosition(Point::ZERO);
    this->addChild(menu6,1);

    star7 = MenuItemImage::create("SimonGame/estrela_prata.png","SimonGame/estrela_dourada.png","SimonGame/estrela_dourada.png",CC_CALLBACK_1(SimonGame::playSoundStar7,this));
    star7->setAnchorPoint(Vec2(0.5,1.0));
    star7->setPosition(Vec2(origin.x + (_screenSize.width/5)*5, origin.y + visibleSize.height + star1->getContentSize().height));// - star1->getContentSize().height));
    star7->setTag(7);
    star7->setScale(_screenSize.width* 0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        star7->setScaleY(0.45);
    menu7 = Menu::create(star7,NULL);
    menu7->setPosition(Point::ZERO);
    this->addChild(menu7,1);
    
    C = Sprite::create("SimonGame/keyImages/Do.png");
    D = Sprite::create("SimonGame/keyImages/Re.png");
    E = Sprite::create("SimonGame/keyImages/Mi.png");
    F = Sprite::create("SimonGame/keyImages/Fa.png");
    G = Sprite::create("SimonGame/keyImages/Sol.png");
    A = Sprite::create("SimonGame/keyImages/La.png");
    B = Sprite::create("SimonGame/keyImages/Si.png");
    
    auto positionKey = Vec2(_screenSize.width/2,_screenSize.height*.2);
    auto scaleKey = _screenSize.width*0.00035;
    
    C->setPosition(positionKey);
    D->setPosition(positionKey);
    E->setPosition(positionKey);
    F->setPosition(positionKey);
    G->setPosition(positionKey);
    A->setPosition(positionKey);
    B->setPosition(positionKey);
    
    C->setScale(scaleKey);
    E->setScale(scaleKey);
    F->setScale(scaleKey);
    G->setScale(scaleKey);
    A->setScale(scaleKey);
    B->setScale(scaleKey);
    D->setScale(scaleKey);
    
    C->setOpacity(0);
    D->setOpacity(0);
    E->setOpacity(0);
    F->setOpacity(0);
    G->setOpacity(0);
    A->setOpacity(0);
    B->setOpacity(0);

    this->addChild(C, 5);
    this->addChild(D, 5);
    this->addChild(E, 5);
    this->addChild(F, 5);
    this->addChild(G, 5);
    this->addChild(A, 5);
    this->addChild(B, 5);
    
    
    
    bright1 = Sprite::create("SimonGame/stars_halo.png");
    bright1->setPosition(Vec2(star1->getContentSize().width/2,star1->getContentSize().height*0.2));
    bright1->setScale(_screenSize.width* 0.0008);
    bright1->setOpacity(0);
    star1->addChild(bright1,-1);
    bright2 = Sprite::create("SimonGame/stars_halo.png");
    bright2->setPosition(Vec2(star1->getContentSize().width/2,star1->getContentSize().height*0.2));
    bright2->setScale(_screenSize.width* 0.0008);
    bright2->setOpacity(0);
    star2->addChild(bright2,-1);
    bright3 = Sprite::create("SimonGame/stars_halo.png");
    bright3->setPosition(Vec2(star1->getContentSize().width/2,star1->getContentSize().height*0.2));
    bright3->setScale(_screenSize.width* 0.0008);
    bright3->setOpacity(0);
    star3->addChild(bright3,-1);
    bright4 = Sprite::create("SimonGame/stars_halo.png");
    bright4->setPosition(Vec2(star1->getContentSize().width/2,star1->getContentSize().height*0.2));
    bright4->setScale(_screenSize.width* 0.0008);
    bright4->setOpacity(0);
    star4->addChild(bright4,-1);
    bright5 = Sprite::create("SimonGame/stars_halo.png");
    bright5->setPosition(Vec2(star1->getContentSize().width/2,star1->getContentSize().height*0.2));
    bright5->setScale(_screenSize.width* 0.0008);
    bright5->setOpacity(0);
    star5->addChild(bright5,-1);
    bright6 = Sprite::create("SimonGame/stars_halo.png");
    bright6->setPosition(Vec2(star1->getContentSize().width/2,star1->getContentSize().height*0.2));
    bright6->setScale(_screenSize.width* 0.0008);
    bright6->setOpacity(0);
    star6->addChild(bright6,-1);
    bright7 = Sprite::create("SimonGame/stars_halo.png");
    bright7->setPosition(Vec2(star1->getContentSize().width/2,star1->getContentSize().height*0.2));
    bright7->setScale(_screenSize.width* 0.0008);
    bright7->setOpacity(0);
    star7->addChild(bright7,-1);
    
    tuhuLegal = Sprite::create("SimonGame/legal.png");
    tuhuLegal->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + _screenSize.height/2));
    tuhuLegal->setScale(0);
    this->addChild(tuhuLegal,5);

    cortinaEsc = Sprite::create("SimonGame/cortina.png");
    cortinaEsc->setAnchorPoint(Vec2(0.5f,1));
    cortinaEsc->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height));
    cortinaEsc->setScale(_screenSize.width/(cortinaEsc->getContentSize().width));
    this->addChild(cortinaEsc,2);

    cortinaDir = Sprite::create("SimonGame/cortina.png");
    cortinaDir->setAnchorPoint(Vec2(0.5f,1));
    cortinaDir->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height));
    cortinaDir->setScale((_screenSize.width/(cortinaDir->getContentSize().width)));
    cortinaDir->setScaleX(-1);
    this->addChild(cortinaDir,2);

    background = Sprite::create("SimonGame/bg_palco.jpg");
    background->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2));
    background->setScale(_screenSize.width/(background->getContentSize().width));
    this->addChild(background);

    cortinas = Sprite::create("SubMenuSong/cortina.png");
    cortinas->setAnchorPoint(Vec2(0.5f,1));
    cortinas->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height));
    cortinas->setScale(_screenSize.width/(cortinas->getContentSize().width));
    this->addChild(cortinas,3);
    
    blackBG = Sprite::create("SimonGame/preto.png");
    blackBG->setAnchorPoint(Vec2(0.5f,1));
    blackBG->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height));
    blackBG->setScale(_screenSize.width/blackBG->getContentSize().width);
    blackBG->setScaleY(1.2);
    blackBG->setOpacity(0);
    this->addChild(blackBG,4);
    
    bar = Sprite::create("SimonGame/progre.png");
    bar->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height/11));
    bar->setScale(_screenSize.width*0.00045);
    if(frameSize.width/frameSize.height < 1.5)
        bar->setScaleY(0.45);
    this->addChild(bar,3);
    
    
    
    backButton = MenuItemImage::create("SimonGame/pause.png","SimonGame/pause.png",CC_CALLBACK_1(SimonGame::pauseGame,this));
    backButton->setPosition(Vec2(_screenSize.width*0.0005 + backButton->getContentSize().width/2.4, origin.y + visibleSize.height - _screenSize.height*0.013 -backButton->getContentSize().height/2.25));
    backButton->setScale(_screenSize.width*0.00045);
    /*if(frameSize.width >= 1024)
        backButton->setScaleY(0.5);*/
    auto menuBackButton = Menu::create(backButton,NULL);
    menuBackButton->setPosition(Point::ZERO);
    this->addChild(menuBackButton,6);    
    
    playButton = MenuItemImage::create("SimonGame/play.png","SimonGame/play.png",CC_CALLBACK_1(SimonGame::resumeGame,this));
    playButton->setPosition(Vec2(_screenSize.width*0.0005 + backButton->getContentSize().width/2.4, origin.y + visibleSize.height - _screenSize.height*0.013 -playButton->getContentSize().height/2.25));
    playButton->setScale(0);
   // if(frameSize.width >= 1024)
       // playButton->setScaleY(0.45);
    auto menuplayButton = Menu::create(playButton,NULL);
    menuplayButton->setPosition(Point::ZERO);
    this->addChild(menuplayButton,5);

    nextTrack = MenuItemImage::create("SimonGame/botaoReplay.png","SimonGame/botaoReplay.png",CC_CALLBACK_1(SimonGame::loadThisScene,this));
    nextTrack->setPosition(Vec2(_screenSize.width*0.1 + backButton->getContentSize().width/2, origin.y + visibleSize.height - _screenSize.height*0.013 -backButton->getContentSize().height/2.25));
    nextTrack->setScale(0);
    if(frameSize.width/frameSize.height < 1.5)
        star1->setScaleY(0.45);
    auto menuTrackButton = Menu::create(nextTrack,NULL);
    menuTrackButton->setPosition(Point::ZERO);
    this->addChild(menuTrackButton,5);
    
    selectInstruments = MenuItemImage::create("SimonGame/sInstrumentos.png","SimonGame/sInstrumentos.png",CC_CALLBACK_1(SimonGame::changeInstrument,this));
    selectInstruments->setPosition(Vec2(_screenSize.width*0.2 + backButton->getContentSize().width/2, origin.y + visibleSize.height - _screenSize.height*0.013 -backButton->getContentSize().height/2.25));
    selectInstruments->setScale(0);
    if(frameSize.width/frameSize.height < 1.5)
        selectInstruments->setScaleY(0.45);
    auto menuInstrumentButton = Menu::create(selectInstruments,NULL);
    menuInstrumentButton->setPosition(Point::ZERO);
    this->addChild(menuInstrumentButton,5);
    
    selectMusic = MenuItemImage::create("SimonGame/botaoTelamusicas.png","SimonGame/botaoTelamusicas.png",CC_CALLBACK_1(SimonGame::backMusicScene,this));
    selectMusic->setPosition(Vec2(_screenSize.width*0.3 + backButton->getContentSize().width/2, origin.y + visibleSize.height - _screenSize.height*0.013 -backButton->getContentSize().height/2.25));
    selectMusic->setScale(0);
    if(frameSize.width/frameSize.height < 1.5)
        selectMusic->setScaleY(0.45);
    auto menuselectMusicButton = Menu::create(selectMusic,NULL);
    menuselectMusicButton->setPosition(Point::ZERO);
    this->addChild(menuselectMusicButton,5);
    
    goMenu = MenuItemImage::create("SubMenuSong/botaomenu.png","SubMenuSong/botaomenu.png",CC_CALLBACK_1(SimonGame::loadMenuScene,this));
    goMenu->setPosition(Vec2(_screenSize.width*0.4 + backButton->getContentSize().width/2, origin.y + visibleSize.height - _screenSize.height*0.013 -backButton->getContentSize().height/2.25));
    goMenu->setScale(0);
    if(frameSize.width/frameSize.height < 1.5)
        goMenu->setScaleY(0.45);
    auto menugoMenuButton = Menu::create(goMenu,NULL);
    menugoMenuButton->setPosition(Point::ZERO);
    this->addChild(menugoMenuButton,5);

    WoodenPlate = Sprite::create("SimonGame/WoodenPlate.png");
    WoodenPlate->setAnchorPoint(Vec2(0.5,1.0));
    WoodenPlate->setPosition(Vec2(origin.x + _screenSize.width/2, origin.y + _screenSize.height + WoodenPlate->getContentSize().height));
    WoodenPlate->setScale(_screenSize.width*0.00045);
    if(frameSize.width/frameSize.height < 1.5)
        WoodenPlate->setScaleY(0.45);
    this->addChild(WoodenPlate,4);
    
    continuePraticando = Sprite::create("SimonGame/ContinuePraticando.png");
    continuePraticando->setAnchorPoint(Vec2(0.5,1.0));
    continuePraticando->setPosition(Vec2(origin.x + _screenSize.width/2, origin.y + _screenSize.height + WoodenPlate->getContentSize().height));
    continuePraticando->setScale(_screenSize.width*0.00045);
    if(frameSize.width/frameSize.height < 1.5)
        continuePraticando->setScaleY(0.45);
    this->addChild(continuePraticando,4);
    
    muitoBemP = Sprite::create("SimonGame/muitoBom.png");
    muitoBemP->setAnchorPoint(Vec2(0.5,1.0));
    muitoBemP->setPosition(Vec2(origin.x + _screenSize.width/2, origin.y + _screenSize.height + muitoBemP->getContentSize().height));
    muitoBemP->setScale(_screenSize.width*0.00045);
    if(frameSize.width/frameSize.height < 1.5)
        muitoBemP->setScaleY(0.45);
    this->addChild(muitoBemP,4);

    replayButton = MenuItemImage::create("SimonGame/botaoReplay.png","SimonGame/botaoReplay.png",CC_CALLBACK_1(SimonGame::loadThisScene,this));
    replayButton->setPosition(Vec2(WoodenPlate->getContentSize().width/2, WoodenPlate->getContentSize().height*0.05));
    replayButton->setScale(_screenSize.width*0.00075);
    auto menureplayButton = Menu::create(replayButton,NULL);
    menureplayButton->setPosition(Point::ZERO);
    WoodenPlate->addChild(menureplayButton,2);

    backButton2 = MenuItemImage::create("SubMenuSong/botaomenu.png","SubMenuSong/botaomenu.png",CC_CALLBACK_1(SimonGame::loadMenuScene,this));
    backButton2->setPosition(Vec2(WoodenPlate->getContentSize().width*0.15, WoodenPlate->getContentSize().height*0.05));
    backButton2->setScale(_screenSize.width*0.00075);
    auto menuBack = Menu::create(backButton2,NULL);
    menuBack->setPosition(Point::ZERO);
    WoodenPlate->addChild(menuBack,2);

    nextTrack2 = MenuItemImage::create("SimonGame/botaoTelamusicas.png","SimonGame/botaoTelamusicas.png",CC_CALLBACK_1(SimonGame::backMusicScene,this));
    nextTrack2->setPosition(Vec2(WoodenPlate->getContentSize().width - WoodenPlate->getContentSize().width*0.15, WoodenPlate->getContentSize().height*0.05));
    nextTrack2->setScale(_screenSize.width*0.00075);
    auto menuTrack = Menu::create(nextTrack2,NULL);
    menuTrack->setPosition(Point::ZERO);
    WoodenPlate->addChild(menuTrack,2);
    
    replayButton2 = MenuItemImage::create("SimonGame/botaoReplay.png","SimonGame/botaoReplay.png",CC_CALLBACK_1(SimonGame::loadThisScene,this));
    replayButton2->setPosition(Vec2(continuePraticando->getContentSize().width/2, continuePraticando->getContentSize().height*0.05));
    replayButton2->setScale(_screenSize.width*0.00075);
    auto menureplayButton2 = Menu::create(replayButton2,NULL);
    menureplayButton2->setPosition(Point::ZERO);
    continuePraticando->addChild(menureplayButton2,2);
    
    backButton3 = MenuItemImage::create("SubMenuSong/botaomenu.png","SubMenuSong/botaomenu.png",CC_CALLBACK_1(SimonGame::loadMenuScene,this));
    backButton3->setPosition(Vec2(continuePraticando->getContentSize().width*0.15, continuePraticando->getContentSize().height*0.05));
    backButton3->setScale(_screenSize.width*0.00075);
    auto menuBack2 = Menu::create(backButton3,NULL);
    menuBack2->setPosition(Point::ZERO);
    continuePraticando->addChild(menuBack2,2);
    
    nextTrack3 = MenuItemImage::create("SimonGame/botaoTelamusicas.png","SimonGame/botaoTelamusicas.png",CC_CALLBACK_1(SimonGame::backMusicScene,this));
    nextTrack3->setPosition(Vec2(continuePraticando->getContentSize().width - continuePraticando->getContentSize().width*0.15, WoodenPlate->getContentSize().height*0.05));
    nextTrack3->setScale(_screenSize.width*0.00075);
    auto menuTrack2 = Menu::create(nextTrack3,NULL);
    menuTrack2->setPosition(Point::ZERO);
    continuePraticando->addChild(menuTrack2,2);
    
    replayButton3 = MenuItemImage::create("SimonGame/botaoReplay.png","SimonGame/botaoReplay.png",CC_CALLBACK_1(SimonGame::loadThisScene,this));
    replayButton3->setPosition(Vec2(muitoBemP->getContentSize().width/2, muitoBemP->getContentSize().height*0.05));
    replayButton3->setScale(_screenSize.width*0.00075);
    auto menureplayButton3 = Menu::create(replayButton3,NULL);
    menureplayButton3->setPosition(Point::ZERO);
    muitoBemP->addChild(menureplayButton3,2);
    
    backButton4 = MenuItemImage::create("SubMenuSong/botaomenu.png","SubMenuSong/botaomenu.png",CC_CALLBACK_1(SimonGame::loadMenuScene,this));
    backButton4->setPosition(Vec2(muitoBemP->getContentSize().width*0.15, muitoBemP->getContentSize().height*0.05));
    backButton4->setScale(_screenSize.width*0.00075);
    auto menuBack3 = Menu::create(backButton4,NULL);
    menuBack3->setPosition(Point::ZERO);
    muitoBemP->addChild(menuBack3,2);
    
    nextTrack4 = MenuItemImage::create("SimonGame/botaoTelamusicas.png","SimonGame/botaoTelamusicas.png",CC_CALLBACK_1(SimonGame::backMusicScene,this));
    nextTrack4->setPosition(Vec2(muitoBemP->getContentSize().width - muitoBemP->getContentSize().width*0.15, WoodenPlate->getContentSize().height*0.05));
    nextTrack4->setScale(_screenSize.width*0.00075);
    auto menuTrack3 = Menu::create(nextTrack4,NULL);
    menuTrack3->setPosition(Point::ZERO);
    muitoBemP->addChild(menuTrack3,2);

    muitoBem = Sprite::create("SimonGame/MuitoBem.png");
    muitoBem->setPosition(Vec2(origin.x + _screenSize.width/2,origin.y + _screenSize.height - _screenSize.height/5));
    muitoBem->setScale(_screenSize.width*0.00045);
    muitoBem->setOpacity(0);
    this->addChild(muitoBem,-1);

    ops = Sprite::create("SimonGame/Ops.png");
    ops->setPosition(Vec2(origin.x + _screenSize.width/2,origin.y + _screenSize.height - _screenSize.height/5));
    ops->setScale(_screenSize.width*0.00045);
    ops->setOpacity(0);
    this->addChild(ops,-1);

    suaVez = Sprite::create("SimonGame/SuaVez.png");
    suaVez->setPosition(Vec2(origin.x + (_screenSize.width/5)*3 + (_screenSize.width/5)/2,origin.y + _screenSize.height - _screenSize.height/5));
    suaVez->setScale(_screenSize.width*0.00045);
    suaVez->setOpacity(0);
    this->addChild(suaVez,-5);

    congratStar1 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar1->setPosition(Vec2(WoodenPlate->getContentSize().width*0.277, WoodenPlate->getContentSize().height*0.22));
    congratStar1->setOpacity(0);
    congratStar1->setScale(_screenSize.width*0.00075);
    WoodenPlate->addChild(congratStar1,2);

    congratStar2 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar2->setPosition(Vec2(WoodenPlate->getContentSize().width/1.935, WoodenPlate->getContentSize().height*0.265));
    congratStar2->setOpacity(0);
    congratStar2->setScale(_screenSize.width*0.00085);
    WoodenPlate->addChild(congratStar2,2);

    congratStar3 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar3->setPosition(Vec2(WoodenPlate->getContentSize().width - WoodenPlate->getContentSize().width*0.2284, WoodenPlate->getContentSize().height*0.22));
    congratStar3->setOpacity(0);
    congratStar3->setScale(_screenSize.width*0.00075);
    WoodenPlate->addChild(congratStar3,2);
    
    congratStar1_2 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar1_2->setPosition(Vec2(continuePraticando->getContentSize().width*0.277, continuePraticando->getContentSize().height*0.22));
    congratStar1_2->setOpacity(0);
    congratStar1_2->setScale(_screenSize.width*0.00075);
    continuePraticando->addChild(congratStar1_2,2);
    
    congratStar2_2 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar2_2->setPosition(Vec2(continuePraticando->getContentSize().width/1.935, continuePraticando->getContentSize().height*0.265));
    congratStar2_2->setOpacity(0);
    congratStar2_2->setScale(_screenSize.width*0.00085);
    continuePraticando->addChild(congratStar2_2,2);
    
    congratStar3_2 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar3_2->setPosition(Vec2(continuePraticando->getContentSize().width - continuePraticando->getContentSize().width*0.2284, WoodenPlate->getContentSize().height*0.22));
    congratStar3_2->setOpacity(0);
    congratStar3_2->setScale(_screenSize.width*0.00075);
    continuePraticando->addChild(congratStar3_2,2);
    
    congratStar1_3 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar1_3->setPosition(Vec2(muitoBemP->getContentSize().width*0.277, muitoBemP->getContentSize().height*0.22));
    congratStar1_3->setOpacity(0);
    congratStar1_3->setScale(_screenSize.width*0.00075);
    muitoBemP->addChild(congratStar1_3,2);
    
    congratStar2_3 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar2_3->setPosition(Vec2(muitoBemP->getContentSize().width/1.935, muitoBemP->getContentSize().height*0.265));
    congratStar2_3->setOpacity(0);
    congratStar2_3->setScale(_screenSize.width*0.00085);
    muitoBemP->addChild(congratStar2_3,2);
    
    congratStar3_3 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar3_3->setPosition(Vec2(muitoBemP->getContentSize().width - muitoBemP->getContentSize().width*0.2284, muitoBemP->getContentSize().height*0.22));
    congratStar3_3->setOpacity(0);
    congratStar3_3->setScale(_screenSize.width*0.00075);
    muitoBemP->addChild(congratStar3_3,2);

    blackFadeReplayBotton = Sprite::create("SimonGame/blurFade.png");
    blackFadeReplayBotton->setPosition(Vec2(_screenSize.width/2, blackFadeReplayBotton->getContentSize().height/4));
    blackFadeReplayBotton->setScaleX(_screenSize.width/blackFadeReplayBotton->getContentSize().width);
    blackFadeReplayBotton->setScaleY(((1.2*_screenSize.height)/3)/blackFadeReplayBotton->getContentSize().height);
    blackFadeReplayBotton->setOpacity(0);
    this->addChild(blackFadeReplayBotton,3);

    goldenLine = Sprite::create("SimonGame/GoldenLine.png");
    goldenLine->setPosition(Vec2(_screenSize.width/2,(0.25*_screenSize.height)/5));
    goldenLine->setScaleX((_screenSize.width)/goldenLine->getContentSize().width);
    goldenLine->setScaleY(_screenSize.height/(10*goldenLine->getContentSize().height));
    goldenLine->setVisible(false);
    this->addChild(goldenLine,4);
    goldenLine2 = Sprite::create("SimonGame/GoldenLine.png");
    goldenLine2->setPosition(Vec2(-(.99*_screenSize.width)/2,(0.25*_screenSize.height)/5));
    goldenLine2->setScaleX((_screenSize.width)/goldenLine->getContentSize().width);
    goldenLine2->setScaleY(_screenSize.height/(10*goldenLine->getContentSize().height));
    goldenLine2->setVisible(false);
    this->addChild(goldenLine2,4);
    
//    blackFadeReplayBotton = Sprite::create("SimonGame/blurFade.png");
//    blackFadeReplayBotton->setPosition(Vec2(_screenSize.width/2, blackFadeReplayBotton->getContentSize().height/4));
//    blackFadeReplayBotton->setScaleX(_screenSize.width/blackFadeReplayBotton->getContentSize().width);
//    blackFadeReplayBotton->setScaleY(((1.2*_screenSize.height)/3)/blackFadeReplayBotton->getContentSize().height);
//    blackFadeReplayBotton->setOpacity(0);
//    this->addChild(blackFadeReplayBotton,3);

    
    if(musicNumber == 1) //QUANTOS DIAS TEM O MÊS
    {
        _Musica = _ListaDeMusicas.getMusica(musicNumber, instrumentNumber);
        intervalosMusica = intervalosMusica02QDTM;
        sequenciaMusicaAtual = sequenciaNotas02QDTM;
        origemStar1 = Vec2(origin.x + (_screenSize.width/6)*1, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar2 = Vec2(origin.x + (_screenSize.width/6)*2, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar3 = Vec2(origin.x + (_screenSize.width/6)*3, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar4 = Vec2(origin.x + (_screenSize.width/6)*4, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar5 = Vec2(origin.x + (_screenSize.width/6)*5, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar6 = Vec2(origin.x + (_screenSize.width/6)*5, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar7 = Vec2(origin.x + (_screenSize.width/5)*4, origin.y + visibleSize.height + star1->getContentSize().height);
    }
    else if(musicNumber == 2) // LA NA PONTE DA VINHAÇA
    {
        _Musica = _ListaDeMusicas.getMusica(musicNumber, instrumentNumber);
        intervalosMusica = intervalosMusicaLNPDV;
        sequenciaMusicaAtual = sequenciaNotasLNPDV;
        origemStar1 = Vec2(origin.x + (_screenSize.width/6)*1, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar2 = Vec2(origin.x + (_screenSize.width/6)*2, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar3 = Vec2(origin.x + (_screenSize.width/6)*3, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar4 = Vec2(origin.x + (_screenSize.width/6)*4, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar5 = Vec2(origin.x + (_screenSize.width/6)*5, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar6 = Vec2(origin.x + (_screenSize.width/6)*5, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar7 = Vec2(origin.x + (_screenSize.width/5)*4, origin.y + visibleSize.height + star1->getContentSize().height);
    }
    else if(musicNumber == 3) // A DANÇA DA CARRANQUINHA
    {
        _Musica = _ListaDeMusicas.getMusica(musicNumber, instrumentNumber);
        intervalosMusica = intervalosMusica01Cordas;
        sequenciaMusicaAtual = sequenciaNotas01Cordas;
        origemStar1 = Vec2(origin.x + (_screenSize.width/6)*1, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar2 = Vec2(origin.x + (_screenSize.width/6)*2, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar3 = Vec2(origin.x + (_screenSize.width/6)*3, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar4 = Vec2(origin.x + (_screenSize.width/6)*4, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar5 = Vec2(origin.x + (_screenSize.width/6)*5, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar6 = Vec2(origin.x + (_screenSize.width/7)*6, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar7 = Vec2(origin.x + (_screenSize.width/5)*4, origin.y + visibleSize.height + star1->getContentSize().height);
    }
    else if(musicNumber == 4) //ATCHE
    {
        _Musica = _ListaDeMusicas.getMusica(musicNumber, instrumentNumber);
        intervalosMusica = intervalosMusicaAtche;
        sequenciaMusicaAtual = sequenciaNotasAtche;
        origemStar1 = Vec2(origin.x + (_screenSize.width/7)*1, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar2 = Vec2(origin.x + (_screenSize.width/7)*2, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar3 = Vec2(origin.x + (_screenSize.width/7)*3, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar4 = Vec2(origin.x + (_screenSize.width/6)*4, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar5 = Vec2(origin.x + (_screenSize.width/7)*5, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar6 = Vec2(origin.x + (_screenSize.width/7)*6, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar7 = Vec2(origin.x + (_screenSize.width/5)*4, origin.y + visibleSize.height + star1->getContentSize().height);
    }
    else if(musicNumber == 5) //NESTA RUA
    {
        _Musica = _ListaDeMusicas.getMusica(musicNumber, instrumentNumber);
        intervalosMusica = intervalosMusicaNRua;
        sequenciaMusicaAtual = sequenciaNotasNestaRua;
        origemStar1 = Vec2(origin.x + (_screenSize.width/6)*1, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar2 = Vec2(origin.x + (_screenSize.width/6)*2, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar3 = Vec2(origin.x + (_screenSize.width/6)*3, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar4 = Vec2(origin.x + (_screenSize.width/6)*4, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar5 = Vec2(origin.x + (_screenSize.width/6)*5, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar6 = Vec2(origin.x + (_screenSize.width/6)*2, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar7 = Vec2(origin.x + (_screenSize.width/6)*4, origin.y + visibleSize.height + star1->getContentSize().height);
    }
    else if(musicNumber == 6) //CONDESSA
    {
        _Musica = _ListaDeMusicas.getMusica(musicNumber, instrumentNumber);
        intervalosMusica = intervalosMusicaCondessa;
        sequenciaMusicaAtual = sequenciaNotasCondessa;
        origemStar1 = Vec2(origin.x + (_screenSize.width/6)*1, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar2 = Vec2(origin.x + (_screenSize.width/6)*2, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar3 = Vec2(origin.x + (_screenSize.width/6)*3, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar4 = Vec2(origin.x + (_screenSize.width/6)*4, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar5 = Vec2(origin.x + (_screenSize.width/6)*5, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar6 = Vec2(origin.x + (_screenSize.width/6)*6, origin.y + visibleSize.height + star1->getContentSize().height);
        origemStar7 = Vec2(origin.x + (_screenSize.width/6)*4, origin.y + visibleSize.height + star1->getContentSize().height);      
    }
    
     pro = Sprite::create("SimonGame/pro.png");
    pro->setPosition(Vec2(origin.x + visibleSize.width/2 - bar->getContentSize().width/3.63, origin.y + visibleSize.height/11));
    pro->setScaleY(_screenSize.width*0.0007);
    pro->setAnchorPoint(Vec2(0.0,0.5));
    this->addChild(pro,3);
    
    bbola = Sprite::create("SimonGame/brilhobola.png");
    bbola->setPosition(Vec2(origin.x + visibleSize.width/2 + bar->getContentSize().width/3.67, origin.y + visibleSize.height/11));
    bbola->setScale(0);
    bbola->setOpacity(0);
    this->addChild(bbola,3);
    bola = Sprite::create("SimonGame/bola.png");
    bola->setPosition(Vec2(origin.x + visibleSize.width/2 + bar->getContentSize().width/3.695, origin.y + visibleSize.height/11));
    bola->setScale(_screenSize.width*0.00045);
    if(frameSize.width/frameSize.height < 1.5)
        bola->setScaleY(0.45);

    bola->setOpacity(0);
    this->addChild(bola,3);
    bola2 = Sprite::create("SimonGame/bola.png");
    bola2->setPosition(Vec2(origin.x + visibleSize.width/2 - bar->getContentSize().width/3.54, origin.y + visibleSize.height/11));
    bola2->setScale(_screenSize.width*0.00045);
    if(frameSize.width/frameSize.height < 1.5)
        bola2->setScaleY(0.45);

    this->addChild(bola2,3);
    
    vermelho = Sprite::create("SimonGame/vermelho.png");
    vermelho->setPosition(Vec2(origin.x + visibleSize.width/2 , origin.y + visibleSize.height/2));
    vermelho->setOpacity(0);
    vermelho->setScale(_screenSize.width/blackBG->getContentSize().width);
    //vermelho->setScaleY(1.0005);
    this->addChild(vermelho,7);
    dourado = Sprite::create("SimonGame/dourado.png");
    dourado->setPosition(Vec2(-origin.x + visibleSize.width/2 , origin.y + visibleSize.height/2));
    dourado->setOpacity(0);
    dourado->setScale(_screenSize.width/blackBG->getContentSize().width);
    //dourado->setScaleY(1.0005);
    this->addChild(dourado,7);

    star1->setPosition(origemStar1);
    star2->setPosition(origemStar2);
    star3->setPosition(origemStar3);
    star4->setPosition(origemStar4);
    star5->setPosition(origemStar5);
    star6->setPosition(origemStar6);
    star7->setPosition(origemStar7);

    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("Instruments/RightSequence.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("Instruments/WrongSequence.mp3");

    this->scheduleOnce(schedule_selector(SimonGame::Animate),0);
    this->scheduleOnce(schedule_selector(SimonGame::manageSound),2);
    this->scheduleOnce(schedule_selector(SimonGame::animateStarShake),0);
    //this->schedule(schedule_selector(SimonGame::animateWave),0.01f);

    return true;
}
void SimonGame::scaleProgress()
{
    sizeProgress = ((origin.x + visibleSize.width/2 + bar->getContentSize().width/3.63) - (origin.x + visibleSize.width/2 - bar->getContentSize().width/3.63))/intervalosMusica.size();
    pro->setScaleX((sizeProgress*scaleAux)/pro->getContentSize().width);
    scaleAux +=1;
    if(finishedMusic)
    {
        bola->runAction(FadeIn::create(0.07));
        bbola->runAction(Sequence::create(FadeIn::create(0.07),EaseElasticOut::create(ScaleTo::create(0.1, _screenSize.width*0.00045)),nullptr));
    }
}
void SimonGame::changeInstrument(Ref* pSender)
{
    if(armazenamentoTemporarioInstrumentos == 1)
    {
        auto scene = SequenceMemory::createScene(armazenamentoTemporarioMusica,1,true);
        Director::getInstance()->replaceScene(scene);
    }
    else
    {
        auto scene = SequenceMemory::createScene(armazenamentoTemporarioMusica,2,true);
        Director::getInstance()->replaceScene(scene);
    }
}
void SimonGame::pauseGame(Ref* pSender)
{
    paused = true;
    auto fadeIn = FadeIn::create(0.5);
    auto scaleB1 = ScaleTo::create(0.5, _screenSize.width*0.00045);
    auto scaleB2 = ScaleTo::create(0.5, _screenSize.width*0.0004);
    auto scaleB3 = ScaleTo::create(0.5, _screenSize.width*0.0004);
    auto scaleB4 = ScaleTo::create(0.5, _screenSize.width*0.0004);
    auto scaleB5 = ScaleTo::create(0.5, _screenSize.width*0.0004);
    
  /*  if(frameSize.width >= 1024)
    {
        scaleB1 = ScaleTo::create(0.5, _screenSize.width*0.00045, 0.5f);
        scaleB2 = ScaleTo::create(0.5, _screenSize.width*0.0004, 0.45f);
        scaleB3 = ScaleTo::create(0.5, _screenSize.width*0.0004, 0.45f);
        scaleB4 = ScaleTo::create(0.5, _screenSize.width*0.0004, 0.45f);
        scaleB5 = ScaleTo::create(0.5, _screenSize.width*0.0004, 0.45f);
    }
    else
    {
        scaleB1 = ScaleTo::create(0.5, _screenSize.width*0.00045);
        scaleB2 = ScaleTo::create(0.5, _screenSize.width*0.0004);
        scaleB3 = ScaleTo::create(0.5, _screenSize.width*0.0004);
        scaleB4 = ScaleTo::create(0.5, _screenSize.width*0.0004);
        scaleB5 = ScaleTo::create(0.5, _screenSize.width*0.0004);
    }*/
 
    blackBG->runAction(Sequence::create(fadeIn,nullptr));
    backButton->runAction(EaseElasticOut::create(ScaleTo::create(0.1, 0)));
    selectInstruments->runAction(Sequence::create(DelayTime::create(0.6),EaseElasticOut::create(scaleB3),nullptr));
    nextTrack->runAction(Sequence::create(DelayTime::create(0.45),EaseElasticOut::create(scaleB2),nullptr));
    selectMusic->runAction(Sequence::create(DelayTime::create(0.75),EaseElasticOut::create(scaleB4),nullptr));
    goMenu->runAction(Sequence::create(DelayTime::create(0.9),EaseElasticOut::create(scaleB5),nullptr));
    playButton->runAction(Sequence::create(DelayTime::create(0.3),EaseElasticOut::create(scaleB1),nullptr));
}
void SimonGame::resumeGame(Ref* pSender)
{
    paused = false;
    auto fadeOut = FadeOut::create(0.5);
    auto scaleB1 = ScaleTo::create(0.5, _screenSize.width*0.00045);
  /*  if(frameSize.width >= 1024)
        scaleB1 = ScaleTo::create(0.5, _screenSize.width*0.00045,0.5);
    else
         scaleB1 = ScaleTo::create(0.5, _screenSize.width*0.00045);*/
    auto scaleB2 = ScaleTo::create(0.5, 0);
    auto scaleB3 = ScaleTo::create(0.5, 0);
    auto scaleB4 = ScaleTo::create(0.5, 0);
    auto scaleB5 = ScaleTo::create(0.5, 0);
    auto returnSong = CallFunc::create([this]()
                                       {
                                           if(!doneSequence)
                                               SimonGame::manageSound(0);
                                       });
    blackBG->runAction(Sequence::create(fadeOut,nullptr));
    backButton->runAction(Sequence::create(DelayTime::create(0.1),EaseElasticOut::create(scaleB1),DelayTime::create(0.005f),nullptr));
    selectInstruments->runAction(Sequence::create(DelayTime::create(0.5),EaseElasticOut::create(scaleB3),nullptr));
    nextTrack->runAction(Sequence::create(DelayTime::create(0.35),EaseElasticOut::create(scaleB2),nullptr));
    selectMusic->runAction(Sequence::create(DelayTime::create(0.65),EaseElasticOut::create(scaleB4),nullptr));
    goMenu->runAction(Sequence::create(DelayTime::create(0.8),EaseElasticOut::create(scaleB5),returnSong,nullptr));
    playButton->runAction(Sequence::create(DelayTime::create(0.2),EaseElasticOut::create(ScaleTo::create(0.1,0)),nullptr));
}
void SimonGame::loadMenuScene(Ref* pSender)
{
    auto scene = MenuScene::scene();
    Director::getInstance()->replaceScene(scene);
}
void SimonGame::loadNext(Ref* pSender)
{
    auto scene = SequenceMemory::createScene();
    Director::getInstance()->replaceScene(scene);
}
void SimonGame::backMusicScene(Ref* pSender)
{
    auto scene = SequenceMemory::createCustomScene(armazenamentoTemporarioMusica,visibleSize);
    Director::getInstance()->replaceScene(scene);
}
void SimonGame::loadThisScene(Ref* pSender)
{
    auto scene = SimonGame::createScene(armazenamentoTemporarioMusica,armazenamentoTemporarioInstrumentos);
    Director::getInstance()->replaceScene(scene);
}

void SimonGame::animateKeys() {
    
   // auto showWave = CallFunc::create([this]()
    //                                         {
    //                                             if(!waveIsVisible)
     //                                                waveIsVisible = true;
     //                                        });
//    auto backPositionGL1 = CallFunc::create([this]()
//                                                        {
//                                                            goldenLine->setPosition(Vec2(origin.x + visibleSize.width/2 , origin.y + visibleSize.height/2));
//                                                        });
//    auto backPositionGL2 = CallFunc::create([this]()
//                                            {
//                                                goldenLine2->setPosition(Vec2(-origin.x + visibleSize.width/2 , origin.y + visibleSize.height/2));
//                                            });
    
    auto showKeyFadeIn = FadeIn::create(_Musica[keyIndexFinal].getTime()/7);
    auto showKeyFadeOut = FadeOut::create(_Musica[keyIndexFinal].getTime()/9);
//    auto moveWave1 = Sequence::create(showKeyFadeIn,MoveTo::create(_Musica[keyIndexFinal].getTime()/2, Vec2(origin.x + visibleSize.width , origin.y + visibleSize.height/2)),showKeyFadeOut,backPositionGL1, nullptr);
//    auto moveWave2 = Sequence::create(showKeyFadeIn,MoveTo::create(_Musica[keyIndexFinal].getTime()/2, Vec2(origin.x + visibleSize.width/2 , origin.y + visibleSize.height/2)),showKeyFadeOut,backPositionGL2, nullptr);
    
    
    if(sequenciaMusicaAtual[keyIndexFinal] == 1){
        C->runAction(Sequence::create(showKeyFadeIn/*,showWave*/,DelayTime::create(_Musica[keyIndexFinal].getTime()/2),showKeyFadeOut,nullptr));
    //    goldenLine->runAction(moveWave1);
     //   goldenLine2->runAction(moveWave2);
    }
    else if (sequenciaMusicaAtual[keyIndexFinal] == 2)
    {
        D->runAction(Sequence::create(showKeyFadeIn/*,showWave*/,DelayTime::create(_Musica[keyIndexFinal].getTime()/2),showKeyFadeOut,nullptr));
    }
    else if (sequenciaMusicaAtual[keyIndexFinal] == 3)
    {
        E->runAction(Sequence::create(showKeyFadeIn/*,showWave*/,DelayTime::create(_Musica[keyIndexFinal].getTime()/2),showKeyFadeOut,nullptr));
    }
    else if (sequenciaMusicaAtual[keyIndexFinal] == 4)
    {
        F->runAction(Sequence::create(showKeyFadeIn/*,showWave*/,DelayTime::create(_Musica[keyIndexFinal].getTime()/2),showKeyFadeOut,nullptr));
    }
    else if (sequenciaMusicaAtual[keyIndexFinal] == 5)
    {
        G->runAction(Sequence::create(showKeyFadeIn/*,showWave*/,DelayTime::create(_Musica[keyIndexFinal].getTime()/2),showKeyFadeOut,nullptr));
    }
    else if (sequenciaMusicaAtual[keyIndexFinal] == 6)
    {
        A->runAction(Sequence::create(showKeyFadeIn/*,showWave*/,DelayTime::create(_Musica[keyIndexFinal].getTime()/2),showKeyFadeOut,nullptr));
    }
    else if (sequenciaMusicaAtual[keyIndexFinal] == 7)
    {
        B->runAction(Sequence::create(showKeyFadeIn/*,showWave*/,DelayTime::create(_Musica[keyIndexFinal].getTime()/2),showKeyFadeOut,nullptr));
    }
}

void SimonGame::animateBright(Sprite* sprite,MenuItemImage* star)
{
    auto brightStar = CallFunc::create([this,star]()
    {        
        star->setEnabled(false);
    });
    auto brightoutStar = CallFunc::create([this,star]()
    {        
        star->setEnabled(true);
    });
    if(doneSequence){
        if(finishedMusic)
        {
            auto brightFadeIn = FadeIn::create(_Musica[keyIndexFinal].getTime()/7);
            auto brightFadeOut = FadeOut::create(_Musica[keyIndexFinal].getTime()/9);
            sprite->runAction(Sequence::create(brightStar,brightFadeIn,brightFadeOut,brightoutStar,nullptr));
        }
        else
        {
            auto brightFadeIn = FadeIn::create(_Musica[positionCheck].getTime()/7);
            auto brightFadeOut = FadeOut::create(_Musica[positionCheck].getTime()/9);
            sprite->runAction(Sequence::create(brightStar,brightFadeIn,brightFadeOut,brightoutStar,nullptr));
        }
    }
    else
    {
        if(finishedMusic)
        {
            auto brightFadeIn = FadeIn::create(_Musica[keyIndexFinal].getTime()/2);
            auto brightFadeOut = FadeOut::create(_Musica[keyIndexFinal].getTime()/2);
            sprite->runAction(Sequence::create(brightStar,brightFadeIn,brightFadeOut,DelayTime::create(_Musica[keyIndexFinal].getTime()/2),brightoutStar,nullptr));
        }
        else
        {
            if(armazenamentoTemporarioMusica == 6)
            {
                auto brightFadeIn = FadeIn::create(_Musica[positionCheck].getTime()/2.6);
                auto brightFadeOut = FadeOut::create(_Musica[positionCheck].getTime()/2.3);
                if(_Musica[positionCheck].getTime() >= 1.4f && _Musica[positionCheck].getTime() < 3)
                {
                    brightFadeOut = FadeOut::create(_Musica[positionCheck].getTime()/3);
                    sprite->runAction(Sequence::create(brightStar,brightFadeIn,DelayTime::create(_Musica[positionCheck].getTime()/1.6f),brightFadeOut,DelayTime::create(0.1f),brightoutStar,nullptr));
                }
                else
                    sprite->runAction(Sequence::create(brightStar,brightFadeIn,brightFadeOut,DelayTime::create(_Musica[positionCheck].getTime()/1.6f),brightoutStar,nullptr));
            }
            else if(armazenamentoTemporarioMusica == 5)
            {
                auto brightFadeIn = FadeIn::create(_Musica[positionCheck].getTime()/2.6);
                auto brightFadeOut = FadeOut::create(_Musica[positionCheck].getTime()/2.3);
                if(keyIndex == 22)
                {
                    //brightFadeOut = FadeOut::create(_Musica[positionCheck].getTime()/7);
                    sprite->runAction(Sequence::create(brightStar,brightFadeIn,DelayTime::create(_Musica[positionCheck].getTime()/0.5f),brightFadeOut,DelayTime::create(0.1f),brightoutStar,nullptr));
                }
                else if(keyIndex == 44)
                {
                    //brightFadeOut = FadeOut::create(_Musica[positionCheck].getTime()/7);
                    sprite->runAction(Sequence::create(brightStar,brightFadeIn,DelayTime::create(_Musica[positionCheck].getTime()/0.3f),brightFadeOut,DelayTime::create(0.1f),brightoutStar,nullptr));
                }
                else if(keyIndex == 66)
                {
                    //brightFadeOut = FadeOut::create(_Musica[positionCheck].getTime()/7);
                    sprite->runAction(Sequence::create(brightStar,brightFadeIn,DelayTime::create(_Musica[positionCheck].getTime()/0.3f),brightFadeOut,DelayTime::create(0.1f),brightoutStar,nullptr));
                }
                else{
                    //log("%d",keyIndex);
                    sprite->runAction(Sequence::create(brightStar,brightFadeIn,brightFadeOut,DelayTime::create(_Musica[positionCheck].getTime()),brightoutStar,nullptr));
                }
            }
            else
            {
                auto brightFadeIn = FadeIn::create(_Musica[positionCheck].getTime()/2.6);
                auto brightFadeOut = FadeOut::create(_Musica[positionCheck].getTime()/2.2);
                sprite->runAction(Sequence::create(brightStar,brightFadeIn,brightFadeOut,DelayTime::create(_Musica[positionCheck].getTime()/2),brightoutStar,nullptr));
            }
        }
    }
}
void SimonGame::update(float dt)
{
}
void SimonGame::playSoundStar1(Ref* pSender)
{
    if(!finishedMusic && !paused)
    {
        if(doneSequence)
        {
            if(_Musica[positionCheck].getStar() == 1)
            {
                SimonGame::verifySound(_Musica[positionCheck].getSound());
                SimonGame::animateBright(bright1,star1);
                clickCount += 1; // Número de cliques que o player já acertou ao todo
                //positionCheck += 1;
                if(clickCount == intervalosMusica[keyIndexAux-1])
                {
                    tempoUltimaNota = _Musica[positionCheck-1].getTime();
                    clickCount = 0;
                    doneSequence = false;
                    keySoundinBlock = 0;
                    keyIndexAux += 1;
                    positionCheck = keyIndex;
                    if((keyIndexAux - 1) < intervalosMusica.size())
                    {
                        SimonGame::scaleProgress();
                        auto audioAcertoNextSequence = CallFunc::create([this]()
                                                                        {
                                                                            SimonGame::animationPopText(true);
                                                                        });
                        auto delayNextSequence = Sequence::create(DelayTime::create(0.4f),audioAcertoNextSequence,nullptr);
                        background->runAction(delayNextSequence);
                    }
                    else
                    {
                        finishedMusic = true;
                        SimonGame::animateBright(bright1,star1);
                        SimonGame::scaleProgress();                        
                        auto audioFinalSequence = CallFunc::create([this]()
                                                                   {
                                                                       SimonGame::manageSoundFinal(0);
                                                                   });
                        auto sequencia = Sequence::create(DelayTime::create(2),audioFinalSequence,nullptr);
                        background->runAction(sequencia);
                    }
                }
            }
            else
            {
                SimonGame::animationPopText(false);
                doneSequence = false;
                positionCheck -= clickCount;
                clickCount = 0;
                keySoundinBlock = 0;
                if(keyIndexAux == 1)
                    keyIndex = 0;
                else
                {
                    keyIndex -= intervalosMusica[keyIndexAux-1];//(decrementoKeyIndex + 1);
                }
                auto audioErro = CallFunc::create([this]()
                                                  {
                                                      //CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/vl01.mp3");
                                                  });
                auto replaySequence = CallFunc::create([this]()
                                                       {
                                                           //SimonGame::manageSound(0);
                                                       });
                auto erroDelay = Sequence::create(audioErro, DelayTime::create(2),replaySequence, nullptr);
                background->runAction(erroDelay);
            }
        }
    }
}
void SimonGame::playSoundStar2(Ref* pSender)
{
    if(!finishedMusic && !paused)
    {
        if(doneSequence)
        {
            if(_Musica[positionCheck].getStar() == 2)
            {
                SimonGame::verifySound(_Musica[positionCheck].getSound());
                SimonGame::animateBright(bright2,star2);
                clickCount += 1; // Número de cliques que o player já acertou ao todo
                //positionCheck += 1;
                if(clickCount == intervalosMusica[keyIndexAux-1])
                {
                    tempoUltimaNota = _Musica[positionCheck-1].getTime();
                    clickCount = 0;
                    positionCheck = keyIndex;
                    doneSequence = false;
                    keySoundinBlock = 0;
                    keyIndexAux += 1;
                    if((keyIndexAux - 1) < intervalosMusica.size())
                    {
                        SimonGame::scaleProgress();
                        auto audioAcertoNextSequence = CallFunc::create([this]()
                                                                        {
                                                                            SimonGame::animationPopText(true);
                                                                        });
                        auto delayNextSequence = Sequence::create(DelayTime::create(0.4f),audioAcertoNextSequence,nullptr);
                        background->runAction(delayNextSequence);
                    }
                    else
                    {
                        finishedMusic = true;
                        SimonGame::animateBright(bright2,star2);
                        SimonGame::scaleProgress();
                        //SimonGame::manageSoundFinal(0);
                        auto audioFinalSequence = CallFunc::create([this]()
                                                                   {
                                                                       SimonGame::manageSoundFinal(0);
                                                                   });
                        auto sequencia = Sequence::create(DelayTime::create(2),audioFinalSequence,nullptr);
                        background->runAction(sequencia);
                    }
                }
            }
            else
            {
                SimonGame::animationPopText(false);
                doneSequence = false;
                positionCheck -= clickCount;
                keySoundinBlock = 0;
                clickCount = 0;
                if(keyIndexAux == 1)
                    keyIndex = 0;
                else
                {
                    keyIndex -= intervalosMusica[keyIndexAux-1];//(decrementoKeyIndex + 1);
                    
                }
                auto audioErro = CallFunc::create([this]()
                                                  {
                                                      //CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/vl01.mp3");
                                                  });
                auto replaySequence = CallFunc::create([this]()
                                                       {
                                                           //SimonGame::manageSound(0);
                                                       });
                auto erroDelay = Sequence::create(audioErro, DelayTime::create(2),replaySequence, nullptr);
                background->runAction(erroDelay);
            }
        }
    }
}
void SimonGame::playSoundStar3(Ref* pSender)
{
    if(!finishedMusic && !paused)
    {
        if(doneSequence)
        {
            if(_Musica[positionCheck].getStar() == 3)
            {
                SimonGame::verifySound(_Musica[positionCheck].getSound());
                SimonGame::animateBright(bright3,star3);
                clickCount += 1; // Número de cliques que o player já acertou ao todo
                //positionCheck += 1;
                if(clickCount == intervalosMusica[keyIndexAux-1])
                {
                    tempoUltimaNota = _Musica[positionCheck-1].getTime();
                    clickCount = 0;
                    positionCheck = keyIndex;
                    doneSequence = false;
                    keySoundinBlock = 0;
                    keyIndexAux += 1;
                    if((keyIndexAux - 1) < intervalosMusica.size())
                    {
                        SimonGame::scaleProgress();
                        auto audioAcertoNextSequence = CallFunc::create([this]()
                                                                        {
                                                                            SimonGame::animationPopText(true);
                                                                        });
                        auto delayNextSequence = Sequence::create(DelayTime::create(0.4f),audioAcertoNextSequence,nullptr);
                        background->runAction(delayNextSequence);
                    }
                    else
                    {
                        finishedMusic = true;
                        SimonGame::animateBright(bright3,star3);
                        SimonGame::scaleProgress();
                        
                        //SimonGame::manageSoundFinal(0);
                        auto audioFinalSequence = CallFunc::create([this]()
                                                                   {
                                                                       SimonGame::manageSoundFinal(0);
                                                                   });
                        auto sequencia = Sequence::create(DelayTime::create(2),audioFinalSequence,nullptr);
                        background->runAction(sequencia);
                    }
                }
            }
            else
            {
                SimonGame::animationPopText(false);
                doneSequence = false;
                positionCheck -= clickCount;
                keySoundinBlock = 0;
                clickCount = 0;
                if(keyIndexAux == 1)
                    keyIndex = 0;
                else
                {
                    keyIndex -= intervalosMusica[keyIndexAux-1];//(decrementoKeyIndex + 1);
                    
                }
                auto audioErro = CallFunc::create([this]()
                                                  {
                                                      //CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/vl01.mp3");
                                                  });
                auto replaySequence = CallFunc::create([this]()
                                                       {
                                                           // SimonGame::manageSound(0);
                                                       });
                auto erroDelay = Sequence::create(audioErro, DelayTime::create(2),replaySequence, nullptr);
                background->runAction(erroDelay);
            }
        }
    }
}
void SimonGame::playSoundStar4(Ref* pSender)
{
    if(!finishedMusic && !paused)
    {
        if(doneSequence)
        {
            if(_Musica[positionCheck].getStar() == 4)
            {
                SimonGame::verifySound(_Musica[positionCheck].getSound());
                SimonGame::animateBright(bright4,star4);
                clickCount += 1; // Número de cliques que o player já acertou ao todo
                //positionCheck += 1;
                if(clickCount == intervalosMusica[keyIndexAux-1])
                {
                    tempoUltimaNota = _Musica[positionCheck-1].getTime();
                    clickCount = 0;
                    positionCheck = keyIndex;
                    doneSequence = false;
                    keySoundinBlock = 0;
                    keyIndexAux += 1;
                    if((keyIndexAux - 1) < intervalosMusica.size())
                    {
                        SimonGame::scaleProgress();
                        auto audioAcertoNextSequence = CallFunc::create([this]()
                                                                        {
                                                                            SimonGame::animationPopText(true);//SimonGame::manageSound(0);
                                                                        });
                        auto delayNextSequence = Sequence::create(DelayTime::create(0.4f),audioAcertoNextSequence,nullptr);
                        background->runAction(delayNextSequence);
                    }
                    else
                    {
                        finishedMusic = true;
                        SimonGame::animateBright(bright4,star4);
                        SimonGame::scaleProgress();
                        
                        //SimonGame::manageSoundFinal(0);
                        auto audioFinalSequence = CallFunc::create([this]()
                                                                   {
                                                                       SimonGame::manageSoundFinal(0);
                                                                   });
                        auto sequencia = Sequence::create(DelayTime::create(2),audioFinalSequence,nullptr);
                        background->runAction(sequencia);
                    }
                }
            }
            else
            {
                SimonGame::animationPopText(false);
                doneSequence = false;
                positionCheck -= clickCount;
                keySoundinBlock = 0;
                clickCount = 0;
                if(keyIndexAux == 1)
                    keyIndex = 0;
                else
                {
                    keyIndex -= intervalosMusica[keyIndexAux-1];//(decrementoKeyIndex + 1);
                    
                }
                auto audioErro = CallFunc::create([this]()
                                                  {
                                                      // CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/vl01.mp3");
                                                  });
                auto replaySequence = CallFunc::create([this]()
                                                       {
                                                           // SimonGame::manageSound(0);
                                                       });
                auto erroDelay = Sequence::create(audioErro, DelayTime::create(2),replaySequence, nullptr);
                background->runAction(erroDelay);
            }
        }
    }
}
void SimonGame::playSoundStar5(Ref* pSender)
{
    if(!finishedMusic && !paused)
    {
        if(doneSequence)
        {
            if(_Musica[positionCheck].getStar() == 5)
            {
                SimonGame::verifySound(_Musica[positionCheck].getSound());
                SimonGame::animateBright(bright5,star5);
                clickCount += 1; // Número de cliques que o player já acertou ao todo
                //positionCheck += 1;
                if(clickCount == intervalosMusica[keyIndexAux-1])
                {
                    tempoUltimaNota = _Musica[positionCheck-1].getTime();
                    clickCount = 0;
                    positionCheck = keyIndex;
                    doneSequence = false;
                    keySoundinBlock = 0;
                    keyIndexAux += 1;
                    if((keyIndexAux - 1) < intervalosMusica.size())
                    {
                        SimonGame::scaleProgress();
                        auto audioAcertoNextSequence = CallFunc::create([this]()
                                                                        {
                                                                            SimonGame::animationPopText(true);//SimonGame::manageSound(0);
                                                                        });
                        auto delayNextSequence = Sequence::create(DelayTime::create(0.4f),audioAcertoNextSequence,nullptr);
                        background->runAction(delayNextSequence);
                    }
                    else
                    {
                        finishedMusic = true;
                        SimonGame::animateBright(bright5,star5);
                        SimonGame::scaleProgress();
                        
                        //SimonGame::manageSoundFinal(0);
                        auto audioFinalSequence = CallFunc::create([this]()
                                                                   {
                                                                       SimonGame::manageSoundFinal(0);
                                                                   });
                        auto sequencia = Sequence::create(DelayTime::create(2),audioFinalSequence,nullptr);
                        background->runAction(sequencia);
                    }
                }
            }
            else
            {
                SimonGame::animationPopText(false);
                doneSequence = false;
                positionCheck -= clickCount;
                keySoundinBlock = 0;
                clickCount = 0;
                if(keyIndexAux == 1)
                    keyIndex = 0;
                else
                {
                    keyIndex -= intervalosMusica[keyIndexAux-1];//(decrementoKeyIndex + 1);
                    
                }
                auto audioErro = CallFunc::create([this]()
                                                  {
                                                      // CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/vl01.mp3");
                                                  });
                auto replaySequence = CallFunc::create([this]()
                                                       {
                                                           // SimonGame::manageSound(0);
                                                       });
                auto erroDelay = Sequence::create(audioErro, DelayTime::create(2),replaySequence, nullptr);
                background->runAction(erroDelay);
            }
        }
    }
}
void SimonGame::playSoundStar6(Ref* pSender)
{
    if(!finishedMusic && !paused)
    {
        if(doneSequence)
        {
            if(_Musica[positionCheck].getStar() == 6)
            {
                SimonGame::verifySound(_Musica[positionCheck].getSound());
                SimonGame::animateBright(bright6,star6);
                clickCount += 1; // Número de cliques que o player já acertou ao todo
                //positionCheck += 1;
                if(clickCount == intervalosMusica[keyIndexAux-1])
                {
                    tempoUltimaNota = _Musica[positionCheck-1].getTime();
                    clickCount = 0;
                    positionCheck = keyIndex;
                    doneSequence = false;
                    keySoundinBlock = 0;
                    keyIndexAux += 1;
                    if((keyIndexAux - 1) < intervalosMusica.size())
                    {
                        SimonGame::scaleProgress();
                        auto audioAcertoNextSequence = CallFunc::create([this]()
                                                                        {
                                                                            SimonGame::animationPopText(true);//SimonGame::manageSound(0);
                                                                        });
                        auto delayNextSequence = Sequence::create(DelayTime::create(0.4f),audioAcertoNextSequence,nullptr);
                        background->runAction(delayNextSequence);
                    }
                    else
                    {
                        finishedMusic = true;
                        SimonGame::animateBright(bright6,star6);
                        SimonGame::scaleProgress();
                        
                        //SimonGame::manageSoundFinal(0);
                        auto audioFinalSequence = CallFunc::create([this]()
                                                                   {
                                                                       SimonGame::manageSoundFinal(0);
                                                                   });
                        auto sequencia = Sequence::create(DelayTime::create(2),audioFinalSequence,nullptr);
                        background->runAction(sequencia);
                    }
                }
            }
            else
            {
                SimonGame::animationPopText(false);
                doneSequence = false;
                positionCheck -= clickCount;
                keySoundinBlock = 0;
                clickCount = 0;
                if(keyIndexAux == 1)
                    keyIndex = 0;
                else
                {
                    keyIndex -= intervalosMusica[keyIndexAux-1];//(decrementoKeyIndex + 1);
                    
                }
                auto audioErro = CallFunc::create([this]()
                                                  {
                                                      //  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/vl01.mp3");
                                                  });
                auto replaySequence = CallFunc::create([this]()
                                                       {
                                                           // SimonGame::manageSound(0);
                                                       });
                auto erroDelay = Sequence::create(audioErro, DelayTime::create(2),replaySequence, nullptr);
                background->runAction(erroDelay);
            }
        }
    }
}
void SimonGame::playSoundStar7(Ref* pSender)
{
    if(!finishedMusic && !paused)
    {
        if(doneSequence)
        {
            if(_Musica[positionCheck].getStar() == 7)
            {
                SimonGame::verifySound(_Musica[positionCheck].getSound());
                SimonGame::animateBright(bright7,star7);
                clickCount += 1; // Número de cliques que o player já acertou ao todo
                //positionCheck += 1;
                if(clickCount == intervalosMusica[keyIndexAux-1])
                {
                    tempoUltimaNota = _Musica[positionCheck-1].getTime();
                    clickCount = 0;
                    positionCheck = keyIndex;
                    doneSequence = false;
                    keySoundinBlock = 0;
                    keyIndexAux += 1;
                    if((keyIndexAux - 1) < intervalosMusica.size())
                    {
                        SimonGame::scaleProgress();
                        auto audioAcertoNextSequence = CallFunc::create([this]()
                                                                        {
                                                                            SimonGame::animationPopText(true);//SimonGame::manageSound(0);
                                                                        });
                        auto delayNextSequence = Sequence::create(DelayTime::create(1),audioAcertoNextSequence,nullptr);
                        background->runAction(delayNextSequence);
                    }
                    else
                    {
                        finishedMusic = true;
                        SimonGame::animateBright(bright7,star7);
                        SimonGame::scaleProgress();
                        
                        //SimonGame::manageSoundFinal(0);
                        auto audioFinalSequence = CallFunc::create([this]()
                                                                   {
                                                                       SimonGame::manageSoundFinal(0);
                                                                   });
                        auto sequencia = Sequence::create(DelayTime::create(2),audioFinalSequence,nullptr);
                        background->runAction(sequencia);
                    }   
                }
            }
            else
            {
                SimonGame::animationPopText(false);
                doneSequence = false;
                positionCheck -= clickCount;
                keySoundinBlock = 0;  
                clickCount = 0;
                if(keyIndexAux == 1)               
                    keyIndex = 0;
                else
                {
                    keyIndex -= intervalosMusica[keyIndexAux-1];//(decrementoKeyIndex + 1);
                    
                }
                auto audioErro = CallFunc::create([this]()
                                                  {                    
                                                      //CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/vl01.mp3");
                                                  });
                auto replaySequence = CallFunc::create([this]()
                                                       { 
                                                           // SimonGame::manageSound(0);
                                                       });
                auto erroDelay = Sequence::create(audioErro, DelayTime::create(2),replaySequence, nullptr);
                background->runAction(erroDelay);
            }
        }
    }
}
void SimonGame::verifySound(int soundIndex) //verifica qual audio deve ser tocado
{
    if(!paused){
    if(musicNumber == 5 && instrumentNumber == 1)
    {
        if(_Musica[soundIndex-1].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota1.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota2.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota3.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota4.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota5.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota6.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota7.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota8.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota9.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota10.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota11.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota12.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota13.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota14.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota15.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota16.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota17.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota18.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota19.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota20.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota21.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota22.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota23.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota24.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota25.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota26.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota27.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota28.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota29.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota30.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota31.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota32.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 33)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota33.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 34)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota34.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 35)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota35.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 36)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota36.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 37)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota37.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 38)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota38.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 39)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota39.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 40)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota40.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 41)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota41.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 42)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota42.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 43)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota43.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 44)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota44.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 45)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota45.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 46)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota46.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 47)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota47.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 48)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota48.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 49)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota49.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 50)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota50.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 51)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota51.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 52)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota52.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 53)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota53.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 54)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota54.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 55)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota55.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 56)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota56.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 57)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota57.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 58)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota58.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 59)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota59.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 60)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota60.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 61)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota61.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 62)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota62.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 63)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota63.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 64)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota64.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 65)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota65.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 66)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota66.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 67)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota67.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }   
    }
        else if(musicNumber == 5 && instrumentNumber == 2)
        {
            if(_Musica[soundIndex-1].getSound() == 1)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota1.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 2)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota2.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 3)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota3.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 4)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota4.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 5)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota5.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 6)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota6.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 7)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota7.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 8)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota8.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 9)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota9.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 10)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota10.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 11)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota11.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 12)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota12.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 13)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota13.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 14)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota14.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 15)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota15.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 16)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota16.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 17)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota17.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 18)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota18.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 19)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota19.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 20)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota20.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 21)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota21.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 22)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota22.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 23)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota23.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 24)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota24.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 25)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota25.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 26)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota26.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 27)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota27.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 28)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota28.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 29)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota29.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 30)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota30.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 31)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota31.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 32)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota32.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 33)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota33.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 34)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota34.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 35)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota35.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 36)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota36.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 37)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota37.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 38)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota38.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 39)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota39.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 40)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota40.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 41)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota41.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 42)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota42.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 43)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota43.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 44)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota44.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 45)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota45.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 46)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota46.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 47)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota47.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 48)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota48.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 49)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota49.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 50)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota50.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 51)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota51.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 52)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota52.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 53)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota53.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 54)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota54.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 55)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota55.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 56)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota56.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 57)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota57.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 58)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota58.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 59)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota59.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 60)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota60.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 61)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota61.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 62)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota62.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 63)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota63.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 64)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota64.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            } 
            else if(_Musica[soundIndex-1].getSound() == 65)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota65.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }
            else if(_Musica[soundIndex-1].getSound() == 66)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota66.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            } 
            else if(_Musica[soundIndex-1].getSound() == 67)
            {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota67.mp3");
                auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
                positionCheck +=1;
                background->runAction(delay);
            }   
        }
    else if(musicNumber == 3 && instrumentNumber == 1)
    {
        if(_Musica[soundIndex-1].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota1.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota2.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota3.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota4.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota5.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota6.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota7.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota8.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota9.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota10.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota11.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota12.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota13.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota14.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota15.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota16.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota17.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota18.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota19.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota20.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota21.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota22.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota23.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota24.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota25.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota26.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota27.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota28.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota29.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota30.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota31.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota32.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }  
    }
    else if(musicNumber == 3 && instrumentNumber == 2)
    {
        if(_Musica[soundIndex-1].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota1.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota2.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota3.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota4.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota5.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota6.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota7.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota8.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota9.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota10.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota11.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota12.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota13.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota14.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota15.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota16.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota17.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota18.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota19.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota20.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota21.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota22.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota23.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota24.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota25.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota26.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota27.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota28.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota29.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota30.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota31.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota32.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }  
    }
    else if(musicNumber == 1 && instrumentNumber == 1)
    {
        if(_Musica[soundIndex-1].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota1.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota2.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota3.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota4.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota5.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota6.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota7.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota8.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota9.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota10.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota11.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota12.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota13.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota14.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota15.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }  
    }
    else if(musicNumber == 1 && instrumentNumber == 2)
    {
        if(_Musica[soundIndex-1].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota1.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota2.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota3.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota4.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota5.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota6.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota7.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota8.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota9.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota10.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota11.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota12.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota13.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota14.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota15.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }  
    }
    else if(musicNumber == 2 && instrumentNumber == 1)
    {
        if(_Musica[soundIndex-1].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota1.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota2.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota3.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota4.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota5.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota6.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota7.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota8.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota9.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota10.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota11.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota12.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota13.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota14.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota15.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota16.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota17.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota18.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota19.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota20.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota21.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota22.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota23.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota24.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota25.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota26.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota27.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota28.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota29.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota30.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }  
    }
    else if(musicNumber == 2 && instrumentNumber == 2)
    {
        if(_Musica[soundIndex-1].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota1.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota2.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota3.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota4.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota5.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota6.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota7.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota8.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota9.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota10.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota11.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota12.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota13.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota14.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota15.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota16.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota17.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota18.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota19.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota20.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota21.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota22.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota23.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota24.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota25.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota26.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota27.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota28.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota29.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        } 
        else if(_Musica[soundIndex-1].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota30.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }  
    }
    else if(musicNumber == 4 && instrumentNumber == 1)
    {
        if(_Musica[soundIndex-1].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota1.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota2.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota3.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota4.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota5.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota6.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota7.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota8.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota9.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota10.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota11.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota12.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota13.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota14.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota15.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota16.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota17.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota18.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota19.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota20.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota21.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota22.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota23.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota24.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota25.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota26.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota27.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota28.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota29.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota30.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota31.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota32.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 33)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota33.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 34)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota34.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
    }
    else if(musicNumber == 4 && instrumentNumber == 2)
    {
        if(_Musica[soundIndex-1].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota1.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota2.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota3.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota4.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota5.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota6.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota7.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota8.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota9.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota10.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota11.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota12.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota13.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota14.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota15.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota16.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota17.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota18.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota19.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota20.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota21.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota22.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota23.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota24.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota25.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota26.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota27.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota28.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota29.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota30.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota31.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota32.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 33)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota33.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 34)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota34.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
    }
    else if(musicNumber == 6 && instrumentNumber == 1)
    {
        if(_Musica[soundIndex-1].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota1.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota2.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota3.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota4.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota5.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota6.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota7.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota8.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota9.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota10.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota11.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota12.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota13.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota14.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota15.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota16.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota17.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota18.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota19.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota20.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota21.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota22.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota23.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota24.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota25.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota26.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota27.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota28.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota29.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota30.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota31.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota32.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 33)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota33.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 34)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota34.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 35)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota35.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 36)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota36.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 37)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota37.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 38)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota38.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 39)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota39.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 40)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota40.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 41)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota41.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 42)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota42.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 43)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota43.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 44)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota44.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 45)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota45.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 46)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota46.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 47)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota47.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 48)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota48.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 49)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota49.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 50)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota50.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 51)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota51.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 52)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota52.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 53)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota53.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 54)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota54.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 55)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota55.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 56)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota56.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 57)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota57.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 58)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota58.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 59)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota59.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 60)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota60.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 61)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota61.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 62)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota62.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 63)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota63.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 64)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota64.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 65)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota65.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 66)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota66.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 67)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota67.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 68)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota68.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 69)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota69.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 70)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota70.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
    }
    else if(musicNumber == 6 && instrumentNumber == 2)
    {
        if(_Musica[soundIndex-1].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota1.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota2.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota3.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota4.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota5.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota6.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota7.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota8.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota9.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota10.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota11.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota12.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota13.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota14.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota15.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota16.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota17.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota18.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota19.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota20.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota21.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota22.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota23.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota24.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota25.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota26.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota27.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota28.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota29.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota30.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota31.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota32.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 33)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota33.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 34)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota34.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 35)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota35.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 36)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota36.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 37)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota37.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 38)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota38.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 39)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota39.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 40)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota40.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 41)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota41.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 42)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota42.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 43)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota43.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 44)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota44.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 45)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota45.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 46)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota46.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 47)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota47.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 48)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota48.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 49)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota49.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 50)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota50.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 51)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota51.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()), nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 52)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota52.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 53)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota53.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 54)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota54.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 55)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota55.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 56)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota56.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 57)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota57.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 58)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota58.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 59)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota59.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 60)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota60.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 61)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota61.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 62)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota62.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 63)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota63.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 64)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota64.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 65)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota65.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 66)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota66.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 67)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota67.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 68)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota68.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 69)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota69.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
        else if(_Musica[soundIndex-1].getSound() == 70)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota70.mp3");
            auto delay = Sequence::create(DelayTime::create(_Musica[soundIndex].getTime()),nullptr);
            positionCheck +=1;
            background->runAction(delay);
        }
    }
    }
}
void SimonGame::playSound(int soundIndex) //verifica qual audio deve ser tocado
{if(!paused){
    if(musicNumber == 5 && instrumentNumber == 1)
    {
        if(_Musica[soundIndex].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota1.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota2.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota3.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota4.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota5.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota6.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota7.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota8.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota9.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota10.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota11.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota12.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota13.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota14.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota15.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota16.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota17.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota18.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota19.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota20.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota21.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota22.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota23.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota24.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota25.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota26.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota27.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota28.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota29.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota30.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota31.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota32.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 33)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota33.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 34)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota34.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 35)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota35.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 36)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota36.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 37)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota37.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 38)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota38.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 39)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota39.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 40)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota40.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 41)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota41.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 42)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota42.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 43)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota43.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 44)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota44.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 45)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota45.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 46)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota46.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 47)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota47.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 48)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota48.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 49)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota49.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 50)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota50.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 51)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota51.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 52)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota52.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 53)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota53.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 54)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota54.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 55)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota55.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 56)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota56.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 57)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota57.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 58)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota58.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 59)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota59.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 60)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota60.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 61)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota61.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 62)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota62.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 63)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota63.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 64)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota64.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 65)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota65.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 66)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota66.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 67)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Cordas/mp3/Nota67.mp3");
        }   
    }
    else if(musicNumber == 5 && instrumentNumber == 2)
    {
        if(_Musica[soundIndex].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota1.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota2.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota3.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota4.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota5.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota6.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota7.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota8.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota9.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota10.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota11.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota12.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota13.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota14.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota15.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota16.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota17.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota18.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota19.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota20.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota21.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota22.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota23.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota24.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota25.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota26.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota27.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota28.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota29.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota30.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota31.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota32.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 33)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota33.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 34)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota34.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 35)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota35.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 36)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota36.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 37)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota37.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 38)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota38.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 39)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota39.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 40)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota40.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 41)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota41.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 42)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota42.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 43)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota43.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 44)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota44.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 45)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota45.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 46)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota46.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 47)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota47.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 48)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota48.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 49)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota49.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 50)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota50.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 51)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota51.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 52)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota52.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 53)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota53.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 54)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota54.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 55)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota55.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 56)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota56.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 57)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota57.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 58)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota58.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 59)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota59.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 60)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota60.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 61)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota61.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 62)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota62.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 63)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota63.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 64)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota64.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 65)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota65.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 66)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota66.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 67)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/NestaRua/Sopro/mp3/Nota67.mp3");
        }
    }
    else if(musicNumber == 3 && instrumentNumber == 1)
    {
        if(_Musica[soundIndex].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota1.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota2.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota3.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota4.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota5.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota6.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota7.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota8.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota9.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota10.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota11.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota12.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota13.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota14.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota15.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota16.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota17.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota18.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota19.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota20.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota21.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota22.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota23.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota24.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota25.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota26.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota27.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota28.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota29.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota30.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota31.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Cordas/mp3/Nota32.mp3");
        }  
    }
    else if(musicNumber == 3 && instrumentNumber == 2)
    {
        if(_Musica[soundIndex].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota1.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota2.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota3.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota4.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota5.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota6.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota7.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota8.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota9.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota10.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota11.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota12.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota13.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota14.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota15.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota16.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota17.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota18.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota19.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota20.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota21.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota22.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota23.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota24.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota25.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota26.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota27.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota28.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota29.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota30.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota31.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/ADancaDaCarranquinha/Sopro/mp3/Nota32.mp3");
        }  
    }
    else if(musicNumber == 1 && instrumentNumber == 1)
    {
        if(_Musica[soundIndex].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota1.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota2.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota3.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota4.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota5.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota6.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota7.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota8.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota9.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota10.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota11.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota12.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota13.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota14.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Cordas/mp3/Nota15.mp3");
        } 
    }
    else if(musicNumber == 1 && instrumentNumber == 2)
    {
        if(_Musica[soundIndex].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota1.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota2.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota3.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota4.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota5.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota6.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota7.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota8.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota9.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota10.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota11.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota12.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota13.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota14.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/QuantosDiasTemOmes/Sopro/mp3/Nota15.mp3");
        } 
    }
    else if(musicNumber == 2 && instrumentNumber == 1)
    {
        if(_Musica[soundIndex].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota1.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota2.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota3.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota4.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota5.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota6.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota7.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota8.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota9.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota10.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota11.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota12.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota13.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota14.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota15.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota16.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota17.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota18.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota19.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota20.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota21.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota22.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota23.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota24.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota25.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota26.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota27.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota28.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota29.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Cordas/mp3/Nota30.mp3");
        } 
    }
    else if(musicNumber == 2 && instrumentNumber == 2)
    {
        if(_Musica[soundIndex].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota1.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota2.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota3.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota4.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota5.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota6.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota7.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota8.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota9.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota10.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota11.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota12.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota13.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota14.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota15.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota16.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota17.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota18.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota19.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota20.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota21.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota22.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota23.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota24.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota25.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota26.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota27.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota28.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota29.mp3");
        } 
        else if(_Musica[soundIndex].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/LaNaPonteDaVinhaca/Sopro/mp3/Nota30.mp3");
        } 
    }
    else if(musicNumber == 4 && instrumentNumber == 1)
    {
        if(_Musica[soundIndex].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota1.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota2.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota3.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota4.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota5.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota6.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota7.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota8.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota9.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota10.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota11.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota12.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota13.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota14.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota15.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota16.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota17.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota18.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota19.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota20.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota21.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota22.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota23.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota24.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota25.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota26.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota27.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota28.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota29.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota30.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota31.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota32.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 33)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota33.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 34)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Cordas/mp3/Nota34.mp3");
        }
    }
    else if(musicNumber == 4 && instrumentNumber == 2)
    {
        if(_Musica[soundIndex].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota1.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota2.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota3.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota4.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota5.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota6.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota7.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota8.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota9.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota10.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota11.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota12.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota13.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota14.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota15.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota16.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota17.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota18.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota19.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota20.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota21.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota22.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota23.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota24.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota25.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota26.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota27.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota28.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota29.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota30.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota31.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota32.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 33)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota33.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 34)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Atche/Sopro/mp3/Nota34.mp3");
        }
    }
    else if(musicNumber == 6 && instrumentNumber == 1)
    {
        if(_Musica[soundIndex].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota1.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota2.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota3.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota4.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota5.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota6.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota7.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota8.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota9.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota10.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota11.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota12.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota13.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota14.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota15.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota16.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota17.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota18.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota19.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota20.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota21.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota22.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota23.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota24.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota25.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota26.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota27.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota28.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota29.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota30.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota31.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota32.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 33)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota33.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 34)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota34.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 35)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota35.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 36)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota36.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 37)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota37.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 38)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota38.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 39)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota39.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 40)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota40.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 41)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota41.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 42)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota42.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 43)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota43.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 44)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota44.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 45)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota45.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 46)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota46.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 47)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota47.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 48)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota48.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 49)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota49.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 50)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota50.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 51)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota51.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 52)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota52.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 53)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota53.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 54)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota54.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 55)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota55.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 56)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota56.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 57)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota57.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 58)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota58.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 59)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota59.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 60)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota60.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 61)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota61.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 62)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota62.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 63)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota63.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 64)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota64.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 65)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota65.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 66)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota66.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 67)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota67.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 68)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota68.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 69)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota69.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 70)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Cordas/mp3/Nota70.mp3");
        }
    }
    else if(musicNumber == 6 && instrumentNumber == 2)
    {
        if(_Musica[soundIndex].getSound() == 1)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota1.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 2)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota2.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 3)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota3.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 4)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota4.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 5)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota5.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 6)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota6.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 7)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota7.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 8)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota8.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 9)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota9.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota10.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 11)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota11.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 12)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota12.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 13)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota13.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 14)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota14.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 15)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota15.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 16)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota16.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 17)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota17.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 18)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota18.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 19)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota19.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 20)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota20.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 21)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota21.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 22)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota22.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 23)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota23.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 24)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota24.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 25)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota25.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 26)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota26.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 27)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota27.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 28)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota28.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 29)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota29.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 30)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota30.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 31)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota31.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 32)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota32.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 33)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota33.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 34)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota34.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 35)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota35.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 36)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota36.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 37)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota37.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 38)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota38.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 39)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota39.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 40)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota40.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 41)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota41.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 42)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota42.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 43)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota43.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 44)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota44.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 45)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota45.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 46)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota46.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 47)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota47.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 48)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota48.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 49)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota49.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 50)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota50.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 51)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota51.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 52)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota52.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 53)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota53.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 54)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota54.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 55)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota55.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 56)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota56.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 57)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota57.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 58)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota58.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 59)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota59.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 60)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota60.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 61)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota61.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 62)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota62.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 63)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota63.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 64)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota64.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 65)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota65.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 66)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota66.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 67)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota67.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 68)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota68.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 69)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota69.mp3");
        }
        else if(_Musica[soundIndex].getSound() == 70)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Musicas/Condessa/Sopro/mp3/Nota70.mp3");
        }
    }
}
}
void SimonGame::animationPopText(bool Correct)
{if(!paused){
    if(Correct)
    {
        
        auto disableStars = CallFunc::create([this]()
                                            {
                                            });
        auto playBlink = CallFunc::create([this]()
                                          {
                                              CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Instruments/RightSequence.mp3");
                                          });
        
        auto playNextSequence =  CallFunc::create([this]()
                                                  {
                                                      SimonGame::manageSound(0);
                                                  });
        if(tempoUltimaNota < 0.5f)
        {
            auto brilhoEstrelas = Sequence::create(disableStars,playBlink,DelayTime::create(0.5f),playNextSequence,nullptr);
            auto animLight = Sequence::create(FadeIn::create(0.1),DelayTime::create(0.15f),FadeOut::create(0.1),nullptr);
            dourado->runAction(animLight);
            background->runAction(brilhoEstrelas);
        }
        else
        {
            auto brilhoEstrelas = Sequence::create(disableStars,playBlink,DelayTime::create(tempoUltimaNota/1.5),playNextSequence,nullptr);
            auto animLight = Sequence::create(FadeIn::create(0.1),DelayTime::create(0.15f),FadeOut::create(0.1),nullptr);
            dourado->runAction(animLight);
            background->runAction(brilhoEstrelas);
            
        }
    }
    else
    {
        auto disableStars2 = CallFunc::create([this]()
                                              {
                                                  /*  star1->setEnabled(false);
                                                   star2->setEnabled(false);
                                                   star3->setEnabled(false);
                                                   star4->setEnabled(false);
                                                   star5->setEnabled(false);
                                                   star6->setEnabled(false);
                                                   star7->setEnabled(false);*/
                                              });
        auto playBlink2 = CallFunc::create([this]()
                                           {
                                               CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("Instruments/WrongSequence.mp3");
                                               numeroDeErros +=1;
                                           });
        
        auto playNextSequence2 =  CallFunc::create([this]()
                                                   {
                                                       SimonGame::manageSound(0);
                                                   });
      //  ops->setScale(_screenSize.width*0.00045);
       // ops->setRotation(0);
        if(flagTextAngle)
        {
          //  ops->setRotation(-20);
          //  flagTextAngle = !flagTextAngle;
        }
        else
        {
          //  ops->setRotation(20);
           // flagTextAngle = !flagTextAngle;
        }
        auto brilhoEstrelas2 = Sequence::create(disableStars2,playBlink2,DelayTime::create(0.5f),playNextSequence2,nullptr);
        auto animLight = Sequence::create(FadeIn::create(0.1),DelayTime::create(0.15f),FadeOut::create(0.1),nullptr);
        vermelho->runAction(animLight);
        background->runAction(brilhoEstrelas2);
    }}
}
void SimonGame::Animate(float dt)
{
     auto anim = Sequence::create(EaseCircleActionOut::create(
     MoveTo::create(4,Vec2(cortinaDir->getContentSize().width, origin.y + visibleSize.height))),
     nullptr
     );
     auto anim2 = Sequence::create(EaseCircleActionOut::create(
     MoveTo::create(6,Vec2(-cortinaDir->getContentSize().width, origin.y + visibleSize.height))),
     nullptr
     );
     auto star1Anim = Sequence::create(EaseCircleActionOut::create(
     MoveTo::create(2,Vec2(origin.x + (_screenSize.width/6)*1, origin.y + visibleSize.height + star1->getContentSize().height/6))),
     nullptr
     );
     auto star2Anim = Sequence::create(EaseCircleActionOut::create(
     MoveTo::create(2,Vec2(origin.x + (_screenSize.width/6)*2, origin.y + visibleSize.height))),
     nullptr
     );
     auto star3Anim = Sequence::create(EaseCircleActionOut::create(
     MoveTo::create(2,Vec2(origin.x + (_screenSize.width/6)*3, origin.y + visibleSize.height + star1->getContentSize().height/6))),
     nullptr
     );
     auto star4Anim2 = Sequence::create(EaseCircleActionOut::create(
     MoveTo::create(2,Vec2(origin.x + (_screenSize.width/6)*4, origin.y + visibleSize.height))),
     nullptr
     );
     auto star5Anim1 = Sequence::create(EaseCircleActionOut::create(
     MoveTo::create(2,Vec2(origin.x + (_screenSize.width/6)*5, origin.y + visibleSize.height + star1->getContentSize().height/6))),
     nullptr
     );

    
    cortinaDir->runAction(anim); //right courtain animation
    cortinaEsc->runAction(anim2); // left courtain animation
    if(musicNumber == 1 || musicNumber == 2 || musicNumber == 3 || musicNumber == 4 || musicNumber == 5 || musicNumber == 6)
    {
       // background->runAction(Sequence::create(playTuhuVoice,nullptr));
        star1->runAction(star1Anim); // star 1 animation
        star2->runAction(star2Anim); // star 2 animation
        star3->runAction(star3Anim); // star 3 animation
        star4->runAction(star4Anim2); // star 4 animation
        star5->runAction(star5Anim1); // star 5 animation
    }
}

void SimonGame::animateWave(float dt)
{
   /* if(waveIsVisible) {
        goldenLine->setVisible(true);
        goldenLine2->setVisible(true);
    }
    else{
        goldenLine->setVisible(false);
        goldenLine2->setVisible(false);
    }*/
//    if(!verifyScaleLimit)
//    {
//        scaleWaveIn += 0.03f;
//        goldenLine->setScaleY(_screenSize.height/((10+scaleWaveIn*6)*goldenLine->getContentSize().height));
//        goldenLine2->setScaleY(_screenSize.height/((10+scaleWaveIn*6)*goldenLine->getContentSize().height));
//    }else{
//        scaleWaveIn -= 0.03f;
//        goldenLine->setScaleY(_screenSize.height/((10+scaleWaveIn*6)*goldenLine->getContentSize().height));
//        goldenLine2->setScaleY(_screenSize.height/((10+scaleWaveIn*6)*goldenLine->getContentSize().height));
//    }
//    if(scaleWaveIn > 1){
//        verifyScaleLimit = true;
//    }
//    else if(scaleWaveIn < -1){
//        verifyScaleLimit = false;
//    }
    
  /*  if(goldenLine->getPosition().x > 3*(_screenSize.width/2))
    {
        goldenLine->setPosition(-(.98*_screenSize.width)/2,goldenLine->getPosition().y);
    }
    if(goldenLine2->getPosition().x > 3*(_screenSize.width/2))
    {
        goldenLine2->setPosition(-(.98*_screenSize.width/2),goldenLine->getPosition().y);
    }
    scaleValue += 0.0015f;
    goldenLine2->setPosition(goldenLine2->getPosition().x+scaleValue, goldenLine2->getPosition().y);
    goldenLine->setPosition(goldenLine->getPosition().x+scaleValue, goldenLine->getPosition().y);
    */
}

void SimonGame::animateStarShake(float dt)
{
    if(!paused)
    {
        auto repeat = CallFunc::create([this]()
        {        
            SimonGame::animateStarShake(0);
        });
        
        auto shakeAnim1 = Sequence::create(RotateTo::create(5,1.6),RotateTo::create(5,0),RotateTo::create(5,-1.6),RotateTo::create(5,0),nullptr);
        auto shakeAnim2 = Sequence::create(RotateTo::create(5,1.6),RotateTo::create(5,0),RotateTo::create(5,-1.6),RotateTo::create(5,0),repeat,nullptr);
        auto shakeAnim3 = Sequence::create(RotateTo::create(5,1.6),RotateTo::create(5,0),RotateTo::create(5,-1.6),RotateTo::create(5,0),nullptr);
        auto shakeAnim4 = Sequence::create(RotateTo::create(5,1.6),RotateTo::create(5,0),RotateTo::create(5,-1.6),RotateTo::create(5,0),nullptr);
        auto shakeAnim5 = Sequence::create(RotateTo::create(5,1.6),RotateTo::create(5,0),RotateTo::create(5,-1.6),RotateTo::create(5,0),nullptr);
        auto shakeAnim6 = Sequence::create(RotateTo::create(5,1.6),RotateTo::create(5,0),RotateTo::create(5,-1.6),RotateTo::create(5,0),nullptr);
        auto shakeAnim7 = Sequence::create(RotateTo::create(5,1.6),RotateTo::create(5,0),RotateTo::create(5,-1.6),RotateTo::create(5,0),nullptr);
        
        star1->runAction(shakeAnim1);
        star2->runAction(shakeAnim2);
        star3->runAction(shakeAnim3);
        star4->runAction(shakeAnim4);
        star5->runAction(shakeAnim5);
        star6->runAction(shakeAnim6);
        star7->runAction(shakeAnim7);
    }
}
void SimonGame::animateVictory(float dt)
{
    if(!paused)
    {
        auto enableButtonMenu = CallFunc::create([this]()
                                                 {
                                                     backButton->setEnabled(false);
                                                 });
        auto enableButtonSongs = CallFunc::create([this]()
                                                  {
                                                      nextTrack->setEnabled(false);
                                                  });
        auto victoryStars = CallFunc::create([this]()
                                             {
                                                 SimonGame::animationStars(numeroDeEstrelasPontos);
                                             });
        auto playApplause = CallFunc::create([this]()
                                             {
                                                 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/Applause.mp3");
                                                 SimpleAudioEngine::getInstance()->playBackgroundMusic("audio_fundo.mp3", true);
                                             });
        auto playVoiceParabens = CallFunc::create([this]()
                                             {
                                                 SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/AudioParabens.mp3");
                                             });
        auto playVoiceMuitoBem = CallFunc::create([this]()
                                                  {
                                                      SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/AudioMuitoBom.mp3");
                                                  });
        auto playVoiceContinuePraticando = CallFunc::create([this]()
                                                  {
                                                      SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/AudioContinuePraticando.mp3");
                                                  });
        auto animButtonMenu = Sequence::create(ScaleTo::create(0.1,0),enableButtonMenu,nullptr);
        auto animButtonSongs = Sequence::create(ScaleTo::create(0.1,0),enableButtonSongs,nullptr);
        
        auto animDir = Sequence::create(
                                        MoveTo::create(1.9,Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height)),
                                        nullptr);
        auto animEsc = Sequence::create(
                                        MoveTo::create(1.9,Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height)),
                                        nullptr);
        
        auto x = EaseBounceOut::create(MoveTo::create(1.7f,Vec2(origin.x + _screenSize.width/2, origin.y + _screenSize.height + WoodenPlate->getContentSize().height/10)));
        if(frameSize.width >=1024)
            x = EaseBounceOut::create(MoveTo::create(1.7f,Vec2(origin.x + _screenSize.width/2, origin.y + _screenSize.height + WoodenPlate->getContentSize().height/10)));
        else
            x = EaseBounceOut::create(MoveTo::create(1.7f,Vec2(origin.x + _screenSize.width/2, origin.y + _screenSize.height + WoodenPlate->getContentSize().height/7)));
        
        cortinaDir->runAction(animDir); //right courtain animation
        cortinaEsc->runAction(animEsc); //left courtain animation
        
        if(numeroDeEstrelasPontos == 3)
        {
            WoodenPlate->runAction(Sequence::create(playApplause,DelayTime::create(2),playVoiceParabens,x ,victoryStars,nullptr)); //Animate victory plate
            blackFadeReplayBotton->runAction(FadeOut::create(0.4f));
        }
        else if(numeroDeEstrelasPontos == 2)
        {
            muitoBemP->runAction(Sequence::create(playApplause,DelayTime::create(2),playVoiceMuitoBem,x ,victoryStars,nullptr));
            blackFadeReplayBotton->runAction(FadeOut::create(0.4f));
        }
        else if(numeroDeEstrelasPontos == 1)
        {
            continuePraticando->runAction(Sequence::create(playApplause,DelayTime::create(2),playVoiceContinuePraticando,x ,victoryStars,nullptr));
            blackFadeReplayBotton->runAction(FadeOut::create(0.4f));
        }
        backButton->runAction(animButtonMenu);
        nextTrack->runAction(animButtonSongs);
    }
}
void SimonGame::animationStars(int numeroDeEstrelas) // FINAL STARS(MUSIC POINTS)
{
    auto sequenciaStar1 = Sequence::create(DelayTime::create(.1f),FadeIn::create(1.2f),nullptr);
    auto sequenciaStar2 = Sequence::create(DelayTime::create(.2f),FadeIn::create(1.2f),nullptr);
    auto sequenciaStar3 = Sequence::create(DelayTime::create(.3f),FadeIn::create(1.2f),nullptr);
    
    auto sequenciaStar1_2 = Sequence::create(DelayTime::create(.1f),FadeIn::create(1.2f),nullptr);
    auto sequenciaStar2_2 = Sequence::create(DelayTime::create(.2f),FadeIn::create(1.2f),nullptr);
    auto sequenciaStar3_2 = Sequence::create(DelayTime::create(.3f),FadeIn::create(1.2f),nullptr);
    
    auto sequenciaStar1_3 = Sequence::create(DelayTime::create(.1f),FadeIn::create(1.2f),nullptr);
    auto sequenciaStar2_3 = Sequence::create(DelayTime::create(.2f),FadeIn::create(1.2f),nullptr);
    auto sequenciaStar3_3 = Sequence::create(DelayTime::create(.3f),FadeIn::create(1.2f),nullptr);

    if(numeroDeEstrelas == 1)
    {
        congratStar1->runAction(sequenciaStar1);
        congratStar1_2->runAction(sequenciaStar1_2);
        congratStar1_3->runAction(sequenciaStar1_3);
    }
    else if(numeroDeEstrelas == 2)
    {
        congratStar1->runAction(sequenciaStar1);
        congratStar1_2->runAction(sequenciaStar1_2);
        congratStar1_3->runAction(sequenciaStar1_3);
        congratStar2->runAction(sequenciaStar2);
        congratStar2_2->runAction(sequenciaStar2_2);
        congratStar2_3->runAction(sequenciaStar2_3);
    }
    else if(numeroDeEstrelas == 3)
    {
        congratStar1->runAction(sequenciaStar1);
        congratStar1_2->runAction(sequenciaStar1_2);
        congratStar1_3->runAction(sequenciaStar1_3);
        congratStar2->runAction(sequenciaStar2);
        congratStar2_2->runAction(sequenciaStar2_2);
        congratStar2_3->runAction(sequenciaStar2_3);
        congratStar3->runAction(sequenciaStar3);
        congratStar3_2->runAction(sequenciaStar3_2);
        congratStar3_3->runAction(sequenciaStar3_3);
    }
}
void SimonGame::manageSound(float dt)
{
    if(!paused)
    {

        auto play_Sound_ChangeColor = CallFunc::create([this]()
        {
            SimonGame::playSound(keyIndex);
            if(_Musica[keyIndex].getStar() == 1)
            {
                SimonGame::animateBright(bright1,star1);
            }
            else if(_Musica[keyIndex].getStar() == 2)
            {
                SimonGame::animateBright(bright2,star2);
            }
            else if(_Musica[keyIndex].getStar() == 3)
            {
                SimonGame::animateBright(bright3,star3);
            }
            else if(_Musica[keyIndex].getStar() == 4)
            {
                SimonGame::animateBright(bright4,star4);
            }
            else if(_Musica[keyIndex].getStar() == 5)
            {
                SimonGame::animateBright(bright5,star5);
            }
            else if(_Musica[keyIndex].getStar() == 6)
            {
                SimonGame::animateBright(bright6,star6);
            }
            else if(_Musica[keyIndex].getStar() == 7)
            {
                SimonGame::animateBright(bright7,star7);
            }
            
        });


        auto updateKeyIndex = CallFunc::create([this]()
        {
            if(_Musica[keyIndex].getStar() == 1)
            {
                star1->setEnabled(true);
            }
            else if(_Musica[keyIndex].getStar() == 2)
            {
                star2->setEnabled(true);
            }
            else if(_Musica[keyIndex].getStar() == 3)
            {
                star3->setEnabled(true);
            }
            else if(_Musica[keyIndex].getStar() == 4)
            {
                star4->setEnabled(true);
            }
            else if(_Musica[keyIndex].getStar() == 5)
            {
                star5->setEnabled(true);
            }
            else if(_Musica[keyIndex].getStar() == 6)
            {
                star6->setEnabled(true);
            }
            else if(_Musica[keyIndex].getStar() == 7)
            {
                star7->setEnabled(true);
            }
            if(keyIndex <_Musica.size())
            {
                keyIndex += 1;
                keySoundinBlock += 1;
            }        
        });

        auto repeatMusic = CallFunc::create([this]()
        {        
            if(keySoundinBlock < intervalosMusica[keyIndexAux - 1])
                SimonGame::manageSound(0);
            else          //RUN THIS IF THE MUSIC BLOCK ENDS
            {
                doneSequence = true;
                suaVez->setScale(_screenSize.width*0.00045);
                suaVez->setRotation(0);
                if(flagTextAngle)
                {
                    suaVez->setRotation(-20);
                    flagTextAngle = !flagTextAngle;
                }
                else
                {
                    suaVez->setRotation(20);
                    flagTextAngle = !flagTextAngle;
                }
            }
        });
        auto recursividade = Sequence::create(play_Sound_ChangeColor,DelayTime::create(_Musica[keyIndex].getTime()),updateKeyIndex,repeatMusic,nullptr);
        background->runAction(recursividade);
    }
}
void SimonGame::manageSoundFinal(float dt)
{
    if(!paused)
    {

        auto play_Sound_ChangeColor = CallFunc::create([this]()
        {
            SimonGame::playSound(keyIndexFinal);

            if(_Musica[keyIndexFinal].getStar() == 1)
            {
                SimonGame::animateBright(bright1,star1);
                SimonGame::animateKeys();
            }
            else if(_Musica[keyIndexFinal].getStar() == 2)
            {
                SimonGame::animateBright(bright2,star2);
                SimonGame::animateKeys();
            }
            else if(_Musica[keyIndexFinal].getStar() == 3)
            {
                SimonGame::animateBright(bright3,star3);
                SimonGame::animateKeys();
            }
            else if(_Musica[keyIndexFinal].getStar() == 4)
            {
                SimonGame::animateBright(bright4,star4);
                SimonGame::animateKeys();
            }
            else if(_Musica[keyIndexFinal].getStar() == 5)
            {
                SimonGame::animateBright(bright5,star5);
                SimonGame::animateKeys();
            }
            else if(_Musica[keyIndexFinal].getStar() == 6)
            {
                SimonGame::animateBright(bright6,star6);
                SimonGame::animateKeys();
            }
            else if(_Musica[keyIndexFinal].getStar() == 7)
            {
                SimonGame::animateBright(bright7,star7);
                SimonGame::animateKeys();
            }
        });


        auto updateKeyIndex = CallFunc::create([this]()
        {
            if(_Musica[keyIndexFinal].getStar() == 1)
            {
                star1->setEnabled(true);
            }
            else if(_Musica[keyIndexFinal].getStar() == 2)
            {
                star2->setEnabled(true);
            }
            else if(_Musica[keyIndexFinal].getStar() == 3)
            {
                star3->setEnabled(true);
            }
            else if(_Musica[keyIndexFinal].getStar() == 4)
            {
                star4->setEnabled(true);
            }
            else if(_Musica[keyIndexFinal].getStar() == 5)
            {
                star5->setEnabled(true);
            }
            else if(_Musica[keyIndexFinal].getStar() == 6)
            {
                star6->setEnabled(true);
            }
            else if(_Musica[keyIndexFinal].getStar() == 7)
            {
                star7->setEnabled(true);
            }
            if(keyIndexFinal <_Musica.size())
            {
                keyIndexFinal += 1;
            }        
        });

        auto repeatMusic = CallFunc::create([this]()
        {        
            if(keyIndexFinal < _Musica.size())
                SimonGame::manageSoundFinal(0);
            else
            {
                //waveIsVisible = false;
                if(numeroDeErros <= 0)
                    numeroDeEstrelasPontos = 3;
                else if(numeroDeErros > 0 && numeroDeErros <= intervalosMusica.size()/2)
                    numeroDeEstrelasPontos = 2;
                else
                    numeroDeEstrelasPontos = 1;
                UserDefault *def = UserDefault::getInstance();
                if(musicNumber == 1)
                {
                    if(numeroDeEstrelasPontos > def->getIntegerForKey("HIGHSCOREM1",0))
                    {
                        def-> setIntegerForKey("HIGHSCOREM1", numeroDeEstrelasPontos);
                        def->flush();
                    }
                }
                else if(musicNumber == 2)
                {
                    if(numeroDeEstrelasPontos > def->getIntegerForKey("HIGHSCOREM2",0))
                    {
                        def-> setIntegerForKey("HIGHSCOREM2", numeroDeEstrelasPontos);
                        def->flush();
                    }
                }
                else if(musicNumber == 3)
                {
                    if(numeroDeEstrelasPontos > def->getIntegerForKey("HIGHSCOREM3",0))
                    {
                        def-> setIntegerForKey("HIGHSCOREM3", numeroDeEstrelasPontos);
                        def->flush();
                    }
                }
                else if(musicNumber == 4)
                {
                    if(numeroDeEstrelasPontos > def->getIntegerForKey("HIGHSCOREM4",0))
                    {
                        def-> setIntegerForKey("HIGHSCOREM4", numeroDeEstrelasPontos);
                        def->flush();
                    }
                }
                else if(musicNumber == 5)
                {
                    if(numeroDeEstrelasPontos > def->getIntegerForKey("HIGHSCOREM5",0))
                    {
                        def-> setIntegerForKey("HIGHSCOREM5", numeroDeEstrelasPontos);
                        def->flush();
                    }
                }
                else if(musicNumber == 6)
                {
                    if(numeroDeEstrelasPontos > def->getIntegerForKey("HIGHSCOREM6",0))
                    {
                        def-> setIntegerForKey("HIGHSCOREM6", numeroDeEstrelasPontos);
                        def->flush();
                    }
                }
                SimonGame::animateVictory(0);// MOSTRA TELA DE VITÓRIA
            }
        });

        auto sayLegal = CallFunc::create([this]()
        {
            tuhuLegal->runAction(Sequence::create(EaseElasticOut::create(ScaleTo::create(0.5f,_screenSize.width*0.0004)),DelayTime::create(2),EaseElasticOut::create(ScaleTo::create(0.5f,0)),nullptr));
            blackFadeReplayBotton->runAction(FadeIn::create(0.4f));
            bar->runAction(EaseBackIn::create( MoveTo::create(0.7f,Vec2(bar->getPositionX(), origin.y - bar->getContentSize().height))));
            pro->runAction(EaseBackIn::create( MoveTo::create(0.7f,Vec2(pro->getPositionX(), origin.y - pro->getContentSize().height))));
            bola->runAction(EaseBackIn::create( MoveTo::create(0.7f,Vec2(bola->getPositionX(), origin.y - bola->getContentSize().height))));
            bbola->runAction(EaseBackIn::create( MoveTo::create(0.7f,Vec2(bbola->getPositionX(), origin.y - bbola->getContentSize().height))));
            bola2->runAction(EaseBackIn::create( MoveTo::create(0.7f,Vec2(bola2->getPositionX(), origin.y - bola2->getContentSize().height))));
            backButton->runAction(EaseBackIn::create(ScaleTo::create(0.5f,0)));
        });

        if(!falouLegal)
        {
            auto recursividade = Sequence::create(sayLegal,DelayTime::create(3.2f),play_Sound_ChangeColor,DelayTime::create(_Musica[keyIndexFinal].getTime()),updateKeyIndex,repeatMusic,nullptr);
            falouLegal = true;
            background->runAction(recursividade);
        }
        else
        {
            auto recursividade = Sequence::create(play_Sound_ChangeColor,DelayTime::create(_Musica[keyIndexFinal].getTime()),updateKeyIndex,repeatMusic,nullptr);
            background->runAction(recursividade);
        }  
    }
}
