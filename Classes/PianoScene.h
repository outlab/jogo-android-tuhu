/*
 * PianoScene.h
 *
 *  Created on: 2015. 12. 10.
 *      Author: Jimmy
 */

#ifndef PIANOSCENE_H_
#define PIANOSCENE_H_

#include "common.h"
#include "MusicInfo.h"

using namespace std;
using namespace cocos2d;

class PianoScene : public Layer {
private:
    Vec2 origin;
    Size visibleSize;
    Size _screenSize;
    Size frameSize;
    Node* _jogo;
    float _margin;
    Button* _backBoy;
    Button* btnBackTrain;
    cocostudio::timeline::ActionTimeline* _boyAction;
    float counterValue;
    int indexReplayList;
    MenuItemImage* replayButton;
    MenuItemImage* nextTrack;
    //MenuItemImage* replayButton2;
    MenuItemImage* replayButton2;
    MenuItemImage* replayButton3;
    MenuItemImage* backButton;
    MenuItemImage* backButton2;
    MenuItemImage* backButton3;
    MenuItemImage* backButton4;
    MenuItemImage* nextTrack2;
    MenuItemImage* nextTrack3;
    MenuItemImage* nextTrack4;
    MenuItemImage* btReplayVideo;
    Menu* menuReplay;
    bool boolStartCounter;

    Node* _muteBgMusic;
    Node* _iconBgMusic1, *_iconBgMusic2, *_iconBgMusic3;

    Vec2 _posBgMusic;

    bool _playBgMusic;
    Rect _rcBgMusic;

    Node* _piano;

    Sprite* dourado;
    Sprite* blackBG;
    Sprite* blackBG2;
    Sprite* blackBGPlayBack;
    Sprite* C;
    Sprite* CSus;
    Sprite* D;
    Sprite* DSus;
    Sprite* E;
    Sprite* F;
    Sprite* FSus;
    Sprite* G;
    Sprite* GSus;
    Sprite* A;
    Sprite* ASus;
    Sprite* B;
    Sprite* tuhuLegal;
    Sprite* tuhuLegalFinal;
    Sprite* congratStar1;
    Sprite* congratStar2;
    Sprite* congratStar3;
    Sprite* congratStar1_2;
    Sprite* congratStar2_2;
    Sprite* congratStar3_2;
    Sprite* congratStar1_3;
    Sprite* congratStar2_3;
    Sprite* congratStar3_3;
    Sprite* muitoBemP;
    Sprite* continuePraticando;
    Sprite* WoodenPlate;
    Vector<Node*> _pianoKeys;
    Vector<Node*> _pianoHalfKeys;
    Vector<Node*> _touchedKeys;
    vector<float> timeToNote;

    MusicInfo _musicInfo;
    Node* _starGray;
    Node* _starYellow;

    float* _currentMusic;
    int _currentMusicCount;
    int _currentMusicIndex;
    bool _stopPlayStar;

    Node* _okayBoy;
    cocostudio::timeline::ActionTimeline* _okayBoyAction;
    bool _showOkayBoy;

    int _currentMusicType;

    void playPauseBgMusic();

    void onTouchButtons(Ref* pSender, Widget::TouchEventType type);

    void loadThisScene(Ref* pSender);
    void loadThisScene2(float dt);
    void backMusicScene(Ref* pSender);
    void loadMenuScene(Ref* pSender);
    void backPianoScene();
    void animationStars(int numeroDeEstrelas);
    void animateVictory(float dt);
    void loadReplayScene(Ref* pSender);
    void animateKey(Node* key);
    void startCounter(float dt);
    void backMenu();
    void backBoy();
    void touchMenu(int type);
    void readyPlayStars(float dt);
    void startPlayStars(float dt);
    float calcTime(float time);
    void showOkayBoy(float dt);
    void returnMenu(float dt);
    void startRecording();
    void startVideo(Ref* pSender);

    void sayLegal(float dt);

    void replayStars(float dt);
    void playReplay(float dt);

    Action* createJogoAction();
    Action* createBgMusicAction1();
    Action* createBgMusicAction2();
    Action* createBgMusicAction3();

    void startBGMusicAction();
    void startBGMusicAction2(float dt);
    void startBGMusicAction3(float dt);

public:
	PianoScene();
	virtual ~PianoScene();

public:
	virtual bool init();

	static Scene* scene();
    static Scene* scene(bool replayTapped, int wMusic);
    static Scene* scene(bool replayGame, int wMusic, vector<float> tappedListTime, Vector<Node*> playedKeys);
    static Scene* scene(bool replayPlayback, bool replayGame, int wMusic, vector<float> tappedListTime, Vector<Node*> playedKeys);
	CREATE_FUNC(PianoScene);

    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchMoved(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);

    void onTouchesBegan(const std::vector<Touch*>& touches, Event *unused_event);
    void onTouchesMoved(const std::vector<Touch*>& touches, Event *unused_event);
    void onTouchesEnded(const std::vector<Touch*>& touches, Event *unused_event);
};

#endif /* PIANOSCENE_H_ */