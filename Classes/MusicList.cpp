//#include "common.h"
#include "MusicList.h"
#include "SoundNoteaa.h"


void MusicList::init()
{
  SoundNoteaa musica02Cordas[] =
    {   // QUANTOS DIAS TEM O MÊS
        SoundNoteaa(2,0.792f,1),
        SoundNoteaa(3,0.696f,2),
        SoundNoteaa(4,0.648f,3),
        SoundNoteaa(4,0.72f,4),
        SoundNoteaa(5,0.72f,5),
        SoundNoteaa(4,0.696f,6),
        SoundNoteaa(3,0.672f,7),
        SoundNoteaa(3,0.672f,8),
        SoundNoteaa(5,0.768f,9),
        SoundNoteaa(3,0.624f,10),
        SoundNoteaa(4,0.648f,11),
        SoundNoteaa(3,0.744f,12),
        SoundNoteaa(4,0.744f,13),
        SoundNoteaa(1,0.72f,14),
        SoundNoteaa(2,1.704f,15)
    };
    SoundNoteaa musica02Sopro[] =
    {   // QUANTOS DIAS TEM O MÊS
        SoundNoteaa(2,0.792f,1),
        SoundNoteaa(3,0.624f,2),
        SoundNoteaa(4,0.72f,3),
        SoundNoteaa(4,0.792f,4),
        SoundNoteaa(5,0.672f,5),
        SoundNoteaa(4,0.696f,6),
        SoundNoteaa(3,0.6f,7),
        SoundNoteaa(3,0.696f,8),
        SoundNoteaa(5,0.816f,9),
        SoundNoteaa(3,0.624f,10),
        SoundNoteaa(4,0.792f,11),
        SoundNoteaa(3,0.696f,12),
        SoundNoteaa(4,0.576f,13),
        SoundNoteaa(1,0.792f,14),
        SoundNoteaa(2,1.56f,15)
    };


    SoundNoteaa musica01Sopro[] =
    {   // A DANÇA DA CARRANQUINHA
        SoundNoteaa(1,0.672f,1),
        SoundNoteaa(4,0.432f,2),
        SoundNoteaa(4,0.624f,3),
        SoundNoteaa(4,0.456f,4),
        SoundNoteaa(4,0.72f,5),
        SoundNoteaa(1,0.672f,6),
        SoundNoteaa(3,0.624f,7),
        SoundNoteaa(3,1.128f,8),
        SoundNoteaa(3,1.008f,9),
        SoundNoteaa(3,0.432f,10),
        SoundNoteaa(3,0.6f,11),
        SoundNoteaa(3,0.432f,12),
        SoundNoteaa(4,0.624f,13),
        SoundNoteaa(3,0.696f,14),
        SoundNoteaa(2,0.744f,15),
        SoundNoteaa(1,0.84f,16),
        SoundNoteaa(1,0.816f,17),
        SoundNoteaa(2,0.432f,18),
        SoundNoteaa(2,0.6f,19),
        SoundNoteaa(2,0.456f,20),
        SoundNoteaa(2,0.672f,21),
        SoundNoteaa(1,0.672f,22),
        SoundNoteaa(3,0.696f,23),
        SoundNoteaa(3,1.008f,24),
        SoundNoteaa(3,0.744f,25),
        SoundNoteaa(3,0.456f,26),
        SoundNoteaa(3,0.624f,27),
        SoundNoteaa(3,0.408f,28),
        SoundNoteaa(5,0.576f,29),
        SoundNoteaa(4,0.768f,30),
        SoundNoteaa(3,0.768f,31),
        SoundNoteaa(3,1.104f,32)
    };
    SoundNoteaa musica01Corda[] =
    {    // A DANÇA DA CARRANQUINHA
        SoundNoteaa(1,0.696f,1),
        SoundNoteaa(4,0.408f,2),
        SoundNoteaa(4,0.624f,3),
        SoundNoteaa(4,0.384f,4),
        SoundNoteaa(4,0.648f,5),
        SoundNoteaa(1,0.6f,6),
        SoundNoteaa(3,0.768f,7),
        SoundNoteaa(3,1.104f,8),
        SoundNoteaa(3,0.744f,9),
        SoundNoteaa(3,0.408f,10),
        SoundNoteaa(3,0.648f,11),
        SoundNoteaa(3,0.48f,12),
        SoundNoteaa(4,0.672f,13),
        SoundNoteaa(3,0.72f,14),
        SoundNoteaa(2,0.672f,15),
        SoundNoteaa(1,1.104f,16),
        SoundNoteaa(1,0.936f,17),
        SoundNoteaa(2,0.408f,18),
        SoundNoteaa(2,0.624f,19),
        SoundNoteaa(2,0.408f,20),
        SoundNoteaa(2,0.648f,21),
        SoundNoteaa(1,0.696f,22),
        SoundNoteaa(3,0.816f,23),
        SoundNoteaa(3,1.08f,24),
        SoundNoteaa(3,1.008f,25),
        SoundNoteaa(3,0.432f,26),
        SoundNoteaa(3,0.648f,27),
        SoundNoteaa(3,0.432f,28),
        SoundNoteaa(5,0.6f,29),
        SoundNoteaa(4,0.672f,30),
        SoundNoteaa(3,0.744f,31),
        SoundNoteaa(3,0.864f,32)
    };
    
    
    SoundNoteaa musica05Cordas[] =
    {    //NESTA RUA
        SoundNoteaa(3,0.692f,1),
        SoundNoteaa(3,0.72f,2),
        SoundNoteaa(5,0.696f,3),
        SoundNoteaa(5,0.72f,4),
        SoundNoteaa(3,0.696f,5),
        SoundNoteaa(2,0.72f,6),
        SoundNoteaa(1,0.72f,7),
        SoundNoteaa(2,0.672f,8),
        SoundNoteaa(5,0.84f,9),
        SoundNoteaa(4,0.576f,10),
        SoundNoteaa(3,1.392f,11),
        SoundNoteaa(1,1.344f,12),
        SoundNoteaa(3,0.672f,13),
        SoundNoteaa(3,0.72f,14),
        SoundNoteaa(5,0.72f,15),
        SoundNoteaa(5,0.744f,16),
        SoundNoteaa(4,0.648f,17),
        SoundNoteaa(3,0.672f,18),
        SoundNoteaa(4,0.744f,19),
        SoundNoteaa(3,0.72f,20),
        SoundNoteaa(5,0.672f,21),
        SoundNoteaa(4,0.744f,22),
        SoundNoteaa(3,2.88f,23),
        SoundNoteaa(2,0.816f,24),
        SoundNoteaa(2,0.792f,25),
        SoundNoteaa(3,0.672f,26),
        SoundNoteaa(3,0.696f,27),
        SoundNoteaa(5,0.696f,28),
        SoundNoteaa(4,0.672f,29),
        SoundNoteaa(4,0.744f,30),
        SoundNoteaa(3,0.672f,31),
        SoundNoteaa(2,0.696f,32),
        SoundNoteaa(1,0.744f,33),
        SoundNoteaa(3,1.32f,34),
        SoundNoteaa(2,1.488f,35),
        SoundNoteaa(2,0.72f,36),
        SoundNoteaa(1,0.696f,37),
        SoundNoteaa(2,1.296f,38),
        SoundNoteaa(5,0.696f,39),
        SoundNoteaa(4,0.672f,40),
        SoundNoteaa(3,0.72f,41),
        SoundNoteaa(4,0.672f,42),
        SoundNoteaa(3,0.672f,43),
        SoundNoteaa(2,0.744f,44),
        SoundNoteaa(1,4.24f,45),
        SoundNoteaa(2,0.768f,46),
        SoundNoteaa(2,0.672f,47),
        SoundNoteaa(3,0.672f,48),
        SoundNoteaa(3,0.696f,49),
        SoundNoteaa(5,0.72f,50),
        SoundNoteaa(4,0.672f,51),
        SoundNoteaa(4,0.768f,52),
        SoundNoteaa(3,0.672f,53),
        SoundNoteaa(2,0.744f,54),
        SoundNoteaa(1,0.72f,55),
        SoundNoteaa(3,1.296f,56),
        SoundNoteaa(2,1.488f,57),
        SoundNoteaa(2,0.72f,58),
        SoundNoteaa(1,0.672f,59),
        SoundNoteaa(2,1.2f,60),
        SoundNoteaa(5,0.768f,61),
        SoundNoteaa(4,0.816f,62),
        SoundNoteaa(3,0.696f,63),
        SoundNoteaa(4,0.672f,64),
        SoundNoteaa(3,0.72f,65),
        SoundNoteaa(2,0.72f,66),
        SoundNoteaa(1,2.976f,67)
    };
    
    
    SoundNoteaa musica05Sopro[] =
    {    //NESTA RUA
        SoundNoteaa(3,0.692f,1),
        SoundNoteaa(3,0.72f,2),
        SoundNoteaa(5,0.696f,3),
        SoundNoteaa(5,0.72f,4),
        SoundNoteaa(3,0.696f,5),
        SoundNoteaa(2,0.72f,6),
        SoundNoteaa(1,0.72f,7),
        SoundNoteaa(2,0.672f,8),
        SoundNoteaa(5,0.84f,9),
        SoundNoteaa(4,0.576f,10),
        SoundNoteaa(3,1.392f,11),
        SoundNoteaa(1,1.344f,12),
        SoundNoteaa(3,0.672f,13),
        SoundNoteaa(3,0.72f,14),
        SoundNoteaa(5,0.72f,15),
        SoundNoteaa(5,0.744f,16),
        SoundNoteaa(4,0.648f,17),
        SoundNoteaa(3,0.672f,18),
        SoundNoteaa(4,0.744f,19),
        SoundNoteaa(3,0.72f,20),
        SoundNoteaa(5,0.672f,21),
        SoundNoteaa(4,0.744f,22),
        SoundNoteaa(3,2.88f,23),
        SoundNoteaa(2,0.816f,24),
        SoundNoteaa(2,0.792f,25),
        SoundNoteaa(3,0.672f,26),
        SoundNoteaa(3,0.696f,27),
        SoundNoteaa(5,0.696f,28),
        SoundNoteaa(4,0.672f,29),
        SoundNoteaa(4,0.744f,30),
        SoundNoteaa(3,0.672f,31),
        SoundNoteaa(2,0.696f,32),
        SoundNoteaa(1,0.744f,33),
        SoundNoteaa(3,1.32f,34),
        SoundNoteaa(2,1.488f,35),
        SoundNoteaa(2,0.72f,36),
        SoundNoteaa(1,0.696f,37),
        SoundNoteaa(2,1.296f,38),
        SoundNoteaa(5,0.696f,39),
        SoundNoteaa(4,0.672f,40),
        SoundNoteaa(3,0.72f,41),
        SoundNoteaa(4,0.672f,42),
        SoundNoteaa(3,0.672f,43),
        SoundNoteaa(2,0.744f,44),
        SoundNoteaa(1,3.024f,45),
        SoundNoteaa(2,0.768f,46),
        SoundNoteaa(2,0.672f,47),
        SoundNoteaa(3,0.672f,48),
        SoundNoteaa(3,0.696f,49),
        SoundNoteaa(5,0.72f,50),
        SoundNoteaa(4,0.672f,51),
        SoundNoteaa(4,0.768f,52),
        SoundNoteaa(3,0.672f,53),
        SoundNoteaa(2,0.744f,54),
        SoundNoteaa(1,0.72f,55),
        SoundNoteaa(3,1.296f,56),
        SoundNoteaa(2,1.488f,57),
        SoundNoteaa(2,0.72f,58),
        SoundNoteaa(1,0.672f,59),
        SoundNoteaa(2,1.2f,60),
        SoundNoteaa(5,0.768f,61),
        SoundNoteaa(4,0.816f,62),
        SoundNoteaa(3,0.696f,63),
        SoundNoteaa(4,0.672f,64),
        SoundNoteaa(3,0.72f,65),
        SoundNoteaa(2,0.72f,66),
        SoundNoteaa(1,2.976f,67)
     
    };
    
    
    SoundNoteaa musica03Corda[] =
    {  //LA NA PONTE DA VIZINHANÇA
        SoundNoteaa(1,0.456f,1),
        SoundNoteaa(2,0.408f,2),
        SoundNoteaa(3,0.624f,3),
        SoundNoteaa(1,0.72f,4),
        SoundNoteaa(5,0.696f,5),
        SoundNoteaa(4,0.624f,6),
        SoundNoteaa(3,0.696f,7),
        SoundNoteaa(1,0.744f,8),
        SoundNoteaa(5,0.648f,9),
        SoundNoteaa(4,0.744f,10),
        SoundNoteaa(3,0.696f,11),
        SoundNoteaa(1,0.744f,12),
        SoundNoteaa(2,0.696f,13),
        SoundNoteaa(2,0.696f,14),
        SoundNoteaa(1,1.776f,15),
        SoundNoteaa(1,0.624f,16),
        SoundNoteaa(2,0.36f,17),
        SoundNoteaa(3,0.672f,18),
        SoundNoteaa(1,0.72f,19),
        SoundNoteaa(5,0.624f,20),
        SoundNoteaa(4,0.72f,21),
        SoundNoteaa(3,0.744f,22),
        SoundNoteaa(1,0.72f,23),
        SoundNoteaa(5,0.72f,24),
        SoundNoteaa(4,0.624f,25),
        SoundNoteaa(3,0.72f,26),
        SoundNoteaa(1,0.72f,27),
        SoundNoteaa(2,0.648f,28),
        SoundNoteaa(2,0.744f,29),
        SoundNoteaa(5,2.064f,30)
    };

    SoundNoteaa musica03Sopro[] =
    {  //LA NA PONTE DA VIZINHAÇA
        SoundNoteaa(1,0.456f,1),
        SoundNoteaa(2,0.432f,2),
        SoundNoteaa(3,0.624f,3),
        SoundNoteaa(1,0.672f,4),
        SoundNoteaa(5,0.696f,5),
        SoundNoteaa(4,0.696f,6),
        SoundNoteaa(3,0.744f,7),
        SoundNoteaa(1,0.672f,8),
        SoundNoteaa(5,0.744f,9),
        SoundNoteaa(4,0.672f,10),
        SoundNoteaa(3,0.744f,11),
        SoundNoteaa(1,0.696f,12),
        SoundNoteaa(2,0.72f,13),
        SoundNoteaa(2,0.72f,14),
        SoundNoteaa(1,1.656f,15),
        SoundNoteaa(1,0.768f,16),
        SoundNoteaa(2,0.384f,17),
        SoundNoteaa(3,0.648f,18),
        SoundNoteaa(1,0.696f,19),
        SoundNoteaa(5,0.672f,20),
        SoundNoteaa(4,0.672f,21),
        SoundNoteaa(3,0.768f,22),
        SoundNoteaa(1,0.696f,23),
        SoundNoteaa(5,0.72f,24),
        SoundNoteaa(4,0.6f,25),
        SoundNoteaa(3,0.792f,26),
        SoundNoteaa(1,0.672f,27),
        SoundNoteaa(2,0.672f,28),
        SoundNoteaa(2,0.768f,29),
        SoundNoteaa(5,1.584f,30)
    };
    
    
    SoundNoteaa musica04Sopro[] =
    {  //ATCHE
        SoundNoteaa(4,1.104f,1),
        SoundNoteaa(4,0.456f,2),
        SoundNoteaa(1,1.32f,3),
        SoundNoteaa(3,1.104f,4),
        SoundNoteaa(5,0.432f,5),
        SoundNoteaa(4,1.056f,6),
        SoundNoteaa(3,1.272f,7),
        SoundNoteaa(2,1.368f,8),
        SoundNoteaa(2,1.44f,9),
        SoundNoteaa(2,1.392f,10),
        SoundNoteaa(2,1.416f,11),
        SoundNoteaa(1,1.296f,12),
        SoundNoteaa(5,1.752f,13),
        SoundNoteaa(4,1.368f,14),
        SoundNoteaa(3,1.32f,15),
        SoundNoteaa(2,1.392f,16),
        SoundNoteaa(2,2.04f,17),
        SoundNoteaa(4,1.416f,18),
        SoundNoteaa(4,0.432f,19),
        SoundNoteaa(1,1.32f,20),
        SoundNoteaa(3,1.032f,21),
        SoundNoteaa(5,0.408f,22),
        SoundNoteaa(4,0.984f,23),
        SoundNoteaa(3,1.344f,24),
        SoundNoteaa(2,1.416f,25),
        SoundNoteaa(2,1.464f,26),
        SoundNoteaa(2,1.44f,27),
        SoundNoteaa(2,1.368f,28),
        SoundNoteaa(1,1.248f,29),
        SoundNoteaa(5,1.68f,30),
        SoundNoteaa(4,1.44f,31),
        SoundNoteaa(3,1.344f,32),
        SoundNoteaa(2,1.392f,33),
        SoundNoteaa(2,1.584f,34)
    };
    
    SoundNoteaa musica04Cordas[] =
    {  //ATCHE
        SoundNoteaa(4,1.104f,1),
        SoundNoteaa(4,0.456f,2),
        SoundNoteaa(1,1.32f,3),
        SoundNoteaa(3,1.104f,4),
        SoundNoteaa(5,0.432f,5),
        SoundNoteaa(4,1.056f,6),
        SoundNoteaa(3,1.272f,7),
        SoundNoteaa(2,1.368f,8),
        SoundNoteaa(2,1.24f,9),
        SoundNoteaa(2,1.292f,10),
        SoundNoteaa(2,1.216f,11),
        SoundNoteaa(1,1.196f,12),
        SoundNoteaa(5,1.536f,13),
        SoundNoteaa(4,1.568f,14),
        SoundNoteaa(3,1.32f,15),
        SoundNoteaa(2,1.392f,16),
        SoundNoteaa(2,2.04f,17),
        SoundNoteaa(4,1.416f,18),
        SoundNoteaa(4,0.432f,19),
        SoundNoteaa(1,1.32f,20),
        SoundNoteaa(3,1.032f,21),
        SoundNoteaa(5,0.432f,22),
        SoundNoteaa(4,1.054f,23),
        SoundNoteaa(3,1.344f,24),
        SoundNoteaa(2,1.416f,25),
        SoundNoteaa(2,1.464f,26),
        SoundNoteaa(2,1.44f,27),
        SoundNoteaa(2,1.368f,28),
        SoundNoteaa(1,1.248f,29),
        SoundNoteaa(5,1.68f,30),
        SoundNoteaa(4,1.44f,31),
        SoundNoteaa(3,1.344f,32),
        SoundNoteaa(2,1.392f,33),
        SoundNoteaa(2,1.584f,34)
    };
    
    SoundNoteaa musica06Cordas[] =
    {    //CONDESSA
        SoundNoteaa(1,0.432f,1),
        SoundNoteaa(2,0.384f,2),
        SoundNoteaa(3,0.648f,3),
        SoundNoteaa(1,0.696f,4),
        SoundNoteaa(5,0.72f,5),
        SoundNoteaa(4,0.696f,6),
        SoundNoteaa(3,0.672f,7),
        SoundNoteaa(1,0.72f,8),
        SoundNoteaa(3,0.384f,9),
        SoundNoteaa(3,0.336f,10),
        SoundNoteaa(3,0.432f,11),
        SoundNoteaa(5,0.408f,12),
        SoundNoteaa(4,1.056f,13),
        SoundNoteaa(3,0.408f,14),
        SoundNoteaa(2,0.696f,15),
        SoundNoteaa(1,0.36f,16),
        SoundNoteaa(1,0.408f,17),
        SoundNoteaa(2,0.672f,18),
        SoundNoteaa(2,0.84f,19),
        SoundNoteaa(2,0.72f,20),
        SoundNoteaa(3,0.384f,21),
        SoundNoteaa(4,1.344f,22),
        SoundNoteaa(5,1.296f,23),
        SoundNoteaa(3,1.44f,24),
        SoundNoteaa(3,0.408f,25),
        SoundNoteaa(3,0.36f,26),
        SoundNoteaa(3,0.384f,27),
        SoundNoteaa(5,0.432f,28),
        SoundNoteaa(4,0.984f,29),
        SoundNoteaa(3,0.432f,30),
        SoundNoteaa(2,0.696f,31),
        SoundNoteaa(1,0.312f,32),
        SoundNoteaa(1,0.456f,33),
        SoundNoteaa(2,0.648f,34),
        SoundNoteaa(2,0.96f,35),
        SoundNoteaa(2,0.432f,36),
        SoundNoteaa(3,0.432f,37),
        SoundNoteaa(4,0.624f,38),
        SoundNoteaa(2,0.696f,39),
        SoundNoteaa(5,0.744f,40),
        SoundNoteaa(4,0.672f,41),
        SoundNoteaa(3,0.744f,42),
        SoundNoteaa(1,0.672f,43),
        SoundNoteaa(3,0.36f,44),
        SoundNoteaa(3,0.384f,45),
        SoundNoteaa(3,0.408f,46),
        SoundNoteaa(5,0.408f,47),
        SoundNoteaa(4,0.672f,48),
        SoundNoteaa(3,0.744f,49),
        SoundNoteaa(2,0.768f,50),
        SoundNoteaa(1,0.408f,51),
        SoundNoteaa(1,0.408f,52),
        SoundNoteaa(2,0.648f,53),
        SoundNoteaa(2,0.984f,54),
        SoundNoteaa(2,0.696f,55),
        SoundNoteaa(3,0.408f,56),
        SoundNoteaa(4,1.32f,57),
        SoundNoteaa(5,1.32f,58),
        SoundNoteaa(3,1.368f,59),
        SoundNoteaa(3,0.384f,60),
        SoundNoteaa(3,0.408f,61),
        SoundNoteaa(3,0.384f,62),
        SoundNoteaa(5,0.408f,63),
        SoundNoteaa(4,0.984f,64),
        SoundNoteaa(4,0.432f,65),
        SoundNoteaa(3,0.72f,66),
        SoundNoteaa(2,0.384f,67),
        SoundNoteaa(2,0.408f,68),
        SoundNoteaa(1,0.672f,69),
        SoundNoteaa(1,0.696f,70)
    };
    
    SoundNoteaa musica06Sopro[] =
    {    //CONDESSA
        SoundNoteaa(1,0.432f,1),
        SoundNoteaa(2,0.384f,2),
        SoundNoteaa(3,0.648f,3),
        SoundNoteaa(1,0.696f,4),
        SoundNoteaa(5,0.72f,5),
        SoundNoteaa(4,0.696f,6),
        SoundNoteaa(3,0.672f,7),
        SoundNoteaa(1,0.72f,8),
        SoundNoteaa(3,0.384f,9),
        SoundNoteaa(3,0.336f,10),
        SoundNoteaa(3,0.432f,11),
        SoundNoteaa(5,0.408f,12),
        SoundNoteaa(4,1.056f,13),
        SoundNoteaa(3,0.408f,14),
        SoundNoteaa(2,0.696f,15),
        SoundNoteaa(1,0.36f,16),
        SoundNoteaa(1,0.408f,17),
        SoundNoteaa(2,0.672f,18),
        SoundNoteaa(2,0.84f,19),
        SoundNoteaa(2,0.72f,20),
        SoundNoteaa(3,0.384f,21),
        SoundNoteaa(4,1.344f,22),
        SoundNoteaa(5,1.296f,23),
        SoundNoteaa(3,1.44f,24),
        SoundNoteaa(3,0.408f,25),
        SoundNoteaa(3,0.36f,26),
        SoundNoteaa(3,0.384f,27),
        SoundNoteaa(5,0.432f,28),
        SoundNoteaa(4,0.984f,29),
        SoundNoteaa(3,0.432f,30),
        SoundNoteaa(2,0.696f,31),
        SoundNoteaa(1,0.312f,32),
        SoundNoteaa(1,0.456f,33),
        SoundNoteaa(2,0.648f,34),
        SoundNoteaa(2,0.96f,35),
        SoundNoteaa(2,0.432f,36),
        SoundNoteaa(3,0.432f,37),
        SoundNoteaa(4,0.624f,38),
        SoundNoteaa(2,0.696f,39),
        SoundNoteaa(5,0.744f,40),
        SoundNoteaa(4,0.672f,41),
        SoundNoteaa(3,0.744f,42),
        SoundNoteaa(1,0.672f,43),
        SoundNoteaa(3,0.36f,44),
        SoundNoteaa(3,0.384f,45),
        SoundNoteaa(3,0.408f,46),
        SoundNoteaa(5,0.408f,47),
        SoundNoteaa(4,0.672f,48),
        SoundNoteaa(3,0.744f,49),
        SoundNoteaa(2,0.768f,50),
        SoundNoteaa(1,0.408f,51),
        SoundNoteaa(1,0.408f,52),
        SoundNoteaa(2,0.648f,53),
        SoundNoteaa(2,0.984f,54),
        SoundNoteaa(2,0.696f,55),
        SoundNoteaa(3,0.408f,56),
        SoundNoteaa(4,1.32f,57),
        SoundNoteaa(5,1.32f,58),
        SoundNoteaa(3,1.368f,59),
        SoundNoteaa(3,0.384f,60),
        SoundNoteaa(3,0.408f,61),
        SoundNoteaa(3,0.384f,62),
        SoundNoteaa(5,0.408f,63),
        SoundNoteaa(4,0.984f,64),
        SoundNoteaa(4,0.432f,65),
        SoundNoteaa(3,0.72f,66),
        SoundNoteaa(2,0.384f,67),
        SoundNoteaa(2,0.408f,68),
        SoundNoteaa(1,0.672f,69),
        SoundNoteaa(1,0.696f,70)
    };

    
    
    for(int z = 0; z < 34; z++)
    {
        _Musica04Sopro.push_back(musica04Sopro[z]);
        timeMusica04Sopro += musica04Sopro[z].getTime();
    }
    for(int z = 0; z < 34; z++)
    {
        _Musica04Cordas.push_back(musica04Cordas[z]);
        timeMusica04Cordas += musica04Cordas[z].getTime();
    }
    
    for(int z = 0; z < 70; z++)
    {
        _Musica06Sopro.push_back(musica06Sopro[z]);
        timeMusica06Sopro += musica06Sopro[z].getTime();
    }
    for(int z = 0; z < 70; z++)
    {
        _Musica06Cordas.push_back(musica06Cordas[z]);
        timeMusica06Cordas += musica06Cordas[z].getTime();
    }
    
    for(int z = 0; z < 30; z++)
    {
        _Musica03Sopro.push_back(musica03Sopro[z]);
        timeMusica03Sopro += musica03Sopro[z].getTime();
    }
    for(int z = 0; z < 30; z++)
    {
        _Musica03Cordas.push_back(musica03Corda[z]);
        timeMusica03Cordas += musica03Corda[z].getTime();
    }

    for(int z = 0; z < 15; z++)
    {
        _Musica02Sopro.push_back(musica02Sopro[z]);
        timeMusica02Sopro += musica02Sopro[z].getTime();
    }
    for(int z = 0; z < 15; z++)
    {
        _Musica02Cordas.push_back(musica02Cordas[z]);
        timeMusica02Cordas += musica02Cordas[z].getTime();
    }

    for(int z = 0; z < 32; z++)
    {
        _Musica01Sopro.push_back(musica01Sopro[z]);
        timeMusica01Sopro += musica01Sopro[z].getTime();
    }
    for(int z = 0; z < 32; z++)
    {
        _Musica01Corda.push_back(musica01Corda[z]);
        timeMusica01Corda += musica01Corda[z].getTime();
    }

    for(int i = 0; i < 67; i++) {
        _Musica05Sopro.push_back (musica05Sopro[i]);
        timeMusica05sopro += musica05Sopro[i].getTime();
    }
    for(int i = 0; i < 67; i++) {
        _Musica05Cordas.push_back (musica05Cordas[i]);
        timeMusica05Cordas += musica05Cordas[i].getTime();
    }
}

MusicList::MusicList()
{

}

MusicList::~MusicList() {
    // TODO Auto-generated destructor stub
}

vector<SoundNoteaa> MusicList::getMusica(int musicNumber, int instrumentNumber)
{
    if(musicNumber == 3 && instrumentNumber == 1)
        return _Musica01Corda; //A DANÇA DA CARRANQUINHA
    else if(musicNumber == 3 && instrumentNumber == 2)
        return _Musica01Sopro; //A DANÇA DA CARRANQUINHA
    else if(musicNumber == 1 && instrumentNumber == 1)
        return _Musica02Cordas; //QUANTOS DIAS TEM O MES
    else if(musicNumber == 1 && instrumentNumber == 2)
        return _Musica02Sopro; //QUANTOS DIAS TEM O MES
    else if(musicNumber == 2 && instrumentNumber == 1)
        return _Musica03Cordas; //LA NA PONTE DA VINHACA
    else if(musicNumber == 2 && instrumentNumber == 2)
        return _Musica03Sopro; //LA NA PONTE DA VINHACA
    else if(musicNumber == 4 && instrumentNumber == 1)
        return _Musica04Cordas; //ATCHE
    else if(musicNumber == 4 && instrumentNumber == 2)
        return _Musica04Sopro; //ATCHE
    else if(musicNumber == 5 && instrumentNumber == 1)
        return _Musica05Cordas; //NESTA RUA
    else if(musicNumber == 5 && instrumentNumber == 2)
        return _Musica05Sopro; //NESTA RUA
    else if(musicNumber == 6 && instrumentNumber == 1)
        return _Musica06Cordas; //CONDESSA
    else if(musicNumber == 6 && instrumentNumber == 2)
        return _Musica06Sopro; //CONDESSA
}
/*
vector<int> MusicList::getMusicaIntervalo(int musicNumber)
{
    if(musicNumber == 1)
        return intervalosMusica01;
    else if(musicNumber == 2)
        return intervalosMusica02;
}*/