/*
 * InfoScene.cpp
 *
 *  Created on: 2015. 12. 16.
 *      Author: Jimmy
 */

#include "InfoScene.h"
#include "MenuScene.h"
#include "platform/android/jni/JniHelper.h"
#include <jni.h>

bool sobre;
InfoScene::InfoScene() {
	// TODO Auto-generated constructor stub
	_margin = 20.0f;
	//_background = nullptr;
}

InfoScene::~InfoScene() {
	// TODO Auto-generated destructor stub
}

Scene* InfoScene::scene() {
	Scene *scene = Scene::create();

	InfoScene *layer = InfoScene::create();
	scene->addChild(layer);

	return scene;
}

bool InfoScene::init() {
	if(!Layer::init())
		return false;

	sobre = true;
	_screenSize = Director::getInstance()->getWinSize();
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();

/*	auto info = CSLoader::createNode("Info/Info.csb");
	_background = info->getChildByTag(69);
	_background->setPosition(Vec2(_screenSize.width * 0.5f, _screenSize.height * 0.5f));
	this->addChild(_background, 0);

	((Button*)_background->getChildByTag(70))->addTouchEventListener(CC_CALLBACK_2(InfoScene::onButtonTouch, this));
	((Button*)_background->getChildByTag(72))->addTouchEventListener(CC_CALLBACK_2(InfoScene::onButtonTouch, this));
	((Button*)_background->getChildByTag(71))->addTouchEventListener(CC_CALLBACK_2(InfoScene::onButtonTouch, this));
	((Button*)_background->getChildByTag(73))->addTouchEventListener(CC_CALLBACK_2(InfoScene::onButtonTouch, this));
*/
	background = Sprite::create("Info/1.png");
    background->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2));
    background->setScale(_screenSize.width/(background->getContentSize().width));
    this->addChild(background,-1);
    background = Sprite::create("Info/2.png");
    background->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + visibleSize.height/2));
    background->setScale(_screenSize.width/(background->getContentSize().width));
    this->addChild(background,1);
texto1 = Sprite::create("Info/tuhuu-1.png");
    texto1->setAnchorPoint(Vec2(0.0,1.0));
    texto1->setPosition(Vec2(origin.x, origin.y + _screenSize.height/2));
    texto1->setScale(_screenSize.width*.001);
    texto1->setScaleX(1.5);
    this->addChild(texto1,0);
    
    texto2 = Sprite::create("Info/tuhuu-2.png");
    texto2->setAnchorPoint(Vec2(0.0,0.7));
    texto1->setPosition(Vec2(origin.x, origin.y + visibleSize.height/2));
    texto2->setScale(_screenSize.width*.001);
    texto2->setScaleX(1.5);
    texto2->setVisible(false);
    this->addChild(texto2,0);
    
    auto backButton = MenuItemImage::create("Info/btn_voltar.png","Info/btn_voltar.png",CC_CALLBACK_1(InfoScene::backMenu,this));
    backButton->setPosition(Vec2( backButton->getContentSize().width/2.5, origin.y + visibleSize.height - backButton->getContentSize().height/2.2));
    backButton->setScale(_screenSize.width*0.00045);
    auto menuBackButton = Menu::create(backButton,NULL);
    menuBackButton->setPosition(Point::ZERO);
    this->addChild(menuBackButton,2);
    
    auto appButton = MenuItemImage::create("Info/btn_avalie.png","Info/btn_avalie.png",CC_CALLBACK_1(InfoScene::esteApp,this));
    appButton->setPosition(Vec2(_screenSize.width  - appButton->getContentSize().width/3, origin.y + visibleSize.height - appButton->getContentSize().height/2.2));
    appButton->setScale(_screenSize.width*0.00045);
    auto menuappButton = Menu::create(appButton,NULL);
    menuappButton->setPosition(Point::ZERO);
    this->addChild(menuappButton,2);
    
    auto sobreButton = MenuItemImage::create("Info/btn_sobre.png","Info/btn_sobre_on.png",CC_CALLBACK_1(InfoScene::showSobre,this));
    sobreButton->setAnchorPoint(Vec2(0.5,1));
    sobreButton->setScale(_screenSize.width*0.00045);
    sobreButton->setPosition(Vec2(_screenSize.width/3, origin.y + visibleSize.height /*- sobreButton->getContentSize().height/45*/));
    auto menusobreButton = Menu::create(sobreButton,NULL);
    menusobreButton->setPosition(Point::ZERO);
    this->addChild(menusobreButton,2);
    
    auto fichaButton = MenuItemImage::create("Info/btn_ficha.png","Info/btn_ficha_on.png",CC_CALLBACK_1(InfoScene::showTecnica,this));
    fichaButton->setAnchorPoint(Vec2(0.5,1));
    fichaButton->setScale(_screenSize.width*0.00045);
    fichaButton->setPosition(Vec2((_screenSize.width/3)*2, origin.y + visibleSize.height/* - fichaButton->getContentSize().height/45*/));
    auto menufichaButton = Menu::create(fichaButton,NULL);
    menufichaButton->setPosition(Point::ZERO);
    this->addChild(menufichaButton,2);
    
    auto touchListener = EventListenerTouchOneByOne::create( );
    touchListener->setSwallowTouches( true );
    touchListener->onTouchBegan = CC_CALLBACK_2( InfoScene::onTouchBegan, this );
    touchListener->onTouchMoved = CC_CALLBACK_2(InfoScene::onTouchMoved, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(InfoScene::onTouchEnded, this);
    Director::getInstance( )->getEventDispatcher( )->addEventListenerWithSceneGraphPriority( touchListener, this );


	//showSobre();

	return true;
}
/*void InfoScene::onButtonTouch(Ref* pSender, Widget::TouchEventType type) {
	if(type == Widget::TouchEventType::ENDED) {
		int iTag = ((Button*)pSender)->getTag();
		switch(iTag) {
		case 70:
			backMenu();
			break;
		case 71:
			showTecnica();
			break;
		case 72:
			showSobre();
			break;
		case 73:
			esteApp();
			break;
		}
	}
}*/
void InfoScene::backMenu(Ref* pSender) {
	SimpleAudioEngine::getInstance()->playEffect("Effect/voltar.mp3");
	SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	Director::getInstance()->popScene();
}
void InfoScene::showSobre(Ref* pSender) {
	SimpleAudioEngine::getInstance()->playEffect("Effect/placas.mp3");
    texto1->setVisible(true);
    texto2->setVisible(false);
    sobre = true;
}
void InfoScene::showTecnica(Ref* pSender) {
	SimpleAudioEngine::getInstance()->playEffect("Effect/placas.mp3");
    texto1->setVisible(false);
    texto2->setVisible(true);
    sobre = false;
}
void InfoScene::esteApp(Ref* pSender) {
	SimpleAudioEngine::getInstance()->playEffect("Effect/bt.mp3");
    JniMethodInfo methodInfo;
    if(!JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/cpp/AppActivity", "showAppDetails", "()V")) {
        return;
    }

    methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
    methodInfo.env->DeleteLocalRef(methodInfo.classID);
}
void InfoScene::onTouchMoved(Touch *touch, Event *event)
{
        Vec2 ptCurrent = touch->getLocation();
        if(!_isButtonMoved && fabsf(ptCurrent.y - _ptStart.y) > 1.0f)
            _isButtonMoved = true;
        
        float fDistance = ptCurrent.y - _ptStart.y;
        
       // if(fDistance < 0 && (musicSIX->getPositionX() <= (3*(visibleSize.width/4)) + ((visibleSize.width/4)/2)))
       //     fDistance = 0;
       // if(fDistance > 0 && (musicONE->getPositionX() >= (visibleSize.width/4)))
       //     fDistance = 0;
    if(sobre)
    {
        if(texto1->getPositionY() <= _screenSize.height/2 && fDistance < 0)
            fDistance = 0;
        else if(texto1->getPositionY() >= _screenSize.height*0.98 && fDistance > 0)
            fDistance = 0;
        else
            texto1->setPosition(texto1->getPositionX(), texto1->getPositionY() + fDistance);
    }
    else
    {
        if(texto2->getPositionY() <= _screenSize.height*.35f && fDistance < 0)
            fDistance = 0;
        else if(texto2->getPositionY() >= _screenSize.height*1.2 && fDistance > 0)
            fDistance = 0;
        else
            texto2->setPosition(texto2->getPositionX(), texto2->getPositionY() + fDistance);
    }
        
        _ptStart = ptCurrent;
}
bool InfoScene::onTouchBegan(Touch *touch, Event *event)
{
        _ptStart = touch->getLocation();
        if(_ptStart.y < _screenSize.height)
        {
            //fourStrings->selected();
        }
        _ptTouch = _ptStart;
        _isButtonMoved = false;
        return true;
}
void InfoScene::onTouchEnded(Touch *touch, Event *event)
{
}