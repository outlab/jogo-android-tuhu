#ifndef __SEQUENCE_MEMORY_SCENE_H__
#define __SEQUENCE_MEMORY_SCENE_H__

#include "cocos2d.h"
//#include "ShadowLayer.h"
using namespace cocos2d;

class SequenceMemory : public cocos2d::Layer
{
private:
   Vector<int>* currentSequence;
    void GoToMusic1(Ref* pSender);
    void GoToMusic2(Ref* pSender);
    void GoToMusic3(Ref* pSender);
    void GoToMusic4(Ref* pSender);
    void GoToMusic5(Ref* pSender);
    void GoToMusic6(Ref* pSender);
    void animChoseInstrument(float dt);
    void goToGameScene(Ref* pSender);
    void rotateWheels(float dt);
    void spinBright(float dt);
    void goToGameSceneBlow(Ref* pSender);
    void goToGameSceneString(Ref* pSender);
    void backToScene(Ref* pSender);
    void changeInstrument(float dt);
    void changeButtons(float dt);
    void teste2(Touch* touch,Event* event);
    
    Size visibleSize;
    Size _screenSize;
    Vec2 origin;
    Vec2 _ptStart;
    Vec2 _ptTouch;
    bool _isButtonMoved;
    bool blackScreeOpen;

    int musicNumber;
    int stringOrBlow;
    int highScoreMusic1;
    int highScoreMusic2;
    int highScoreMusic3;
    int highScoreMusic4;
    int highScoreMusic5;
    int highScoreMusic6;
    
   /* float currentPositionXM1;
    float currentPositionXM2;
    float currentPositionXM3;
    float currentPositionXM4;
    float currentPositionXM5;
    float currentPositionXM6;*/

    float r;
    float spinL;
    Sprite* pivot;
    Sprite* leftLight;
    Sprite* mask2;
    Sprite* mask3;
    Sprite* cortinas;
    Sprite* blackBG;
    Sprite* goldLight1;
    Sprite* goldLight2;
    Sprite* star1;
    Sprite* star2;
    Sprite* star3;
    Sprite* star1G;
    Sprite* star2G;
    Sprite* star3G;
    Sprite* nome1;
    Sprite* nome2;
    Sprite* nome3;
    Sprite* nome4;
    Sprite* nome5;
    Sprite* nome6;


    Label* texto;

    
    MenuItemImage* backButton;
    MenuItemImage* backButton2;
    MenuItemImage* fourStrings;
    MenuItemImage* fourBlowing;

    UserDefault* def1;
    

  bool onTouchBegan(Touch *touch, Event *event);
  void onTouchMoved(Touch *touch, Event *event);
  void onTouchEnded(Touch *touch, Event *event);



public:

    SequenceMemory();
    static cocos2d::Scene* createScene();
    static cocos2d::Scene* createScene(int musicNumber, int StringOrBlow,bool varTrue);
    static cocos2d::Scene* createCustomScene(int musicnumber, Size visibleSize);
    void loadMenuScene(Ref* pSender);
    void loadThisScene(Ref* pSender);

    virtual bool init();
    void update(float dt);

    // implement the "static create()" method manually
    CREATE_FUNC(SequenceMemory);
};

#endif // __SEQUENCE_MEMORY_SCENE_H__