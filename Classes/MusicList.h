#ifndef _MUSIC_LIST_H_
#define _MUSIC_LIST_H_

#include "SoundNoteaa.h"

using namespace std;

class MusicList
{
private:
     vector<SoundNoteaa> _Musica01;
    vector<SoundNoteaa> _Musica02;
    vector<SoundNoteaa> _Musica01Sopro;
    vector<SoundNoteaa> _Musica01Corda;
    vector<SoundNoteaa> _Musica02Sopro;
    vector<SoundNoteaa> _Musica02Cordas;
    vector<SoundNoteaa> _Musica03Sopro;
    vector<SoundNoteaa> _Musica03Cordas;
    vector<SoundNoteaa> _Musica04Sopro;
    vector<SoundNoteaa> _Musica04Cordas;
    vector<SoundNoteaa> _Musica05Cordas;
    vector<SoundNoteaa> _Musica05Sopro;
    vector<SoundNoteaa> _Musica06Cordas;
    vector<SoundNoteaa> _Musica06Sopro;
    
    vector<int> intervalosMusica01;
    vector<int> intervalosMusica02;

    float timeMusica01;
    float timeMusica02;
    float timeMusica01Sopro;
    float timeMusica01Corda;
    float timeMusica02Sopro;
    float timeMusica02Cordas;
    float timeMusica03Sopro;
    float timeMusica03Cordas;
    float timeMusica04Sopro;
    float timeMusica04Cordas;
    float timeMusica06Sopro;
    float timeMusica06Cordas;
    float timeMusica05sopro;
    float timeMusica05Cordas;


public:

    vector<SoundNoteaa> getMusica(int musicNumber,int instrumentNumber);
    vector<int> getMusicaIntervalo(int musicNumber);

    MusicList();
    ~MusicList();
    void init();


};


#endif /* _MUSIC_LIST_H_ */