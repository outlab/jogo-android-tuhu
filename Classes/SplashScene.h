#include "common.h"

class SplashScene : public Layer {

private:
    Size _screenSize;

    std::thread* _loadThread;

public:
    SplashScene();
    ~SplashScene();

public:
	virtual bool init();

	static Scene* scene();

    void viewMenuScene(float dt);

    void loadResources();


	CREATE_FUNC(SplashScene);
};
