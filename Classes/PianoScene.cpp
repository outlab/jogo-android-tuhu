/*
 * PianoScene.cpp
 *
 *  Created on: 2015. 12. 10.
 *      Author: Jimmy
 */
#include <string>
#include "PianoScene.h"
#include "MenuScene.h"
#include "MusicInfo.cpp"
#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include "SonarFrameworks.h"

#define PLAY_DELAY_TIME 2.0f
 USING_NS_CC;
using namespace std;

Vector<Node*> allPlayedKeys;
int whichMusic;
vector<float> tappedTime;

bool verifyFirstGameNotes;
bool verifybiggerThan;
bool clearOneTime;
bool finishedGame;
bool btSeeReplayTapped;
bool replay;
bool startBlack;
bool btReplayTapped;
int playMusic;
int numeroDeEstrelasPontosGame1;
bool x;
bool stopRecordAndShowReplayVideo;
bool finishedPlayback;
bool startedPlayback;

string music2KeySequence[62] =
{
    "mi","mi","re","do","mi","mi","sol","sol","fa","mi","fa","fa","fa","mi","re","fa","fa","la","la","sol","fa",
    "sol","do_2","do_2","si","la","si","mi","la","la","sol","fa","sol","fa","fa","mi","re","mi","mi","re","do",
    "re","re_2","do_2","si","la","la","re_2","re_2","re_2","do_2","si","la","la","re_2","re_2","re_2","re_2","re_2","do_2","si","la"
};

string music1KeySequence[48] =
{
    "re_2","re_2","re_2","do_2","si","sol","fa_s","mi","mi","mi","re","mi","do_2","do_2","do_2","si","la","fa_s",
    "mi","re","re","re","do","re","re","sol","si","re_2","mi_2","re_2","do_2","la","fa_s","mi","fa_s","mi",
    "re","sol","la","si","do_2","si","la","fa_s","sol","la","si","sol"
};

string music0KeySequence[39] = //CARNEIRINHO
{
    "si","do_2","re_2","si","mi_2","re_2","do_2","si","la","si","do_2","la","si","do_2","la","re_2","do_2","si",
    "la","sol","la","si","sol","la","si","sol","do_2","si","la","sol","fa_s","sol","la","sol","la","fa_s","si","la","sol"
};
PianoScene::PianoScene() {
	// TODO Auto-generated constructor stub




    _jogo = nullptr;
    _margin = 20.0f;
    _backBoy = nullptr;
	_boyAction = nullptr;
	_muteBgMusic = nullptr;
	_iconBgMusic1 = nullptr;
	_iconBgMusic2 = nullptr;
	_iconBgMusic3 = nullptr;
	_playBgMusic = true;
	_piano = nullptr;
    x = true;
	_starGray = nullptr;
	_starYellow = nullptr;
    boolStartCounter = false;
    counterValue = 0;
	_currentMusicCount = 0;
	_currentMusicIndex = 0;
	_currentMusic = nullptr;
	_currentMusicType = 0;
    verifybiggerThan = false;
	_okayBoy = nullptr;
	_okayBoyAction = nullptr;

	_stopPlayStar = false;
	_showOkayBoy = false;

    indexReplayList = 0;
    numeroDeEstrelasPontosGame1 = 0;
    stopRecordAndShowReplayVideo = false;
    finishedPlayback = false;
    startedPlayback = false;
}

PianoScene::~PianoScene() {
	// TODO Auto-generated destructor stub
}

Scene* PianoScene::scene() {
    replay = false;
    startBlack = false;
    btReplayTapped = false;
    allPlayedKeys.clear();
	Scene *scene = Scene::create();

	PianoScene *layer = PianoScene::create();
	scene->addChild(layer);

	return scene;
}

Scene* PianoScene::scene(bool replayTapped, int wMusic) {
    replay = false;
    startBlack = true;
    btReplayTapped = replayTapped;
    allPlayedKeys.clear();
    Scene *scene = Scene::create();

    PianoScene *layer = PianoScene::create();
    scene->addChild(layer);

    return scene;
}

Scene* PianoScene::scene(bool replayGame, int wMusic, vector<float> tappedListTime, Vector<Node*> playedKeys) {
    //replay = false;
    startBlack = replayGame;
    playMusic = wMusic;
    allPlayedKeys = playedKeys;
    tappedTime = tappedListTime;
    Scene *scene = Scene::create();
    stopRecordAndShowReplayVideo = false;
    finishedPlayback = false;
    startedPlayback = false;
    PianoScene *layer = PianoScene::create();
    scene->addChild(layer);
    return scene;
}

Scene* PianoScene::scene(bool replayPlayback, bool replayGame, int wMusic, vector<float> tappedListTime, Vector<Node*> playedKeys) {
    replay = true;
    btSeeReplayTapped = true;
    startBlack = replayGame;
    playMusic = wMusic;
    allPlayedKeys = playedKeys;
    tappedTime = tappedListTime;
//    log("NUMERO DE NOTAS %zd",allPlayedKeys.size());
//    for(int i=0;i<tappedTime.size();i++){
//        log("%f",tappedTime[i]);
//    }
    Scene *scene = Scene::create();
    PianoScene *layer = PianoScene::create();
    scene->addChild(layer);
    return scene;
}

bool PianoScene::init() {
	if(!Layer::init())
		return false;

    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    auto glview = Director::getInstance()->getOpenGLView();
    frameSize = glview->getFrameSize();

    SimpleAudioEngine::getInstance()->preloadEffect("Jogo01/tremzinho.mp3");
    SimpleAudioEngine::getInstance()->preloadEffect("Jogo01/tororo.mp3");
    SimpleAudioEngine::getInstance()->preloadEffect("Jogo01/carneirinho.mp3");


	EventDispatcher* dispatcher = Director::getInstance()->getEventDispatcher();
	auto positionListener = EventListenerTouchOneByOne::create();
	positionListener->setSwallowTouches(true);
	positionListener->onTouchBegan = CC_CALLBACK_2(PianoScene::onTouchBegan, this);
	positionListener->onTouchMoved = CC_CALLBACK_2(PianoScene::onTouchMoved, this);
	positionListener->onTouchEnded = CC_CALLBACK_2(PianoScene::onTouchEnded, this);
	dispatcher->addEventListenerWithSceneGraphPriority(positionListener, this);

	EventListenerTouchAllAtOnce* multitouchListener = EventListenerTouchAllAtOnce::create();
	multitouchListener->onTouchesBegan = CC_CALLBACK_2(PianoScene::onTouchesBegan, this);
	multitouchListener->onTouchesMoved = CC_CALLBACK_2(PianoScene::onTouchesMoved, this);
	multitouchListener->onTouchesEnded = CC_CALLBACK_2(PianoScene::onTouchesEnded, this);
	dispatcher->addEventListenerWithSceneGraphPriority(multitouchListener, this);

	_screenSize = Director::getInstance()->getWinSize();

    blackBG2 = Sprite::create("Jogo01/carregando.png");
    blackBG2->setAnchorPoint(Vec2(0,1));
    blackBG2->setPosition(Vec2(0, origin.y + _screenSize.height));
    blackBG2->setScaleX(_screenSize.width/blackBG2->getContentSize().width);
    blackBG2->setScaleY(_screenSize.height/blackBG2->getContentSize().height);
    if(!startBlack){
        blackBG2->setOpacity(0);
    }
    this->addChild(blackBG2,4);

    blackBGPlayBack = Sprite::create("Jogo01/carregando.png");
    blackBGPlayBack->setAnchorPoint(Vec2(0,1));
    blackBGPlayBack->setPosition(Vec2(0, origin.y + _screenSize.height));
    blackBGPlayBack->setScaleX(_screenSize.width/blackBGPlayBack->getContentSize().width);
    blackBGPlayBack->setScaleY(_screenSize.height/blackBGPlayBack->getContentSize().height);
    this->addChild(blackBGPlayBack, 9);
    blackBGPlayBack->setVisible(false);

    auto background = CSLoader::createNode("Jogo01/Back.csb");
	background->setPosition(Vec2(_screenSize.width * 0.5f, _screenSize.height * 0.5f));
	//Tuhu::addChildForIOS(this, background, 0);
    this->addChild(background,0);

	auto circle = CSLoader::createNode("Jogo01/Jogo01.csb");
	_jogo = circle->getChildByTag(60);
	_jogo->setPosition(Vec2(_screenSize.width * 0.5f, -0.6f * _jogo->getContentSize().height));
    _jogo->setScale(0.8f);
	//Tuhu::addChildForIOS(this, _jogo, 1);
    this->addChild(_jogo,1);

	_boyAction = CSLoader::createTimeline("Jogo01/Jogo01.csb");
	_boyAction->retain();
	_jogo->getChildByTag(26)->runAction(_boyAction);

	_okayBoy = _jogo->getChildByTag(83);
	_okayBoy->setVisible(false);
	_okayBoyAction = CSLoader::createTimeline("Jogo01/Jogo01.csb");
	_okayBoy->runAction(_okayBoyAction);

    tuhuLegal = Sprite::create("SimonGame/legal.png");
    tuhuLegal->setPosition(Vec2(origin.x + visibleSize.width/2, origin.y + _screenSize.height/1.4));
    tuhuLegal->setScale(0);
    this->addChild(tuhuLegal,5);

    auto positionKey = Vec2(_screenSize.width/2,_screenSize.height*.7);
    auto scaleKey = _screenSize.width*0.0006;

    C = Sprite::create("SimonGame/keyImages/Do.png");
    CSus = Sprite::create("SimonGame/keyImages/Do_s.png");
    D = Sprite::create("SimonGame/keyImages/Re.png");
    DSus = Sprite::create("SimonGame/keyImages/Re_s.png");
    E = Sprite::create("SimonGame/keyImages/Mi.png");
    F = Sprite::create("SimonGame/keyImages/Fa.png");
    FSus = Sprite::create("SimonGame/keyImages/Fa_s.png");
    G = Sprite::create("SimonGame/keyImages/Sol.png");
    GSus = Sprite::create("SimonGame/keyImages/Sol_s.png");
    A = Sprite::create("SimonGame/keyImages/La.png");
    ASus = Sprite::create("SimonGame/keyImages/La_s.png");
    B = Sprite::create("SimonGame/keyImages/Si.png");

    C->setPosition(positionKey);
    D->setPosition(positionKey);
    E->setPosition(positionKey);
    F->setPosition(positionKey);
    G->setPosition(positionKey);
    A->setPosition(positionKey);
    B->setPosition(positionKey);
    CSus->setPosition(positionKey);
    DSus->setPosition(positionKey);
    FSus->setPosition(positionKey);
    GSus->setPosition(positionKey);
    ASus->setPosition(positionKey);

    C->setScale(scaleKey);
    E->setScale(scaleKey);
    F->setScale(scaleKey);
    G->setScale(scaleKey);
    A->setScale(scaleKey);
    B->setScale(scaleKey);
    D->setScale(scaleKey);
    CSus->setScale(scaleKey);
    DSus->setScale(scaleKey);
    FSus->setScale(scaleKey);
    GSus->setScale(scaleKey);
    ASus->setScale(scaleKey);

    C->setOpacity(0);
    D->setOpacity(0);
    E->setOpacity(0);
    F->setOpacity(0);
    G->setOpacity(0);
    A->setOpacity(0);
    B->setOpacity(0);
    CSus->setOpacity(0);
    DSus->setOpacity(0);
    FSus->setOpacity(0);
    ASus->setOpacity(0);
    GSus->setOpacity(0);

    this->addChild(C, 5);
    this->addChild(D, 5);
    this->addChild(E, 5);
    this->addChild(F, 5);
    this->addChild(G, 5);
    this->addChild(A, 5);
    this->addChild(B, 5);
    this->addChild(CSus, 5);
    this->addChild(DSus, 5);
    this->addChild(ASus, 5);
    this->addChild(FSus, 5);
    this->addChild(GSus, 5);


    verifyFirstGameNotes = true;
        if(btSeeReplayTapped){
            clearOneTime = true;
            verifyFirstGameNotes = false;
        }

    if (!replay) {
        auto replaySong = CallFunc::create([this]()
                                           {
                                               if(btReplayTapped){
                                                   auto fadeOut = FadeOut::create(0.2);
                                                   blackBG2->runAction(Sequence::create(fadeOut/*,DelayTime::create(0.1f)*/,nullptr));
                                                   PianoScene::touchMenu(playMusic);
                                               }
                                           });
        auto moveJogo = Sequence::create(

                                         EaseIn::create(RotateTo::create(0.5f, -30.0f), 2),
                                         EaseIn::create(RotateTo::create(0.4f, 20.0f), 2),
                                         RotateTo::create(0.3f, -10.0f),
                                         RotateTo::create(0.2f, 5.0f),
                                         RotateTo::create(0.1f, -2.5f),
                                         RotateTo::create(0.1f, 0.0f),
                                         replaySong,
                                         nullptr
                                         );
        _jogo->runAction(moveJogo);
    } else {
        auto replaySong = CallFunc::create([this]()
                                                        {
                                                            auto fadeOut = FadeOut::create(0.1);
                                                            blackBG2->runAction(Sequence::create(fadeOut/*,DelayTime::create(0.1f)*/,nullptr));
                                                            PianoScene::touchMenu(playMusic);
                                                        });
        auto moveJogo = Sequence::create(

                                         EaseIn::create(RotateTo::create(0.5f, -30.0f), 0.2f),
                                         EaseIn::create(RotateTo::create(0.4f, 20.0f), 0.2f),
                                         RotateTo::create(0.03f, -10.0f),
                                         RotateTo::create(0.02f, 5.0f),
                                         RotateTo::create(0.01f, -2.5f),
                                         RotateTo::create(0.01f, 0.0f),
                                         replaySong,
                                         nullptr
                                         );
        _jogo->runAction(moveJogo);
    }


    WoodenPlate = Sprite::create("SimonGame/WoodenPlate.png");
    WoodenPlate->setAnchorPoint(Vec2(0.5,1.0));
    WoodenPlate->setPosition(Vec2(origin.x + (.7*_screenSize.width)/3, origin.y + _screenSize.height + WoodenPlate->getContentSize().height));
    WoodenPlate->setScale(_screenSize.width*0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        WoodenPlate->setScaleY(0.4);
    this->addChild(WoodenPlate,4);

    continuePraticando = Sprite::create("SimonGame/ContinuePraticando.png");
    continuePraticando->setAnchorPoint(Vec2(0.5,1.0));
    continuePraticando->setPosition(Vec2(origin.x + (.7*_screenSize.width)/3, origin.y + _screenSize.height + WoodenPlate->getContentSize().height));
    continuePraticando->setScale(_screenSize.width*0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        continuePraticando->setScaleY(0.4);
    this->addChild(continuePraticando,4);

    muitoBemP = Sprite::create("SimonGame/muitoBom.png");
    muitoBemP->setAnchorPoint(Vec2(0.5,1.0));
    muitoBemP->setPosition(Vec2(origin.x + (.7*_screenSize.width)/3, origin.y + _screenSize.height + muitoBemP->getContentSize().height));
    muitoBemP->setScale(_screenSize.width*0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        muitoBemP->setScaleY(0.4);
    this->addChild(muitoBemP,4);


    btReplayVideo = MenuItemImage::create("placa_gameplay.png","placa_gameplay.png",CC_CALLBACK_1(PianoScene::startVideo,this));
    btReplayVideo->setAnchorPoint(Vec2(0.5,1.0));
    //btReplayVideo->setPosition(Vec2(_screenSize.width/2, _screenSize.height/2));
    btReplayVideo->setPosition(Vec2(origin.x + (2.3*_screenSize.width)/3, origin.y + _screenSize.height + btReplayVideo->getContentSize().height));
    btReplayVideo->setScale(_screenSize.width*0.0004);
    if(frameSize.width/frameSize.height < 1.5)
        btReplayVideo->setScaleY(0.4);
    auto menuReplayVideo = Menu::create(btReplayVideo,NULL);
    menuReplayVideo->setPosition(Point::ZERO);
    this->addChild(menuReplayVideo,5);

//    replayVideoButton = Sprite::create("placa_gameplay.png");
//    //replayVideoButton->setAnchorPoint(Vec2(0.5,1.0));
//    replayVideoButton->setPosition(Vec2(_screenSize.width/2,_screenSize.height/2));
//    replayVideoButton->setScale(_screenSize.width*0.0004);
//    if(frameSize.width/frameSize.height < 1.5)
//        replayVideoButton->setScaleY(0.4);
//    this->addChild(replayVideoButton,4);

    congratStar1 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar1->setPosition(Vec2(WoodenPlate->getContentSize().width*0.277, WoodenPlate->getContentSize().height*0.22));
    congratStar1->setOpacity(0);
    congratStar1->setScale(_screenSize.width*0.00075);
    WoodenPlate->addChild(congratStar1,2);

    congratStar2 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar2->setPosition(Vec2(WoodenPlate->getContentSize().width/1.935, WoodenPlate->getContentSize().height*0.265));
    congratStar2->setOpacity(0);
    congratStar2->setScale(_screenSize.width*0.00085);
    WoodenPlate->addChild(congratStar2,2);

    congratStar3 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar3->setPosition(Vec2(WoodenPlate->getContentSize().width - WoodenPlate->getContentSize().width*0.2284, WoodenPlate->getContentSize().height*0.22));
    congratStar3->setOpacity(0);
    congratStar3->setScale(_screenSize.width*0.00075);
    WoodenPlate->addChild(congratStar3,2);

    congratStar1_2 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar1_2->setPosition(Vec2(continuePraticando->getContentSize().width*0.277, continuePraticando->getContentSize().height*0.22));
    congratStar1_2->setOpacity(0);
    congratStar1_2->setScale(_screenSize.width*0.00075);
    continuePraticando->addChild(congratStar1_2,2);

    congratStar2_2 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar2_2->setPosition(Vec2(continuePraticando->getContentSize().width/1.935, continuePraticando->getContentSize().height*0.265));
    congratStar2_2->setOpacity(0);
    congratStar2_2->setScale(_screenSize.width*0.00085);
    continuePraticando->addChild(congratStar2_2,2);

    congratStar3_2 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar3_2->setPosition(Vec2(continuePraticando->getContentSize().width - continuePraticando->getContentSize().width*0.2284, WoodenPlate->getContentSize().height*0.22));
    congratStar3_2->setOpacity(0);
    congratStar3_2->setScale(_screenSize.width*0.00075);
    continuePraticando->addChild(congratStar3_2,2);

    congratStar1_3 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar1_3->setPosition(Vec2(muitoBemP->getContentSize().width*0.277, muitoBemP->getContentSize().height*0.22));
    congratStar1_3->setOpacity(0);
    congratStar1_3->setScale(_screenSize.width*0.00075);
    muitoBemP->addChild(congratStar1_3,2);

    congratStar2_3 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar2_3->setPosition(Vec2(muitoBemP->getContentSize().width/1.935, muitoBemP->getContentSize().height*0.265));
    congratStar2_3->setOpacity(0);
    congratStar2_3->setScale(_screenSize.width*0.00085);
    muitoBemP->addChild(congratStar2_3,2);

    congratStar3_3 = Sprite::create("SimonGame/congratsGoldStar.png");
    congratStar3_3->setPosition(Vec2(muitoBemP->getContentSize().width - muitoBemP->getContentSize().width*0.2284, muitoBemP->getContentSize().height*0.22));
    congratStar3_3->setOpacity(0);
    congratStar3_3->setScale(_screenSize.width*0.00075);
    muitoBemP->addChild(congratStar3_3,2);



    replayButton = MenuItemImage::create("SimonGame/botaoReplay.png","SimonGame/botaoReplay.png",CC_CALLBACK_1(PianoScene::loadThisScene,this));
    replayButton->setPosition(Vec2(WoodenPlate->getContentSize().width/2, WoodenPlate->getContentSize().height*0.05));
    replayButton->setScale(_screenSize.width*0.00075);
    auto menureplayButton = Menu::create(replayButton,NULL);
    menureplayButton->setPosition(Point::ZERO);
    WoodenPlate->addChild(menureplayButton,2);

    backButton2 = MenuItemImage::create("SubMenuSong/botaomenu.png","SubMenuSong/botaomenu.png",CC_CALLBACK_1(PianoScene::loadMenuScene,this));
    backButton2->setPosition(Vec2(WoodenPlate->getContentSize().width*0.15, WoodenPlate->getContentSize().height*0.05));
    backButton2->setScale(_screenSize.width*0.00075);
    auto menuBack = Menu::create(backButton2,NULL);
    menuBack->setPosition(Point::ZERO);
    WoodenPlate->addChild(menuBack,2);

    nextTrack2 = MenuItemImage::create("SimonGame/botaoTelamusicas.png","SimonGame/botaoTelamusicas.png",CC_CALLBACK_1(PianoScene::backMusicScene,this));
    nextTrack2->setPosition(Vec2(WoodenPlate->getContentSize().width - WoodenPlate->getContentSize().width*0.15, WoodenPlate->getContentSize().height*0.05));
    nextTrack2->setScale(_screenSize.width*0.00075);
    auto menuTrack = Menu::create(nextTrack2,NULL);
    menuTrack->setPosition(Point::ZERO);
    WoodenPlate->addChild(menuTrack,2);

    replayButton2 = MenuItemImage::create("SimonGame/botaoReplay.png","SimonGame/botaoReplay.png",CC_CALLBACK_1(PianoScene::loadThisScene,this));
    replayButton2->setPosition(Vec2(continuePraticando->getContentSize().width/2, continuePraticando->getContentSize().height*0.05));
    replayButton2->setScale(_screenSize.width*0.00075);
    auto menureplayButton2 = Menu::create(replayButton2,NULL);
    menureplayButton2->setPosition(Point::ZERO);
    continuePraticando->addChild(menureplayButton2,2);

    backButton3 = MenuItemImage::create("SubMenuSong/botaomenu.png","SubMenuSong/botaomenu.png",CC_CALLBACK_1(PianoScene::loadMenuScene,this));
    backButton3->setPosition(Vec2(continuePraticando->getContentSize().width*0.15, continuePraticando->getContentSize().height*0.05));
    backButton3->setScale(_screenSize.width*0.00075);
    auto menuBack2 = Menu::create(backButton3,NULL);
    menuBack2->setPosition(Point::ZERO);
    continuePraticando->addChild(menuBack2,2);

    nextTrack3 = MenuItemImage::create("SimonGame/botaoTelamusicas.png","SimonGame/botaoTelamusicas.png",CC_CALLBACK_1(PianoScene::backMusicScene,this));
    nextTrack3->setPosition(Vec2(continuePraticando->getContentSize().width - continuePraticando->getContentSize().width*0.15, WoodenPlate->getContentSize().height*0.05));
    nextTrack3->setScale(_screenSize.width*0.00075);
    auto menuTrack2 = Menu::create(nextTrack3,NULL);
    menuTrack2->setPosition(Point::ZERO);
    continuePraticando->addChild(menuTrack2,2);

    replayButton3 = MenuItemImage::create("SimonGame/botaoReplay.png","SimonGame/botaoReplay.png",CC_CALLBACK_1(PianoScene::loadThisScene,this));
    replayButton3->setPosition(Vec2(muitoBemP->getContentSize().width/2, muitoBemP->getContentSize().height*0.05));
    replayButton3->setScale(_screenSize.width*0.00075);
    auto menureplayButton3 = Menu::create(replayButton3,NULL);
    menureplayButton3->setPosition(Point::ZERO);
    muitoBemP->addChild(menureplayButton3,2);

    backButton4 = MenuItemImage::create("SubMenuSong/botaomenu.png","SubMenuSong/botaomenu.png",CC_CALLBACK_1(PianoScene::loadMenuScene,this));
    backButton4->setPosition(Vec2(muitoBemP->getContentSize().width*0.15, muitoBemP->getContentSize().height*0.05));
    backButton4->setScale(_screenSize.width*0.00075);
    auto menuBack3 = Menu::create(backButton4,NULL);
    menuBack3->setPosition(Point::ZERO);
    muitoBemP->addChild(menuBack3,2);

    nextTrack4 = MenuItemImage::create("SimonGame/botaoTelamusicas.png","SimonGame/botaoTelamusicas.png",CC_CALLBACK_1(PianoScene::backMusicScene,this));
    nextTrack4->setPosition(Vec2(muitoBemP->getContentSize().width - muitoBemP->getContentSize().width*0.15, WoodenPlate->getContentSize().height*0.05));
    nextTrack4->setScale(_screenSize.width*0.00075);
    auto menuTrack3 = Menu::create(nextTrack4,NULL);
    menuTrack3->setPosition(Point::ZERO);
    muitoBemP->addChild(menuTrack3,2);

    float h = (3*visibleSize.height)/2;
    float sh = (_screenSize.height*0.00088);

    blackBG = Sprite::create("SubMenuSong/blackBG.png");
    blackBG->setAnchorPoint(Vec2(0,1));
    blackBG->setPosition(Vec2(0, origin.y + h));
    blackBG->setScaleX(_screenSize.width/blackBG->getContentSize().width);
    blackBG->setScaleY(sh);
    blackBG->setOpacity(0);
    this->addChild(blackBG,4);



    dourado = Sprite::create("SimonGame/dourado.png");
    dourado->setAnchorPoint(Vec2(0,0));
    dourado->setPosition(Vec2(0,0));
    dourado->setOpacity(0);
    dourado->setScale(_screenSize.width/blackBG->getContentSize().width);
    this->addChild(dourado,7);


	((Button*)_jogo->getChildByTag(40))->addTouchEventListener(CC_CALLBACK_2(PianoScene::onTouchButtons, this));
	((Button*)_jogo->getChildByTag(42))->addTouchEventListener(CC_CALLBACK_2(PianoScene::onTouchButtons, this));
	((Button*)_jogo->getChildByTag(45))->addTouchEventListener(CC_CALLBACK_2(PianoScene::onTouchButtons, this));

	auto backTrain = CSLoader::createNode("Penta/BackTrain.csb");
    btnBackTrain = (Button*)backTrain->getChildByTag(36);
	btnBackTrain->addTouchEventListener(CC_CALLBACK_2(PianoScene::onTouchButtons, this));
	btnBackTrain->setPosition(Vec2(_margin + btnBackTrain->getContentSize().width * 0.5f, _screenSize.height - btnBackTrain->getContentSize().height * 0.5f - _margin));
	//Tuhu::addChildForIOS(this, btnBackTrain, 8); //volta para menu
	this->addChild(btnBackTrain, 8);

	auto backBoy = CSLoader::createNode("Jogo01/BackBoy.csb");
	_backBoy = (Button*)backBoy->getChildByTag(80);
	_backBoy->setPosition(Vec2(_screenSize.width - 2.0f * _backBoy->getContentSize().width, _screenSize.height - _backBoy->getContentSize().height * 0.5f - _margin));
	_backBoy->addTouchEventListener(CC_CALLBACK_2(PianoScene::onTouchButtons, this));
	_backBoy->setVisible(false);
    this->addChild(_backBoy,1);
    //Tuhu::addChildForIOS(this, _backBoy, 1);

	auto score = CSLoader::createNode("Jogo01/Score.csb");
	_muteBgMusic = score->getChildByTag(128);
	_iconBgMusic1 = score->getChildByTag(129);
	_iconBgMusic2 = score->getChildByTag(130);
	_iconBgMusic3 = score->getChildByTag(131);

	_posBgMusic = Vec2(_screenSize.width - _muteBgMusic->getContentSize().width * 0.5f - _margin, _screenSize.height - _muteBgMusic->getContentSize().height * 0.5f - _margin);
	_muteBgMusic->setPosition(_posBgMusic);
	_muteBgMusic->setVisible(false);
	//Tuhu::addChildForIOS(this, _muteBgMusic, 2);
    this->addChild(_muteBgMusic,2);

	_iconBgMusic1->setPosition(_posBgMusic);
	_iconBgMusic2->setPosition(_posBgMusic);
	_iconBgMusic3->setPosition(Vec2(_posBgMusic.x, _posBgMusic.y - _margin * 2));

	_iconBgMusic1->setVisible(false);
	_iconBgMusic2->setVisible(false);
	_iconBgMusic3->setVisible(false);

	//Tuhu::addChildForIOS(this, _iconBgMusic1, 2);
    this->addChild(_iconBgMusic1,2);
    this->addChild(_iconBgMusic2,2);
    this->addChild(_iconBgMusic3,2);
	//Tuhu::addChildForIOS(this, _iconBgMusic2, 2);
	//Tuhu::addChildForIOS(this, _iconBgMusic3, 2);

	auto piano = CSLoader::createNode("Jogo01/Piano.csb");
	_piano = piano->getChildByTag(29);
	_piano->setPosition(Vec2(_screenSize.width * 0.5f, -0.5f * _piano->getContentSize().height));
    _piano->setScaleX(1.2f);
	//Tuhu::addChildForIOS(this, _piano, 3);
    this->addChild(_piano,3);

	//char szName[32];
	_pianoKeys.clear();
	for(int i = 134; i <= 143; i++) {
		auto pianoKey = _piano->getChildByTag(i);

		pianoKey->setVisible(false);

		_pianoKeys.pushBack(pianoKey);
	}
	_pianoHalfKeys.clear();
	for(int i = 145; i <= 151; i++) {
		auto halfKey = _piano->getChildByTag(i);
		halfKey->setVisible(false);

		_pianoHalfKeys.pushBack(halfKey);
	}

	_rcBgMusic = Rect(_screenSize.width - 100.0f, _screenSize.height - 100.0f, 100.0f, 100.0f);

	_playBgMusic = false;

	_musicInfo.init();



    this->schedule(schedule_selector(PianoScene::startCounter),0.1f);
    //this->scheduleOnce(schedule_selector(PianoScene::sayLegal), 3.0f);
	return true;
}

void PianoScene::onTouchButtons(Ref* pSender, Widget::TouchEventType type) {
	Button* btn = (Button*)pSender;
	int iTag = btn->getTag();
    //log("%s",music0KeySequence[0].c_str());
	if(type == Widget::TouchEventType::ENDED) {
		switch(iTag) {
		case 36:
			backMenu();
			break;
		case 42:
                playMusic = 2;
			touchMenu(2);
			break;
		case 40:
                playMusic = 1;
			touchMenu(1);
			break;
		case 45:
                playMusic = 0;
			touchMenu(0);
			break;
		case 80:
                counterValue = 0;
                timeToNote.clear();
                allPlayedKeys.clear();
                backPianoScene();//backBoy();
		default:
			break;
		}
	}
}

void PianoScene::startCounter(float dt) {

    float delayTime = 0;
    if(replay){// && btSeeReplayTapped) {
        if (playMusic == 0) {
            delayTime = 0;
        }else if (playMusic == 1){
            if(indexReplayList <=35) {
                delayTime = 0;
            }else{
                delayTime = 0;
            }

        }else {
            if (indexReplayList < 22) {
                delayTime = 0;
            }else if (indexReplayList >= 22 && indexReplayList < 52) {
                delayTime = 0;
            }else {
                //delayTime = 0.6;
            }
        }
    }

    if(boolStartCounter){
        if(clearOneTime){
            counterValue = 0;
            clearOneTime = false;
            timeToNote = tappedTime;
        }
        counterValue += 0.1f;
        if(replay) {
            auto i = timeToNote.size();
            if(btSeeReplayTapped){
                i = tappedTime.size();
            }else {
                i = timeToNote.size();
            }
            if(i > 0) {
                if(finishedGame) {
                    this->scheduleOnce(schedule_selector(PianoScene::showOkayBoy), 3.0f);
                    this->scheduleOnce(schedule_selector(PianoScene::animateVictory), 1.0f);
                    btnBackTrain->setScale(0);
                    x = !x;
                    finishedGame = false;
                }
                if(btSeeReplayTapped) {
                    if(counterValue > tappedTime[indexReplayList] + delayTime) {
                        verifybiggerThan = true;
                    }else {
                        verifybiggerThan = false;
                    }
                }else {
                    if(counterValue > timeToNote[indexReplayList] + delayTime) {
                        verifybiggerThan = true;
                    }else {
                        verifybiggerThan = false;
                    }
                }
                if(verifybiggerThan  && i > indexReplayList) {
                    if (btSeeReplayTapped) {
                       // log("btSeeReplayTapped = true");
                       // log("COUNTER VALUE - %f",counterValue);
                        //log("TIME TO NOTE - %f",timeToNote[indexReplayList] + delayTime);
                       // log("i - %lu",i);
                       // log("INDEX REPLAY LIST - %d",indexReplayList);
                       // log("NUMERO DE NOTAS AQUI %zd",allPlayedKeys.size());
                        //SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/AudioMuitoBom.mp3");
                        animateKey(allPlayedKeys.at(indexReplayList));
                        indexReplayList += 1;
                    }else {
                        log("btSeeReplayTapped = false");
                    }
                }else {
                    //log("COUNTER VALUE - %f",counterValue);
                    //log("TIME TO NOTE - %f",tappedTime[indexReplayList] + delayTime);
                    //log("i - %lu",i);
                    //log("INDEX REPLAY LIST - %d",indexReplayList);
                    //SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/Applause.mp3");

                    if(!(i > indexReplayList) && x) {
                        //log("TERMINOU REPLAY");
                        btSeeReplayTapped = false;
                        this->scheduleOnce(schedule_selector(PianoScene::showOkayBoy), 3.0f);
                        this->scheduleOnce(schedule_selector(PianoScene::animateVictory), 1.0f);
                        btnBackTrain->setScale(0);
                        x = !x;
                    }
                }
            }else {
                if(!(i > indexReplayList) && x) {

                       this->scheduleOnce(schedule_selector(PianoScene::showOkayBoy), 3.0f);
                       this->scheduleOnce(schedule_selector(PianoScene::animateVictory), 1.0f);
                       btnBackTrain->setScale(0);

                       x = !x;
                }
            }

        }
    }
}

void PianoScene::animateKey(Node* key) {
    float keyHideTime = 0.15f;
    float keyShowTime = 0.03f;
    char szKeyFile[512];
    //key->setVisible(true);
    memset(szKeyFile, 0, sizeof(char) * 512);
    sprintf(szKeyFile, "%s.ogg", key->getName().c_str());
    SimpleAudioEngine::getInstance()->playEffect(szKeyFile);

    char szName[32];
    memset(szName, 0, sizeof(char) * 32);
    sprintf(szName, "%s_over", key->getName().c_str());
    auto pianoOver = CSLoader::createNode("Jogo01/PianoOver.csb");
    auto keyOver = pianoOver->getChildByName(szName);
    keyOver->setOpacity(0);
    pianoOver->removeChild(keyOver);
    _piano->addChild(keyOver);


    if("do" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);

                                             //C->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {

                                             auto fadeOut = FadeOut::create(0.01);
                                             //C->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("re" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                          {
                                              auto fadeIn = FadeIn::create(0.01);

                                             // D->runAction(Sequence::create(fadeIn,nullptr));
                                          });
        auto hideNote = CallFunc::create([this]()
                                         {

                                             auto fadeOut = FadeOut::create(0.01);
                                             //D->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("mi" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);

                                             //E->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {

                                             auto fadeOut = FadeOut::create(0.01);
                                             //E->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("fa" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);

                                            // F->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {

                                             auto fadeOut = FadeOut::create(0.01);
                                             //F->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("sol" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);

                                             //G->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {

                                             auto fadeOut = FadeOut::create(0.01);
                                             //G->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("la" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);

                                            // A->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {

                                             auto fadeOut = FadeOut::create(0.01);
                                            // A->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("si" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);

                                            // B->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {

                                             auto fadeOut = FadeOut::create(0.01);
                                            // B->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("do_2" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);

                                            // C->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {

                                             auto fadeOut = FadeOut::create(0.01);
                                             //C->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("re_2" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);
                                             //auto fadeOut = FadeOut::create(0.015);
                                            // D->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {
                                             //auto fadeIn = FadeIn::create(0.015);
                                             auto fadeOut = FadeOut::create(0.01);
                                             //D->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("mi_2" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);

                                            // E->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {
                                             //auto fadeIn = FadeIn::create(0.015);
                                             auto fadeOut = FadeOut::create(0.01);
                                            // E->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("do_s" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);
                                             //auto fadeOut = FadeOut::create(0.015);
                                             //CSus->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {
                                             //auto fadeIn = FadeIn::create(0.015);
                                             auto fadeOut = FadeOut::create(0.01);
                                            // CSus->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("re_s" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);
                                             //auto fadeOut = FadeOut::create(0.015);
                                            // DSus->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {
                                             //auto fadeIn = FadeIn::create(0.015);
                                             auto fadeOut = FadeOut::create(0.01);
                                            // DSus->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("fa_s" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);
                                             //auto fadeOut = FadeOut::create(0.015);
                                            // FSus->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {
                                             //auto fadeIn = FadeIn::create(0.015);
                                             auto fadeOut = FadeOut::create(0.01);
                                             //FSus->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("sol_s" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);
                                             //auto fadeOut = FadeOut::create(0.015);
                                             //GSus->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {
                                             //auto fadeIn = FadeIn::create(0.015);
                                             auto fadeOut = FadeOut::create(0.01);
                                             //GSus->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }else if("la_s" == key->getName()){
        auto showNote = CallFunc::create([this]()
                                         {
                                             auto fadeIn = FadeIn::create(0.01);
                                             //auto fadeOut = FadeOut::create(0.015);
                                            // ASus->runAction(Sequence::create(fadeIn,nullptr));
                                         });
        auto hideNote = CallFunc::create([this]()
                                         {
                                             //auto fadeIn = FadeIn::create(0.015);
                                             auto fadeOut = FadeOut::create(0.01);
                                             //ASus->runAction(Sequence::create(fadeOut,nullptr));
                                         });
        auto actKeyOver = Sequence::create(showNote,FadeIn::create(keyShowTime), FadeOut::create(keyHideTime),hideNote, nullptr);
        keyOver->runAction(actKeyOver);
    }

     //  auto actKeyOver = Sequence::create(FadeIn::create(keyShowTime), FadeOut::create(keyHideTime), nullptr);
    //keyOver->runAction(actKeyOver);
    _currentMusicIndex += 2;
}

void PianoScene::loadReplayScene(Ref* pSender) {
    SimpleAudioEngine::getInstance()->stopAllEffects();
    auto scene = PianoScene::scene(true, whichMusic, timeToNote,allPlayedKeys);
    Director::getInstance()->replaceScene(scene);

}

void PianoScene::backMenu() {
	SimpleAudioEngine::getInstance()->playEffect("Effect/voltar.ogg");
    SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	auto pScene = MenuScene::scene();
	Director::getInstance()->replaceScene(pScene);
}
void PianoScene::backBoy() {
	_jogo->stopAllActions();
	auto actJogo = createJogoAction();
	auto actPiano = Sequence::create(
			DelayTime::create(0.9f),
			MoveBy::create(0.7f, Vec2(0.0f, -1 * _piano->getContentSize().height * 0.9f)),
			nullptr
	);
	_piano->runAction(actPiano);

	auto act = Spawn::create(
			Sequence::create(
					DelayTime::create(0.9f),
					MoveBy::create(0.7f, Vec2(0.0f, -1 * _piano->getContentSize().height * 0.7f)),
					nullptr
					),
			actJogo,
			nullptr
			);
	_jogo->runAction(act);

	_boyAction->gotoFrameAndPause(0);
	_backBoy->setVisible(false);

	_iconBgMusic1->stopAllActions();
	_iconBgMusic1->setVisible(false);
	_iconBgMusic2->stopAllActions();
	_iconBgMusic2->setVisible(false);
	_iconBgMusic3->stopAllActions();
	_iconBgMusic3->setVisible(false);
	_muteBgMusic->setVisible(false);
	SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	_playBgMusic = false;
	_stopPlayStar = true;
}

void PianoScene::loadMenuScene(Ref* pSender)
{

    auto scene = MenuScene::scene();
    Director::getInstance()->replaceScene(scene);
}

void PianoScene::backMusicScene(Ref* pSender)
{
    auto pScene = PianoScene::scene();
    auto pTran = TransitionFade::create(0.5f, pScene);
    SimpleAudioEngine::getInstance()->stopAllEffects();
    SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    //SimpleAudioEngine::getInstance()->playEffect("Effect/bt.mp3");
    Director::getInstance()->replaceScene(pTran);
}

void PianoScene::backPianoScene()
{
    auto pScene = PianoScene::scene();
    auto pTran = TransitionFade::create(0.5f, pScene);
    SimpleAudioEngine::getInstance()->stopAllEffects();
    SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    //SimpleAudioEngine::getInstance()->playEffect("Effect/bt.mp3");
    Director::getInstance()->replaceScene(pTran);
}

void PianoScene::startVideo(Ref* pSender)
{
    SimpleAudioEngine::getInstance()->stopAllEffects();
    if(timeToNote.size() > 0) {
        auto scene = PianoScene::scene(true,true, whichMusic, timeToNote,allPlayedKeys);
        Director::getInstance()->replaceScene(scene);
    }else {
        auto scene = PianoScene::scene(true,true, whichMusic, tappedTime,allPlayedKeys);
        Director::getInstance()->replaceScene(scene);
    }

    startedPlayback = true;
}

void PianoScene::loadThisScene(Ref* pSender) //reload same song
{
    if(allPlayedKeys.size() > 0){
        _jogo->stopAllActions();

        //SimpleAudioEngine::getInstance()->stopAllEffects();
        auto scene  = PianoScene::scene(true, whichMusic);
        //auto scene = PianoScene::scene(true, whichMusic, timeToNote,allPlayedKeys);
        Director::getInstance()->replaceScene(scene);
    }else {
        auto pScene = PianoScene::scene();
        auto pTran = TransitionFade::create(0.5f, pScene);
        SimpleAudioEngine::getInstance()->stopAllEffects();
        SimpleAudioEngine::getInstance()->stopBackgroundMusic();
        //SimpleAudioEngine::getInstance()->playEffect("Effect/bt.mp3");
        Director::getInstance()->replaceScene(pTran);
    }
}

void PianoScene::touchMenu(int type) {
    counterValue = 0;
    //TuhuCtrl::startRecording();
    boolStartCounter = true;
    whichMusic = type;
    _jogo->stopAllActions();
    if(!replay) {
        SimpleAudioEngine::getInstance()->playEffect("Effect/placas.ogg");
    }
    auto actJogo = createJogoAction();

    auto actPiano = Sequence::create(
                                     DelayTime::create(0.9f),
                                     MoveBy::create(0.7f, Vec2(0.0f, _piano->getContentSize().height * 0.9f)),
                                     nullptr
                                     );
    _piano->runAction(actPiano);

    auto act = Spawn::create(
                             Sequence::create(
                                              DelayTime::create(0.9f),
                                              MoveBy::create(0.7f, Vec2(0.0f, _piano->getContentSize().height * 0.7f)),
                                              nullptr
                                              ),
                             actJogo,
                             nullptr
                             );
    _jogo->runAction(act);

    _boyAction->gotoFrameAndPlay(0, 120, true);

    _backBoy->getChildByTag(81)->setVisible(false);
    _backBoy->getChildByTag(82)->setVisible(false);
    _backBoy->getChildByTag(83)->setVisible(false);
    _backBoy->getChildByTag(81 + type)->setVisible(true);

    _backBoy->setVisible(true);

    startBGMusicAction();
    //SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    _playBgMusic = true;
    float delayTime = 0.0f;


    if(type == 2) {
        _currentMusic = _musicInfo._music1;
        _currentMusicCount = MUSIC1_COUNT * 2;
        _currentMusicIndex = 0;
        delayTime = 0.0f;
    } else if(type == 1) {
        _currentMusic = _musicInfo._music2;
        _currentMusicCount = MUSIC2_COUNT * 2;
        _currentMusicIndex = 0;
        delayTime = 0.5f;
    } else {
        _currentMusic = _musicInfo._music3;
        _currentMusicCount = MUSIC3_COUNT * 2;
        _currentMusicIndex = 0;
        delayTime = 0.0f;
    }
    _currentMusicType = type;


    this->schedule(schedule_selector(PianoScene::startPlayStars), 1.6f + delayTime + _currentMusic[1]);
    this->scheduleOnce(schedule_selector(PianoScene::readyPlayStars), 1.6f);
}
void PianoScene::readyPlayStars(float dt) {
    SimpleAudioEngine::getInstance()->stopAllEffects();
    if(replay) {
//        auto fadeIn = FadeIn::create(0.5);
//        dourado->runAction(Sequence::create(fadeIn,DelayTime::create(0.5f),nullptr));

    }
    if(_currentMusicType == 2) {

        if(replay){

            auto playSong = CallFunc::create([this]()
                                             {
                                                 //SimpleAudioEngine::getInstance()->playBackgroundMusic("Jogo01/tremzinho.mp3");
                                             });

            auto delayPlay = Sequence::create(
                                              DelayTime::create(0.1),
                                              playSong,
                                              nullptr
                                              );
            this->runAction(delayPlay);
        }
            SimpleAudioEngine::getInstance()->playBackgroundMusic("Jogo01/tremzinho.mp3");

	} else if(_currentMusicType == 1) {
        if(replay){
            //startPlayStars(8);
            auto playSong = CallFunc::create([this]()
                                             {
                                                 //SimpleAudioEngine::getInstance()->playBackgroundMusic("Jogo01/tororo.mp3");
                                             });

            auto delayPlay = Sequence::create(
                                              DelayTime::create(0.0),
                                              playSong,
                                              nullptr
                                              );
            this->runAction(delayPlay);
        }
            SimpleAudioEngine::getInstance()->playBackgroundMusic("Jogo01/tororo.mp3");

    } else {
        if(replay){

            auto playSong = CallFunc::create([this]()
                                             {
                                                 //SimpleAudioEngine::getInstance()->playBackgroundMusic("Jogo01/carneirinho.mp3");
                                             });

            auto delayPlay = Sequence::create(
                                              DelayTime::create(0.0),
                                              playSong,
                                              nullptr
                                              );
            this->runAction(delayPlay);
        }
            SimpleAudioEngine::getInstance()->playBackgroundMusic("Jogo01/carneirinho.mp3");

    }
}
void PianoScene::startPlayStars(float dt) {
	this->unschedule(schedule_selector(PianoScene::startPlayStars));
	if(_stopPlayStar) {
		//log("stopped play star!");
		_stopPlayStar = false;
		return;
	}
    if(!btSeeReplayTapped) {
	float fallingTime = 2.19f;
	float keyShowTime = 0.03f;
	float keyHideTime = 0.15f;

	int iKey = (int)_currentMusic[_currentMusicIndex];
	//log("play key = %d", iKey);

	Node* pianoKey = nullptr;
	auto star = CSLoader::createNode("Jogo01/Star.csb");
	int iTag = 0;
	if(iKey > 10) {
		pianoKey = _pianoHalfKeys.at(iKey - 11);
		iTag = 237;
	} else {
		pianoKey = _pianoKeys.at(iKey - 1);
		iTag = 239;
	}
	auto realstar = star->getChildByTag(iTag);

	Vec2 pos = _piano->convertToWorldSpace(pianoKey->getPosition());
	realstar->setPosition(Vec2(pos.x, _screenSize.height));
	//Tuhu::addChildForIOS(this, realstar, 4);
    this->addChild(realstar,4);
	auto actStar = Sequence::create(MoveTo::create(fallingTime - keyShowTime - keyHideTime, Vec2(pos.x, _piano->getContentSize().height * 0.9f)), FadeOut::create(0.16f), nullptr);
	realstar->runAction(actStar);

	char szName[32];
	memset(szName, 0, sizeof(char) * 32);
	sprintf(szName, "%s_over", pianoKey->getName().c_str());
	auto pianoOver = CSLoader::createNode("Jogo01/PianoOver.csb");
	auto keyOver = pianoOver->getChildByName(szName);
	keyOver->setOpacity(0);
    pianoOver->removeChild(keyOver);
	_piano->addChild(keyOver);
    //if(!replay) {
        auto actKeyOver = Sequence::create(DelayTime::create(fallingTime - keyShowTime - keyHideTime), FadeIn::create(keyShowTime), FadeOut::create(keyHideTime), nullptr);
        keyOver->runAction(actKeyOver);
    //}
    }
	_currentMusicIndex += 2;

	if(_currentMusicIndex >= _currentMusicCount) {
		//log("play end!");

        this->scheduleOnce(schedule_selector(PianoScene::replayStars),3.0f);
        if(!btSeeReplayTapped){
            this->scheduleOnce(schedule_selector(PianoScene::playReplay), 2.3f);
        }
		//this->scheduleOnce(schedule_selector(PianoScene::showOkayBoy), 4.0f);
		//this->scheduleOnce(schedule_selector(PianoScene::sayLegal), 3.0f);
		return;
	}
    if(!btSeeReplayTapped) {
        float fTime = (_currentMusic[_currentMusicIndex + 1]) - (_currentMusic[_currentMusicIndex - 1]);

        this->schedule(schedule_selector(PianoScene::startPlayStars), fTime);
    }
}

void PianoScene::replayStars(float dt) {

    int delayTime = 0;
    if(playMusic == 2) {
        _currentMusic = _musicInfo._music1;
        _currentMusicCount = MUSIC1_COUNT * 2;
        _currentMusicIndex = 0;
        delayTime = 0.0f;
    } else if(playMusic == 1) {
        _currentMusic = _musicInfo._music2;
        _currentMusicCount = MUSIC2_COUNT * 2;
        _currentMusicIndex = 0;
        delayTime = 0.5f;
    } else {
        _currentMusic = _musicInfo._music3;
        _currentMusicCount = MUSIC3_COUNT * 2;
        _currentMusicIndex = 0;
        delayTime = 0.0f;
    }
    _currentMusicType = playMusic;


    auto showBlack = CallFunc::create([this]()
                                     {

//                                         auto fadeIn = FadeIn::create(0.5);
//                                            blackBG->runAction(Sequence::create(fadeIn,DelayTime::create(0.5f),nullptr));
//                                         _backBoy->setVisible(false);
                                     });
    //tuhuLegal->runAction(Sequence::create(showBlack,EaseElasticOut::create(ScaleTo::create(0.5f,_screenSize.width*0.0004)),DelayTime::create(2),EaseElasticOut::create(ScaleTo::create(0.1f,0)),nullptr));

//    this->schedule(schedule_selector(PianoScene::startPlayStars), 1.6f + delayTime + _currentMusic[1]);
    this->scheduleOnce(schedule_selector(PianoScene::readyPlayStars), 1.6f);
}

void PianoScene::playReplay(float dt) {
    //log("Starts replay");
    counterValue = 0;
    //boolStartCounter = true;
    replay = true;
    finishedGame = true;
    //this->scheduleOnce(schedule_selector(PianoScene::readyPlayStars), 1.6f);
    //PianoScene::startCounter(0);
}

float PianoScene::calcTime(float time) {
	int iTime = (int)time;
	float ms = (time - (float)iTime) * 100.0f;
	ms = ms * 99 / 24;
	return (float)iTime + ms / 100.0f;
}

void PianoScene::showOkayBoy(float dt) {
	_backBoy->setVisible(false);
//    auto fadeOut = FadeOut::create(0.1);
//    auto fadeOutBLACK = FadeOut::create(0.1);
//    blackBG->runAction(Sequence::create(fadeOutBLACK,nullptr));
//    dourado->runAction(Sequence::create(fadeOut,nullptr));

	_iconBgMusic1->stopAllActions();
	_iconBgMusic1->setVisible(false);
	_iconBgMusic2->stopAllActions();
	_iconBgMusic2->setVisible(false);
	_iconBgMusic3->stopAllActions();
	_iconBgMusic3->setVisible(false);
	_muteBgMusic->setVisible(false);

	_jogo->getChildByTag(26)->setVisible(false);
	_okayBoy->setVisible(true);
	_okayBoyAction->gotoFrameAndPlay(0, 120, false);

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	_playBgMusic = false;

	auto act = MoveBy::create(0.1f, Vec2(0.0f, _piano->getContentSize().height * -0.5f));
	_jogo->runAction(act);

	auto actPiano = Sequence::create(
			MoveBy::create(0.1f, Vec2(0.0f, _piano->getContentSize().height * -0.9f)),
			nullptr
	);
	_piano->runAction(actPiano);

	_showOkayBoy = true;
}

void PianoScene::sayLegal(float dt) {

}

void PianoScene::returnMenu(float dt) {
    if(!replay)
    {
        _jogo->stopAllActions();
        auto actJogo = createJogoAction();
        auto act = Spawn::create(
                                 MoveBy::create(0.1f, Vec2(0.0f, _piano->getContentSize().height * -0.2f)),
                                 actJogo,
                                 nullptr);
        _jogo->runAction(act);

        _jogo->getChildByTag(26)->setVisible(true);
        _okayBoy->setVisible(false);
        _okayBoyAction->gotoFrameAndPause(0);

        _showOkayBoy = false;
    }else {
        auto pScene = PianoScene::scene();
        auto pTran = TransitionFade::create(0.5f, pScene);
        SimpleAudioEngine::getInstance()->stopAllEffects();
        SimpleAudioEngine::getInstance()->stopBackgroundMusic();
        //SimpleAudioEngine::getInstance()->playEffect("Effect/bt.mp3");
        Director::getInstance()->replaceScene(pTran);
    }
}
bool PianoScene::onTouchBegan(Touch* touch, Event* unused_event) {
	if(_rcBgMusic.containsPoint(touch->getLocation())) {
		//log("touch began");
		return true;
	}

	return false;
}
void PianoScene::onTouchMoved(Touch* touch, Event* unused_event) {
}
void PianoScene::onTouchEnded(Touch* touch, Event* unused_event) {
	if(_rcBgMusic.containsPoint(touch->getLocation())) {
		SimpleAudioEngine::getInstance()->playEffect("Effect/bt.ogg");
		playPauseBgMusic();
	}
}
void PianoScene::onTouchesBegan(const std::vector<Touch*>& touches, Event *unused_event) {
    if(!replay) {
        if(_showOkayBoy) return;
        //log("touches began");
        char szKeyFile[512];

        for(Touch* touch : touches) {
            Vec2 pt = _piano->convertToNodeSpace(touch->getLocation());
            Node* touchedKey = nullptr;
            for(Node* halfKey : _pianoHalfKeys) {
                if(halfKey->getBoundingBox().containsPoint(pt)) {
                    touchedKey = halfKey;
                    break;
                }
            }
            if(touchedKey == nullptr) {
                for(Node* key : _pianoKeys) {
                    if(key->getBoundingBox().containsPoint(pt)) {
                        touchedKey = key;
                        //timeToNote.push_back(counterValue);
                        //log("CLICOU AQUI %s", touchedKey->getName().c_str());
                        break;
                    }
                }
            }
            if(touchedKey == nullptr) continue;

            _touchedKeys.pushBack(touchedKey);
            if(!replay) {
                allPlayedKeys.pushBack(touchedKey);
                timeToNote.push_back(counterValue);
            }
            //log("%zd",allPlayedKeys.size());
            touchedKey->setVisible(true);


            memset(szKeyFile, 0, sizeof(char) * 512);
            sprintf(szKeyFile, "%s.ogg", touchedKey->getName().c_str());
            //log("play key sound! filename is %s", szKeyFile);
            SimpleAudioEngine::getInstance()->playEffect(szKeyFile);
        }
    }
}
void PianoScene::onTouchesMoved(const std::vector<Touch*>& touches, Event *unused_event) {
    if(!replay) {
        if(_showOkayBoy) return;

        char szKeyFile[512];
        Vec2 pt;
        for(Touch* touch : touches) {
            pt = _piano->convertToNodeSpace(touch->getLocation());
            Node* touchedKey = nullptr;
            for(Node* halfKey : _pianoHalfKeys) {
                if(halfKey->getBoundingBox().containsPoint(pt)) {
                    touchedKey = halfKey;
                    break;
                }
            }
            if(touchedKey == nullptr) {
                for(Node* key : _pianoKeys) {
                    if(key->getBoundingBox().containsPoint(pt)) {
                        touchedKey = key;
                        break;
                    }
                }
            }
            if(touchedKey == nullptr) continue;
            if(_touchedKeys.contains(touchedKey)) continue;

            _touchedKeys.pushBack(touchedKey);
            touchedKey->setVisible(true);


            if(!replay) {
                allPlayedKeys.pushBack(touchedKey);
                timeToNote.push_back(counterValue);
            }


            memset(szKeyFile, 0, sizeof(char) * 512);
            sprintf(szKeyFile, "%s.ogg", touchedKey->getName().c_str());
            //log("play key sound! filename is %s", szKeyFile);
            SimpleAudioEngine::getInstance()->playEffect(szKeyFile);
        }

        bool bExist = false;
        for(Node* key : _touchedKeys) {
            bExist = false;
            for(Touch* touch : touches) {
                pt = _piano->convertToNodeSpace(touch->getLocation());
                if(key->getBoundingBox().containsPoint(pt)) {
                    bExist = true;
                    break;
                }
            }
            if(!bExist) {
                key->setVisible(false);
                _touchedKeys.eraseObject(key);
            }
        }
    }
}

void PianoScene::animateVictory(float dt)
{
        {
            int pointVerifier = 0;
            int nEstrelas = 0;
            if(playMusic == 1) {
                if(allPlayedKeys.size() >= 48 )
                {
                    for(int i = 0; i < 48 ; i++) {
                        if(allPlayedKeys.at(i)->getName() == music1KeySequence[i]){
                            pointVerifier += 1;
                            //log("%s , %s , %d" ,allPlayedKeys.at(i)->getName().c_str(), music1KeySequence[i].c_str(), pointVerifier);
                        }
                    }

                    if(48 == pointVerifier) {
                        nEstrelas = 3;
                    }else if(24 <= pointVerifier && pointVerifier < 48 ){
                        nEstrelas = 2;
                    }else if(24 > pointVerifier){
                        nEstrelas = 1;
                    }
                    numeroDeEstrelasPontosGame1 = nEstrelas;
                }
                else
                {
                    for(int i = 0; i < allPlayedKeys.size() ; i++) {
                        if(allPlayedKeys.at(i)->getName() == music1KeySequence[i]){
                            pointVerifier += 1;
                            //log("%s , %s , %d" ,allPlayedKeys.at(i)->getName().c_str(), music1KeySequence[i].c_str(), pointVerifier);
                        }
                    }
                    if(48 == pointVerifier) {
                        //log("%d , %d",48, pointVerifier);
                        nEstrelas = 3;
                    }else if(24 <= pointVerifier && pointVerifier < 48 ){
                        //log("%d , %d",48, pointVerifier);
                        nEstrelas = 2;
                    }else if(24 > pointVerifier){
                        //log("%d , %d",48, pointVerifier);
                        nEstrelas = 1;
                    }
                    numeroDeEstrelasPontosGame1 = nEstrelas;
                }
            }else if (playMusic == 2) {
                if(allPlayedKeys.size() >= 62 )
                {
                    for(int i = 0; i < 62 ; i++) {
                        if(allPlayedKeys.at(i)->getName() == music2KeySequence[i]){
                            pointVerifier += 1;
                        }
                    }

                    if(62 == pointVerifier) {
                        nEstrelas = 3;
                    }else if(31 <= pointVerifier && pointVerifier < 62 ){
                        nEstrelas = 2;
                    }else if(31 > pointVerifier){
                        nEstrelas = 1;
                    }
                    numeroDeEstrelasPontosGame1 = nEstrelas;
                }
                else
                {
                    for(int i = 0; i < allPlayedKeys.size() ; i++) {
                        if(allPlayedKeys.at(i)->getName() == music2KeySequence[i]){
                            pointVerifier += 1;
                        }
                    }
                    if(62 == pointVerifier) {
                        nEstrelas = 3;
                    }else if(31 <= pointVerifier && pointVerifier < 62 ){
                        nEstrelas = 2;
                    }else if(31 > pointVerifier){
                        nEstrelas = 1;
                    }
                    numeroDeEstrelasPontosGame1 = nEstrelas;
                }
            }else {
                if(allPlayedKeys.size() >= 39 )
                {
                    for(int i = 0; i < 39 ; i++) {
                        if(allPlayedKeys.at(i)->getName() == music0KeySequence[i]){
                            pointVerifier += 1;
                        }
                    }

                    if(39 == pointVerifier) {
                        nEstrelas = 3;
                    }else if(20 <= pointVerifier && pointVerifier < 39 ){
                        nEstrelas = 2;
                    }else if(20 > pointVerifier){
                        nEstrelas = 1;
                    }
                    numeroDeEstrelasPontosGame1 = nEstrelas;
                }
                else
                {
                    for(int i = 0; i < allPlayedKeys.size() ; i++) {
                        if(allPlayedKeys.at(i)->getName() == music0KeySequence[i]){
                            pointVerifier += 1;
                        }
                    }
                    if(39 == pointVerifier) {
                        nEstrelas = 3;
                    }else if(20 <= pointVerifier && pointVerifier < 39 ){
                        nEstrelas = 2;
                    }else if(20 > pointVerifier){
                        nEstrelas = 1;
                    }
                    numeroDeEstrelasPontosGame1 = nEstrelas;
                }
            }
        auto victoryStars = CallFunc::create([this]()
                                             {
                                                 PianoScene::animationStars(numeroDeEstrelasPontosGame1);
                                             });
        auto playApplause = CallFunc::create([this]()
                                             {
                                                // CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/Applause.mp3");
                                                 //SimpleAudioEngine::getInstance()->playBackgroundMusic("audio_fundo.mp3", true);
                                             });
        auto playVoiceParabens = CallFunc::create([this]()
                                                  {
                                                      CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/Applause.mp3");
                                                      SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/AudioParabens.mp3");
                                                  });
        auto playVoiceMuitoBem = CallFunc::create([this]()
                                                  {
                                                      CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/Applause.mp3");
                                                      SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/AudioMuitoBom.mp3");
                                                  });
        auto playVoiceContinuePraticando = CallFunc::create([this]()
                                                            {
                                                                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/Applause.mp3");
                                                                SimpleAudioEngine::getInstance()->playEffect("SimonGame/Sound/AudioContinuePraticando.mp3");
                                                            });

       // auto animeBoy = Sequence::create(DelayTime::create(4),EaseElasticOut::create(ScaleTo::create(0.5f,_screenSize.height*0.0008)),nullptr);

        auto x = EaseBounceOut::create(MoveTo::create(1.7f,Vec2(origin.x + (.7*_screenSize.width)/3, origin.y + _screenSize.height + WoodenPlate->getContentSize().height/10)));
        if(frameSize.width >=1024)
            x = EaseBounceOut::create(MoveTo::create(1.7f,Vec2(origin.x + (.7*_screenSize.width)/3, origin.y + _screenSize.height + WoodenPlate->getContentSize().height/10)));
        else
            x = EaseBounceOut::create(MoveTo::create(1.7f,Vec2(origin.x + (.7*_screenSize.width)/3, origin.y + _screenSize.height + WoodenPlate->getContentSize().height/7)));


            auto y = EaseBounceOut::create(MoveTo::create(1.7f,Vec2(origin.x + (2.3*_screenSize.width)/3, origin.y + _screenSize.height + WoodenPlate->getContentSize().height/10)));
            if(frameSize.width >=1024)
                y = EaseBounceOut::create(MoveTo::create(1.7f,Vec2(origin.x + (2.3*_screenSize.width)/3, origin.y + _screenSize.height + WoodenPlate->getContentSize().height/10)));
            else
                y = EaseBounceOut::create(MoveTo::create(1.7f,Vec2(origin.x + (2.3*_screenSize.width)/3, origin.y + _screenSize.height + WoodenPlate->getContentSize().height/7)));


        if(numeroDeEstrelasPontosGame1 == 3)
        {
            SimpleAudioEngine::getInstance()->stopBackgroundMusic();
            WoodenPlate->runAction(Sequence::create(playApplause,DelayTime::create(2),playVoiceParabens,x ,victoryStars,nullptr)); //Animate victory plate

            if (verifyFirstGameNotes) {
                            if(timeToNote.size() > 0) {
                                btReplayVideo->runAction(Sequence::create(DelayTime::create(3),y,nullptr));
                            }
                        }else {
                            btReplayVideo->runAction(Sequence::create(DelayTime::create(3),y,nullptr));
                        }
        }
        else if(numeroDeEstrelasPontosGame1 == 2)
        {
            SimpleAudioEngine::getInstance()->stopBackgroundMusic();
            muitoBemP->runAction(Sequence::create(playApplause,DelayTime::create(2),playVoiceMuitoBem,x ,victoryStars,nullptr));

            if(verifyFirstGameNotes) {
                            if(timeToNote.size() > 0){
                                btReplayVideo->runAction(Sequence::create(DelayTime::create(3),y,nullptr));
                            }
                        }else {
                            btReplayVideo->runAction(Sequence::create(DelayTime::create(3),y,nullptr));
                        }
        }
        else if(numeroDeEstrelasPontosGame1 == 1 || numeroDeEstrelasPontosGame1 == 0)
        {
            SimpleAudioEngine::getInstance()->stopBackgroundMusic();
            continuePraticando->runAction(Sequence::create(playApplause,DelayTime::create(2),playVoiceContinuePraticando,x ,victoryStars,nullptr));

            if(verifyFirstGameNotes) {
                            if(timeToNote.size() > 0){
                                btReplayVideo->runAction(Sequence::create(DelayTime::create(3),y,nullptr));
                            }
                        }else {
                            btReplayVideo->runAction(Sequence::create(DelayTime::create(3),y,nullptr));
                        }
        }
    }
}

void PianoScene::animationStars(int numeroDeEstrelas) // FINAL STARS(MUSIC POINTS)
{


    auto sequenciaStar1 = Sequence::create(DelayTime::create(.1f),FadeIn::create(1.2f),nullptr);
    auto sequenciaStar2 = Sequence::create(DelayTime::create(.2f),FadeIn::create(1.2f),nullptr);
    auto sequenciaStar3 = Sequence::create(DelayTime::create(.3f),FadeIn::create(1.2f),nullptr);

    auto sequenciaStar1_2 = Sequence::create(DelayTime::create(.1f),FadeIn::create(1.2f),nullptr);
    auto sequenciaStar2_2 = Sequence::create(DelayTime::create(.2f),FadeIn::create(1.2f),nullptr);
    auto sequenciaStar3_2 = Sequence::create(DelayTime::create(.3f),FadeIn::create(1.2f),nullptr);

    auto sequenciaStar1_3 = Sequence::create(DelayTime::create(.1f),FadeIn::create(1.2f),nullptr);
    auto sequenciaStar2_3 = Sequence::create(DelayTime::create(.2f),FadeIn::create(1.2f),nullptr);
    auto sequenciaStar3_3 = Sequence::create(DelayTime::create(.3f),FadeIn::create(1.2f),nullptr);

    if(numeroDeEstrelas == 1 || numeroDeEstrelas == 0)
    {
        congratStar1->runAction(sequenciaStar1);
        congratStar1_2->runAction(sequenciaStar1_2);
        congratStar1_3->runAction(sequenciaStar1_3);
    }
    else if(numeroDeEstrelas == 2)
    {
        congratStar1->runAction(sequenciaStar1);
        congratStar1_2->runAction(sequenciaStar1_2);
        congratStar1_3->runAction(sequenciaStar1_3);
        congratStar2->runAction(sequenciaStar2);
        congratStar2_2->runAction(sequenciaStar2_2);
        congratStar2_3->runAction(sequenciaStar2_3);
    }
    else if(numeroDeEstrelas == 3)
    {
        congratStar1->runAction(sequenciaStar1);
        congratStar1_2->runAction(sequenciaStar1_2);
        congratStar1_3->runAction(sequenciaStar1_3);
        congratStar2->runAction(sequenciaStar2);
        congratStar2_2->runAction(sequenciaStar2_2);
        congratStar2_3->runAction(sequenciaStar2_3);
        congratStar3->runAction(sequenciaStar3);
        congratStar3_2->runAction(sequenciaStar3_2);
        congratStar3_3->runAction(sequenciaStar3_3);
    }
}

void PianoScene::onTouchesEnded(const std::vector<Touch*>& touches, Event *unused_event) {
    if(!replay){
        if(_showOkayBoy) return;

        for(Node* key : _touchedKeys) {
            key->setVisible(false);
        }
        _touchedKeys.clear();
    }
}
void PianoScene::playPauseBgMusic() {
	if(_playBgMusic) {
		SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	} else {
		SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	}
	_playBgMusic = !_playBgMusic;
	_iconBgMusic1->setVisible(_playBgMusic);
	_iconBgMusic2->setVisible(_playBgMusic);
	_iconBgMusic3->setVisible(_playBgMusic);
	_muteBgMusic->setVisible(!_playBgMusic);
}
void PianoScene::startBGMusicAction() {
	_iconBgMusic1->setVisible(true);
	auto act1 = createBgMusicAction1();
	_iconBgMusic1->runAction(act1);

	this->scheduleOnce(schedule_selector(PianoScene::startBGMusicAction2), 0.4f);
	this->scheduleOnce(schedule_selector(PianoScene::startBGMusicAction3), 0.8f);
}
void PianoScene::startBGMusicAction2(float dt) {
	_iconBgMusic2->setVisible(true);
	auto act2 = createBgMusicAction2();
	_iconBgMusic2->runAction(act2);
}
void PianoScene::startBGMusicAction3(float dt) {
	_iconBgMusic3->setVisible(true);
	auto act3 = createBgMusicAction3();
	_iconBgMusic3->runAction(act3);
}
Action* PianoScene::createBgMusicAction1() {
	return RepeatForever::create(
			Sequence::create(
					MoveTo::create(0.0f, _posBgMusic),
					FadeIn::create(0.1f),
					MoveBy::create(0.7f, Vec2(5.0f, _margin * 3)),
					FadeOut::create(0.2f),
					DelayTime::create(0.2f),
					nullptr
					)
			);
}
Action* PianoScene::createBgMusicAction2() {
	return RepeatForever::create(
			Sequence::create(
					MoveTo::create(0.0f, _posBgMusic),
					FadeIn::create(0.1f),
					MoveBy::create(0.7f, Vec2(-1.0f * _margin, _margin * 3)),
					FadeOut::create(0.2f),
					DelayTime::create(0.2f),
					nullptr
					)
			);
}
Action* PianoScene::createBgMusicAction3() {
	return RepeatForever::create(
			Sequence::create(
					MoveTo::create(0.0f, Vec2(_posBgMusic.x, _posBgMusic.y - _margin * 2)),
					FadeIn::create(0.1f),
					MoveBy::create(0.7f, Vec2(-0.5f * _margin, _margin * 3)),
					FadeOut::create(0.2f),
					DelayTime::create(0.2f),
					nullptr
					)
			);
}
Action* PianoScene::createJogoAction() {
	return Sequence::create(
			EaseIn::create(RotateBy::create(0.5f, -210.0f), 2),
			EaseIn::create(RotateBy::create(0.4f, 50.0f), 2),
			RotateBy::create(0.3f, -30.0f),
			RotateBy::create(0.2f, 15.0f),
			RotateBy::create(0.1f, -7.5f),
			RotateBy::create(0.1f, 2.5f),
			nullptr
		);
}
