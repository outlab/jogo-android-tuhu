#include "SplashScene.h"
#include "MenuScene.h"
#include "MenuScene.cpp"

SplashScene::SplashScene() {
	_loadThread = nullptr;
}
SplashScene::~SplashScene() {
	if(_loadThread != nullptr)
		delete _loadThread;
}
Scene* SplashScene::scene() {
	Scene *scene = Scene::create();

	SplashScene *layer = SplashScene::create();
	scene->addChild(layer);

	return scene;
}

bool SplashScene::init() {
	if(!Layer::init())
		return false;

	_screenSize = Director::getInstance()->getWinSize();

	auto vplayer = experimental::ui::VideoPlayer::create();
	vplayer->setPosition(Vec2(_screenSize.width * 0.5f, _screenSize.height * 0.5f));
	vplayer->setContentSize(_screenSize);
	vplayer->setFileName("fundobranco.mp4");
	vplayer->play();
	this->addChild(vplayer);

	this->scheduleOnce(schedule_selector(SplashScene::viewMenuScene), TIME_SPLASH);

	_loadThread = new std::thread(&SplashScene::loadResources, this);

	return true;
}

void SplashScene::loadResources() {
	log("start to load effects...");
	char szKeyFile[32];
	for(int i = 0; i < 6; i++) {
		memset(szKeyFile, 0, sizeof(char) * 32);
		sprintf(szKeyFile, "guitar/%d_%d.mp3", i + 1, i);
		SimpleAudioEngine::getInstance()->preloadEffect(szKeyFile);
	}

	log("start to load musics...");
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("Jogo01/tremzinho.mp3");
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("Jogo01/tororo.mp3");
	SimpleAudioEngine::getInstance()->preloadBackgroundMusic("Jogo01/carneirinho.mp3");

	log("complete to load resources!");
}

void SplashScene::viewMenuScene(float dt) {
	_loadThread->join();

	auto pScene = MenuScene::scene();
	auto pTran = TransitionFade::create(0.5f, pScene);
	Director::getInstance()->replaceScene(pTran);
}
