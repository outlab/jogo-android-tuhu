#ifndef __SIMON_GAME_SCENE_H__
#define __SIMON_GAME_SCENE_H__

#include "MusicList.h"
#include "cocos2d.h"

using namespace std;
using namespace cocos2d;

class SimonGame : public cocos2d::Layer
{
private:

  vector<SoundNoteaa> _Musica;
    void animateWave(float dt);
    void verifySound(int soundIndex);
    void playSound(int soundIndex);
    void animate7(Ref* pSender);
    void playSoundStar1(Ref* pSender);
    void playSoundStar2(Ref* pSender);
    void playSoundStar3(Ref* pSender);
    void playSoundStar4(Ref* pSender);
    void playSoundStar5(Ref* pSender);
    void playSoundStar6(Ref* pSender);
    void playSoundStar7(Ref* pSender);
    void animateBright(Sprite* sprite,MenuItemImage* star);
    void verifySequenceSize(int sequenceSize);
    void manageSound(float dt); //Main function control play sequences
    void manageSoundFinal(float dt); //Plays the entire sequence at the final of the game
    void animateVictory(float dt);
    void animationStars(int numeroDeEstrelas);
    void animationPopText(bool Correct);
    void preloadSounds(int SoundBlock, int SoundBlock2);
    void Animate(float dt);
    void animateStarShake(float dt);
    void loadNext(Ref* pSender); //Load next music block
    void backMusicScene(Ref* pSender);
    void loadThisScene(Ref* pSender);
    void pauseGame(Ref* pSender);
    void resumeGame(Ref* pSender);
    void changeInstrument(Ref* pSender);
    void animateKeys();
    void scaleProgress();
    //void escala();
    Node* tocaAudio;
    Node* animaCortina;

    Label* texto;
    //_String* temp;

    int keyIndex; // Define qual audio da musica foi tocado por ultimo 1 a 12
    int keyIndexAux; // Define qual bloco de audio esta sendo executado 1 a 4
    int keySoundinBlock; // Define qual audio do bloco está sendo executado 1 a 3    
    int keyIndexFinal; // Plays the entire song if the player wins
    int positionCheck; // guarda a ultima posição de som clicada pelo player
    int clickCount; //Conta se o numero de cliques foi igual ao de sons do bloco
    int angleIncrement; //Helps generate animation start swing

    bool firstPlay; // first 5 seconds before start - defines if it's the first time playing this
    bool doneSequence; // ready to read player touches
    bool wrongSound; // defines if the player played the right sound or not
    bool finishedMusic;
    bool rightSwing;
    bool flagTextAngle;
    bool paused;

    Size visibleSize;
    Size _screenSize;
    
    Vec2 origin;
    Vec2 origemStar1;
    Vec2 origemStar2;
    Vec2 origemStar3;
    Vec2 origemStar4;
    Vec2 origemStar5;
    Vec2 origemStar6;
    Vec2 origemStar7;

    MusicList _ListaDeMusicas;

    Menu* menu1;
    Menu* menu2;
    Menu* menu3;
    Menu* menu4;
    Menu* menu5;
    Menu* menu6;
    Menu* menu7;

    Sprite* C;
    Sprite* D;
    Sprite* E;
    Sprite* F;
    Sprite* G;
    Sprite* A;
    Sprite* B;
    Sprite* goldenLine2;
    Sprite* goldenLine;
    Sprite* blackFadeReplayBotton;
    Sprite* continuePraticando;
    Sprite* muitoBemP;
    Sprite* cortinaDir;
    Sprite* cortinaEsc;
    Sprite* cortinas;
    Sprite* background;
    Sprite* bright1;
    Sprite* bright2;
    Sprite* bright3;
    Sprite* bright4;
    Sprite* bright5;
    Sprite* bright6;
    Sprite* bright7;
    Sprite* WoodenPlate;
    Sprite* congratStar1;
    Sprite* congratStar2;
    Sprite* congratStar3;
    Sprite* congratStar1_2;
    Sprite* congratStar2_2;
    Sprite* congratStar3_2;
    Sprite* congratStar1_3;
    Sprite* congratStar2_3;
    Sprite* congratStar3_3;
    Sprite* muitoBem;
    Sprite* suaVez;
    Sprite* ops;
    Sprite* blackBG;
    Sprite* pro;
    Sprite* bola;
    Sprite* bola2;
    Sprite* bbola;
    Sprite* bar;
    Sprite* vermelho;
    Sprite* dourado;
    Sprite* tuhuLegal;

    MenuItemImage* star1;
    MenuItemImage* botao1;
    MenuItemImage* star2;
    MenuItemImage* star3;
    MenuItemImage* star4;
    MenuItemImage* star5;
    MenuItemImage* star6;
    MenuItemImage* star7;
    MenuItemImage* backButton;
    MenuItemImage* nextTrack;
    MenuItemImage* replayButton;
    MenuItemImage* replayButton2;
    MenuItemImage* replayButton3;
    MenuItemImage* backButton2;
    MenuItemImage* backButton3;
    MenuItemImage* backButton4;
    MenuItemImage* nextTrack2;
    MenuItemImage* nextTrack3;
    MenuItemImage* nextTrack4;
    MenuItemImage* selectInstruments;
    MenuItemImage* goMenu;
    MenuItemImage* selectMusic;
    MenuItemImage* playButton;

public:
    static cocos2d::Scene* createScene(int Musica,int StringOrBlow);

    virtual bool init();
    virtual void update(float dt);
    void loadMenuScene(Ref* pSender);

    SimonGame();

    // implement the "static create()" method manually
    CREATE_FUNC(SimonGame);
};

#endif // __SIMON_GAME_SCENE_H__