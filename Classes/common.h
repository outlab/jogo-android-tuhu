#include "cocos2d.h"
#include "tag.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace ui;
using namespace CocosDenshion;

#define APP_WIDTH	1350
#define APP_HEIGHT	768

#define TIME_SPLASH 		4.0f
#define TIME_CLOUD_MOVE 	15.0f
#define TIME_MENU_EFFECT	5.0f
#define TIME_UPDATE_WHEELS  0.002f
#define TIME_UPDATE_LIGHT  0.015f

#define AUDIO_04 "SimonGame/Sound/v04.mp3"  //1
#define TIME_AUDIO_04 0.705f

#define AUDIO_05 "SimonGame/Sound/v05.mp3" //2
#define TIME_AUDIO_05 1.384f