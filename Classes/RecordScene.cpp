/*
 * RecordScene.cpp
 *
 *  Created on: 2015. 12. 10.
 *      Author: Jimmy
 */

#include "RecordScene.h"
#include "MenuScene.h"
#include "platform/android/jni/JniHelper.h"
#include <jni.h>

RecordScene::RecordScene() {
	// TODO Auto-generated constructor stub
	_margin = 20.0f;
	_boy = nullptr;
	_icoMusic = nullptr;
	_icoMute = nullptr;
	_selInstrument = nullptr;
	_instruments = nullptr;
	_piano = nullptr;
	_currentInstrument = 0;
	_isPlayBgMusic = false;
	_isDramLongPlayed = false;
	_dram = nullptr;
	_guitar = nullptr;
	_btnBack = nullptr;
	_btnBackOfRecord = nullptr;
	_btnRecord = nullptr;
	_recording = nullptr;
	_recordBoy = nullptr;
	_recordBoyAction = nullptr;
	_recReady3 = nullptr;
	_recReady2 = nullptr;
	_recReady1 = nullptr;
	_cartLeft = nullptr;
	_cartRight = nullptr;
	_middlePos = 0.0f;
}

RecordScene::~RecordScene() {
	// TODO Auto-generated destructor stub
	for(GUITAR_LINE* line : _guitarLines) {
		delete line;
	}
}

Scene* RecordScene::scene() {
	Scene *scene = Scene::create();

	RecordScene *layer = RecordScene::create();
	scene->addChild(layer);

	return scene;
}

bool RecordScene::init() {
	if(!Layer::init())
		return false;

	EventDispatcher* dispatcher = Director::getInstance()->getEventDispatcher();
	auto positionListener = EventListenerTouchOneByOne::create();
	positionListener->setSwallowTouches(true);
	positionListener->onTouchBegan = CC_CALLBACK_2(RecordScene::onTouchBegan, this);
	positionListener->onTouchMoved = CC_CALLBACK_2(RecordScene::onTouchMoved, this);
	positionListener->onTouchEnded = CC_CALLBACK_2(RecordScene::onTouchEnded, this);
	dispatcher->addEventListenerWithSceneGraphPriority(positionListener, this);

	auto multitouchListener = EventListenerTouchAllAtOnce::create();
	multitouchListener->onTouchesBegan = CC_CALLBACK_2(RecordScene::onTouchesBegan, this);
	multitouchListener->onTouchesMoved = CC_CALLBACK_2(RecordScene::onTouchesMoved, this);
	multitouchListener->onTouchesEnded = CC_CALLBACK_2(RecordScene::onTouchesEnded, this);
	dispatcher->addEventListenerWithSceneGraphPriority(multitouchListener, this);

	_screenSize = Director::getInstance()->getWinSize();

	auto jogo02 = CSLoader::createNode("Jogo02/Jogo02.csb");
	auto background = jogo02->getChildByTag(153);
	background->setPosition(Vec2(_screenSize.width * 0.5f, _screenSize.height * 0.5f));
	background->setScale(_screenSize.width / background->getContentSize().width, _screenSize.height / background->getContentSize().height);
	this->addChild(background, 0);

	_bgRecord = jogo02->getChildByTag(127);
	this->addChild(_bgRecord, 0);
	_bgRecord->setVisible(false);

	auto backLeft = jogo02->getChildByTag(165);
	backLeft->setPosition(Vec2(_screenSize.width * 0.5f - backLeft->getContentSize().width * 0.1f, _screenSize.height * 0.5f));
	backLeft->setScale(1.5f);
	this->addChild(backLeft, 1);
	auto backRight = jogo02->getChildByTag(164);
	backRight->setPosition(Vec2(_screenSize.width * 0.5f + backRight->getContentSize().width * 0.1f, _screenSize.height * 0.5f));
	backRight->setScale(1.5f);
	this->addChild(backRight, 1);

	_btnBack = (Button*)jogo02->getChildByTag(154);
	float fTopButton = _screenSize.height - 0.5f * _btnBack->getContentSize().height - _margin;
	_btnBack->setPosition(Vec2(_margin + 0.5f * _btnBack->getContentSize().width, fTopButton));
	_btnBack->addTouchEventListener(CC_CALLBACK_2(RecordScene::onTouchButtons, this));
	this->addChild(_btnBack, 2);

	auto record = CSLoader::createNode("Jogo02/Record.csb");
	_btnBackOfRecord = (Button*)record->getChildByTag(218);
	_btnBackOfRecord->setPosition(Vec2(_margin + 0.5f * _btnBack->getContentSize().width, fTopButton));
	_btnBackOfRecord->addTouchEventListener(CC_CALLBACK_2(RecordScene::onTouchButtons, this));
	_btnBackOfRecord->setVisible(false);
	this->addChild(_btnBackOfRecord, 2);

	_btnRecord = (Button*)jogo02->getChildByTag(155);
	_btnRecord->setPosition(Vec2(_btnBack->getContentSize().width + _margin * 8, fTopButton));
	_btnRecord->setScale(0);
	_btnRecord->addTouchEventListener(CC_CALLBACK_2(RecordScene::onTouchButtons, this));
	this->addChild(_btnRecord, 2);

	_recording = record->getChildByTag(192);
	_recording->setPosition(Vec2(_btnBack->getContentSize().width + _margin * 8, fTopButton));
	_recording->setVisible(false);
	this->addChild(_recording, 2);

	_recReady3 = record->getChildByTag(201);
	_recReady2 = record->getChildByTag(202);
	_recReady1 = record->getChildByTag(203);
	_recReady3->setPosition(Vec2(_btnBack->getContentSize().width + _margin * 8, fTopButton));
	_recReady2->setPosition(Vec2(_btnBack->getContentSize().width + _margin * 8, fTopButton));
	_recReady1->setPosition(Vec2(_btnBack->getContentSize().width + _margin * 8, fTopButton));
	_recReady3->setOpacity(0);
	_recReady2->setOpacity(0);
	_recReady1->setOpacity(0);
	this->addChild(_recReady3, 2);
	this->addChild(_recReady2, 2);
	this->addChild(_recReady1, 2);

	_instruments = jogo02->getChildByTag(168);
	_instruments->setPosition(Vec2(_screenSize.width * 0.5f, _instruments->getContentSize().height * -0.25f));
	this->addChild(_instruments, 4);
	_middlePos = _instruments->getContentSize().height * 0.75f;

	_bgRecord->setPosition(Vec2(_screenSize.width * 0.5f, _middlePos));
	float fScale = (_screenSize.height - _middlePos) / _bgRecord->getContentSize().height;
	_bgRecord->setScale(fScale);

	_piano = _instruments->getChildByTag(1000);
	_pianoKeys.clear();
	for(int i = 172; i <= 181; i++) {
		auto pianoKey = _piano->getChildByTag(i);
		pianoKey->setVisible(false);

		_pianoKeys.pushBack(pianoKey);
	}
	_pianoHalfKeys.clear();
	for(int i = 182; i <= 188; i++) {
		auto halfKey = _piano->getChildByTag(i);
		halfKey->setVisible(false);

		_pianoHalfKeys.pushBack(halfKey);
	}
	_dram = _instruments->getChildByTag(1001);
	_dramKeys.clear();
	for(int i = 197; i <= 201; i++) {
		auto dramKey = _dram->getChildByTag(i);
		dramKey->setVisible(false);
		_dramKeys.pushBack(dramKey);
	}
	_guitar = _instruments->getChildByTag(1002);
	_guitarLines.clear();
	for(int i = 10021; i <= 10026; i++) {
		auto line = _guitar->getChildByTag(i);
		GUITAR_LINE* pLine = new GUITAR_LINE;
		pLine->_node = line;
		pLine->_rect = line->getBoundingBox();
		line->setScale(0.8f, 0.0f);
		_guitarLines.push_back(pLine);
	}

	auto boy = CSLoader::createNode("Jogo02/Boy.csb");
	_boy = boy->getChildByTag(40);
	_boy->setPosition(Vec2(_screenSize.width * 0.5f, _middlePos));
	this->addChild(_boy, 3);

	auto recBoy = CSLoader::createNode("Jogo02/RecordBoy.csb");
	_recordBoy = recBoy->getChildByTag(215);
	_recordBoy->setPosition(Vec2(_screenSize.width * 0.5f, _middlePos));
	_recordBoy->setVisible(false);
	this->addChild(_recordBoy, 3);

	_recordBoyAction = CSLoader::createTimeline("Jogo02/RecordBoy.csb");
	_recordBoyAction->retain();
	_recordBoy->runAction(_recordBoyAction);

	_icoMusic = jogo02->getChildByTag(160);
	_icoMusic->setPosition(Vec2(_screenSize.width - 0.5f * _icoMusic->getContentSize().width - _margin, fTopButton + _margin));
	this->addChild(_icoMusic, 2);

	_icoMute = jogo02->getChildByTag(162);
	_icoMute->setPosition(Vec2(_screenSize.width - 0.5f * _icoMusic->getContentSize().width - _margin, fTopButton));
	_icoMute->setVisible(false);
	this->addChild(_icoMute, 2);

	_selInstrument = (Button*)jogo02->getChildByTag(156);
	_selInstrument->setPosition(Vec2(_screenSize.width - _margin * 10 - _icoMute->getContentSize().width, fTopButton));
	_selInstrument->addTouchEventListener(CC_CALLBACK_2(RecordScene::onTouchButtons, this));
	this->addChild(_selInstrument, 2);

	_cartLeft = jogo02->getChildByTag(163);
	_cartRight = jogo02->getChildByTag(166);
	_cartLeft->setPosition(Vec2(_screenSize.width * 0.5f, _middlePos));
	_cartRight->setPosition(Vec2(_screenSize.width * 0.5f, _middlePos));
	_cartLeft->setScale(1.5f);
	_cartRight->setScale(1.5f);
	this->addChild(_cartLeft, 5);
	this->addChild(_cartRight, 5);

	_rcIconMusic = Rect(_screenSize.width - 100.0f, _screenSize.height - 100.0f, 250.0f, 250.0f);

	openCart();

	this->scheduleOnce(schedule_selector(RecordScene::readyStart), 1.5f);

	selectInstrument();

	return true;
}
void RecordScene::openCart() {
	auto actLeft = Sequence::create(DelayTime::create(0.5f), MoveTo::create(1.0f, Vec2(-1.0f * _margin, _middlePos)), nullptr);
	_cartLeft->runAction(actLeft);
	auto actRight = Sequence::create(DelayTime::create(0.5f), MoveTo::create(1.0f, Vec2(_margin + _screenSize.width, _middlePos)), nullptr);
	_cartRight->runAction(actRight);

	SimpleAudioEngine::getInstance()->playEffect("Effect/cortina.ogg");
}
void RecordScene::closeCart() {
	auto actLeft = Sequence::create(DelayTime::create(0.5f), MoveTo::create(1.0f, Vec2(_screenSize.width * 0.5f, _middlePos)), nullptr);
	_cartLeft->runAction(actLeft);
	auto actRight = Sequence::create(DelayTime::create(0.5f), MoveTo::create(1.0f, Vec2(_screenSize.width * 0.5f, _middlePos)), nullptr);
	_cartRight->runAction(actRight);

	SimpleAudioEngine::getInstance()->playEffect("Effect/cortina.ogg");
}
void RecordScene::readyStart(float dt) {
	auto action = CSLoader::createTimeline("Jogo02/Boy.csb");
	_boy->runAction(action);
	action->gotoFrameAndPlay(120, 240, false);

	auto actMusic = CSLoader::createTimeline("Jogo02/Jogo02.csb");
	actMusic->gotoFrameAndPlay(0, 15, false);
	_icoMusic->runAction(actMusic);

	SimpleAudioEngine::getInstance()->playEffect("Sons/E_hora_do_show.ogg");

	this->scheduleOnce(schedule_selector(RecordScene::playStart), 2.5f);
}
void RecordScene::playStart(float dt) {
	auto action = CSLoader::createTimeline("Jogo02/Boy.csb");
	_boy->runAction(action);
	action->gotoFrameAndPlay(0, 120, true);

	SimpleAudioEngine::getInstance()->playBackgroundMusic("Jogo02/loop_jogo_2.mp3", true);
	_isPlayBgMusic = true;
}
void RecordScene::selectInstrument() {
	_selInstrument->getChildByTag(157)->setVisible(false);
	_selInstrument->getChildByTag(158)->setVisible(false);
	_selInstrument->getChildByTag(159)->setVisible(false);
	_selInstrument->getChildByTag(157 + _currentInstrument)->setVisible(true);

	_instruments->getChildByTag(1000)->setVisible(false);
	_instruments->getChildByTag(1001)->setVisible(false);
	_instruments->getChildByTag(1002)->setVisible(false);
	_instruments->getChildByTag(1000 + _currentInstrument)->setVisible(true);
}
void RecordScene::backMenu() {
	SimpleAudioEngine::getInstance()->playEffect("Effect/voltar.ogg");
	auto pScene = MenuScene::scene();
	Director::getInstance()->replaceScene(pScene);
}
void RecordScene::onTouchButtons(Ref* pSender, Widget::TouchEventType type) {
	if(type == Widget::TouchEventType::ENDED) {
		int iTag = ((Button*)pSender)->getTag();
		switch(iTag) {
		case 156:
			_currentInstrument = (_currentInstrument + 1) % 3;
			selectInstrument();
			break;
		case 154:
		case 218:
			backMenu();
			break;
		case 155:
			readyRecord();
			break;
		default:
			break;
		}
	}
}
void RecordScene::readyRecord() {
	log("ready record 1");

	closeCart();

	this->scheduleOnce(schedule_selector(RecordScene::readyRecord2), 2.0f);
}
void RecordScene::readyRecord2(float dt) {
	log("ready record 2");
	_btnBack->setVisible(false);
	_btnRecord->setVisible(false);
	_boy->setVisible(false);

	_bgRecord->setVisible(true);
	_btnBackOfRecord->setVisible(true);
	_recording->setVisible(true);
	for(int i = 196; i <= 199; i++)
		_recording->getChildByTag(i)->setOpacity(0);

	_recordBoy->setVisible(true);

	openCart();

	this->scheduleOnce(schedule_selector(RecordScene::readyRecord3), 2.0f);
}
void RecordScene::readyRecord3(float dt) {
	log("ready record 3");
	SimpleAudioEngine::getInstance()->playEffect("Effect/countdown.ogg");

	_recordBoyAction->gotoFrameAndPlay(0, 180, false);

	auto act3 = Sequence::create(
			FadeIn::create(0.0f),
			MoveTo::create(0.0f, _recording->getPosition()),
			Spawn::create(
				MoveBy::create(1.0f, Vec2(_margin * 4.0f, _margin * -8.0f)),
				Sequence::create(
						DelayTime::create(0.5f),
						FadeOut::create(0.5f),
						nullptr
						),
				nullptr
			),
			nullptr
			);
	_recReady3->runAction(act3);

	auto act2 = Sequence::create(
			DelayTime::create(1.0f),
			FadeIn::create(0.0f),
			MoveTo::create(0.0f, _recording->getPosition()),
			Spawn::create(
				MoveBy::create(1.0f, Vec2(_margin * -4.0f, _margin * -8.0f)),
				Sequence::create(
						DelayTime::create(0.5f),
						FadeOut::create(0.5f),
						nullptr
						),
				nullptr
			),
			nullptr
			);
	_recReady2->runAction(act2);

	auto act1 = Sequence::create(
			DelayTime::create(2.0f),
			FadeIn::create(0.0f),
			MoveTo::create(0.0f, _recording->getPosition()),
			Spawn::create(
				MoveBy::create(1.0f, Vec2(0.0f, _margin * -8.0f)),
				Sequence::create(
						DelayTime::create(0.5f),
						FadeOut::create(0.5f),
						nullptr
						),
				nullptr
			),
			nullptr
			);
	_recReady1->runAction(act1);

	this->scheduleOnce(schedule_selector(RecordScene::readyRecord4), 3.0f);
}
void RecordScene::readyRecord4(float dt) {
	log("ready record 4");
	SimpleAudioEngine::getInstance()->playEffect("Sons/Gravando.ogg");
	_recordBoyAction->gotoFrameAndPlay(240, 390, false);
	this->scheduleOnce(schedule_selector(RecordScene::startRecord), 3.0f);
}
void RecordScene::startRecord(float dt) {
	log("start record!");
	JniMethodInfo methodInfo;
	if(!JniHelper::getStaticMethodInfo(methodInfo, "com/everyplay/Everyplay/Everyplay", "startRecording", "()V")) {
		return;
	}

	methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);

	_recordBoyAction->gotoFrameAndPlay(180, 240, true);

	this->scheduleOnce(schedule_selector(RecordScene::timeoutRecord), 60.0f);

	_recording->getChildByTag(200)->runAction(RotateBy::create(60.0f, 360.0f));
	for(int i = 196; i <= 199; i++) {
		auto act = Sequence::create(
				DelayTime::create(15.0f * (i - 195)),
				FadeIn::create(0.3f),
				nullptr
				);
		_recording->getChildByTag(i)->runAction(act);
	}
}
void RecordScene::timeoutRecord(float dt) {
	log("record timeout!");
	stopRecord();
}
void RecordScene::stopRecord() {
	log("stop record!");
	JniMethodInfo methodInfo;
	if(!JniHelper::getStaticMethodInfo(methodInfo, "com/everyplay/Everyplay/Everyplay", "stopRecording", "()V")) {
		return;
	}

	methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);

	closeCart();
	this->scheduleOnce(schedule_selector(RecordScene::stopRecordAfter), 2.0f);
}
void RecordScene::stopRecordAfter(float dt) {
	JniMethodInfo methodShare;
	if(!JniHelper::getStaticMethodInfo(methodShare, "com/everyplay/Everyplay/Everyplay", "showEveryplaySharing", "()Z")) {
		return;
	}

	methodShare.env->CallStaticBooleanMethod(methodShare.classID, methodShare.methodID);
	methodShare.env->DeleteLocalRef(methodShare.classID);

	_bgRecord->setVisible(false);
	_btnBackOfRecord->setVisible(false);
	_recording->setVisible(false);
	_recordBoy->setVisible(false);

	_btnBack->setVisible(true);
	_btnRecord->setVisible(true);
	_boy->setVisible(true);

	_recordBoyAction->gotoFrameAndPause(0);
	_recReady3->stopAllActions();
	_recReady2->stopAllActions();
	_recReady1->stopAllActions();
	_recording->stopAllActions();
	_recording->getChildByTag(200)->stopAllActions();
	_recording->getChildByTag(200)->setRotation(0.0f);
	for(int i = 196; i <= 199; i++) {
		_recording->getChildByTag(i)->stopAllActions();
	}

	openCart();
}
bool RecordScene::onTouchBegan(Touch *touch, Event *unused_event) {
	if(_rcIconMusic.containsPoint(touch->getLocation()) ||
			_recording->getBoundingBox().containsPoint(touch->getLocation()))
		return true;

	return false;
}
void RecordScene::onTouchMoved(Touch *touch, Event *unused_event) {
}
void RecordScene::onTouchEnded(Touch *touch, Event *unused_event) {
	if(_rcIconMusic.containsPoint(touch->getLocation())) {
		SimpleAudioEngine::getInstance()->playEffect("Effect/bt.ogg");
		if(_isPlayBgMusic) {
			SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
		} else {
			SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
		}
		_icoMusic->setVisible(!_isPlayBgMusic);
		_icoMute->setVisible(_isPlayBgMusic);
		_isPlayBgMusic = !_isPlayBgMusic;
	} else if(_recording->getBoundingBox().containsPoint(touch->getLocation())) {
		stopRecord();
	}
}
void RecordScene::onTouchesBegan(const std::vector<Touch*>& touches, Event *unused_event) {
	char szKeyFile[512];

	if(_currentInstrument == 0) {
		for(Touch* touch : touches) {
			Vec2 pt = _piano->convertToNodeSpace(touch->getLocation());
			Node* touchedKey = nullptr;
			for(Node* halfKey : _pianoHalfKeys) {
				if(halfKey->getBoundingBox().containsPoint(pt)) {
					touchedKey = halfKey;
					break;
				}
			}
			if(touchedKey == nullptr) {
				for(Node* key : _pianoKeys) {
					if(key->getBoundingBox().containsPoint(pt)) {
						touchedKey = key;
						break;
					}
				}
			}
			if(touchedKey == nullptr) continue;

			_touchedKeys.pushBack(touchedKey);
			touchedKey->setVisible(true);

			memset(szKeyFile, 0, sizeof(char) * 512);
			sprintf(szKeyFile, "%s.ogg", touchedKey->getName().c_str());
			//log("play key sound! filename is %s", szKeyFile);
			SimpleAudioEngine::getInstance()->playEffect(szKeyFile);
		}
	} else if(_currentInstrument == 1) {
		_isDramLongPlayed = false;
		for(Touch* touch : touches) {
			Vec2 pt = _dram->convertToNodeSpace(touch->getLocation());
			Node* touchedKey = nullptr;
			for(Node* key : _dramKeys) {
				if(key->getBoundingBox().containsPoint(pt)) {
					touchedKey = key;
					break;
				}
			}
			if(touchedKey == nullptr) continue;

			_touchedKeys.pushBack(touchedKey);
			touchedKey->setVisible(true);
		}
		this->scheduleOnce(schedule_selector(RecordScene::playDramLongSound), 0.5f);
	} else if(_currentInstrument == 2) {
		playGuitar(touches, false);
	}
}
void RecordScene::onTouchesMoved(const std::vector<Touch*>& touches, Event *unused_event) {
	if(_currentInstrument == 0) {
		char szKeyFile[512];
		Vec2 pt;
		for(Touch* touch : touches) {
			pt = _piano->convertToNodeSpace(touch->getLocation());
			Node* touchedKey = nullptr;
			for(Node* halfKey : _pianoHalfKeys) {
				if(halfKey->getBoundingBox().containsPoint(pt)) {
					touchedKey = halfKey;
					break;
				}
			}
			if(touchedKey == nullptr) {
				for(Node* key : _pianoKeys) {
					if(key->getBoundingBox().containsPoint(pt)) {
						touchedKey = key;
						break;
					}
				}
			}
			if(touchedKey == nullptr) continue;
			if(_touchedKeys.contains(touchedKey)) continue;

			_touchedKeys.pushBack(touchedKey);
			touchedKey->setVisible(true);

			memset(szKeyFile, 0, sizeof(char) * 512);
			sprintf(szKeyFile, "%s.ogg", touchedKey->getName().c_str());
			//log("play key sound! filename is %s", szKeyFile);
			SimpleAudioEngine::getInstance()->playEffect(szKeyFile);
		}
		bool bExist = false;
		for(Node* key : _touchedKeys) {
			bExist = false;
			for(Touch* touch : touches) {
				pt = _piano->convertToNodeSpace(touch->getLocation());
				if(key->getBoundingBox().containsPoint(pt)) {
					bExist = true;
					break;
				}
			}
			if(!bExist) {
				key->setVisible(false);
				_touchedKeys.eraseObject(key);
			}
		}

	} else if(_currentInstrument == 2) {
		playGuitar(touches, true);
	}
}
void RecordScene::onTouchesEnded(const std::vector<Touch*>& touches, Event *unused_event) {
	if(_currentInstrument == 0) {
		for(Node* key : _touchedKeys) {
			key->setVisible(false);

		}
		_touchedKeys.clear();
	} else if(_currentInstrument == 1) {
		this->unschedule(schedule_selector(RecordScene::playDramLongSound));

		char szKeyFile[512];
		if(!_isDramLongPlayed) {
			for(Node* key : _touchedKeys) {
				key->setVisible(false);
				memset(szKeyFile, 0, sizeof(char) * 512);
				sprintf(szKeyFile, "dram/%s.ogg", key->getName().c_str());
				//log("play key sound! filename is %s", szKeyFile);
				SimpleAudioEngine::getInstance()->playEffect(szKeyFile);
			}
			_touchedKeys.clear();
		}
	} else if(_currentInstrument == 2) {
		_touchedKeys.clear();
	}
}
void RecordScene::playGuitar(const std::vector<Touch*>& touches, bool bIsMove) {
	char szKeyFile[32];
	char szKeyName[16];
	for(Touch* touch : touches) {
		Vec2 pt = _guitar->convertToNodeSpace(touch->getLocation());
		Node* touchedKey = nullptr;
		for(GUITAR_LINE* line : _guitarLines) {
			if(line->_rect.containsPoint(pt)) {
				touchedKey = line->_node;
				break;
			}
		}
		if(touchedKey == nullptr) continue;
		if(bIsMove && _touchedKeys.contains(touchedKey)) continue;

		int i = 0;
		for(i = 0; i <= 6; i++) {
			memset(szKeyName, 0, sizeof(char) * 16);
			sprintf(szKeyName, "%s_%d", touchedKey->getName().c_str(), i);
			auto node = _guitar->getChildByName(szKeyName);
			if(node == nullptr) continue;
			if(node->getBoundingBox().containsPoint(pt))
				break;
		}
		if(i > 5) i = 0;

		_touchedKeys.pushBack(touchedKey);
		touchedKey->setVisible(true);
		touchedKey->stopAllActions();
		auto act = Sequence::create(
						ScaleTo::create(0.0f, 0.8f, 1.0f),
						ScaleTo::create(2.0f, 0.8f, 0.0f),
						nullptr
						);
		touchedKey->runAction(act);

		memset(szKeyFile, 0, sizeof(char) * 32);
		sprintf(szKeyFile, "guitar/%s_%d.mp3", touchedKey->getName().c_str(), i);
		//log("play key sound! filename is %s", szKeyFile);
		SimpleAudioEngine::getInstance()->playEffect(szKeyFile);
	}
}
void RecordScene::playDramLongSound(float dt) {
	_isDramLongPlayed = true;
	char szKeyFile[512];
	for(Node* key : _touchedKeys) {
		key->setVisible(false);
		memset(szKeyFile, 0, sizeof(char) * 512);
		sprintf(szKeyFile, "dram/%s_.ogg", key->getName().c_str());
		log("play key sound! filename is %s", szKeyFile);
		SimpleAudioEngine::getInstance()->playEffect(szKeyFile);
	}
	_touchedKeys.clear();
}
