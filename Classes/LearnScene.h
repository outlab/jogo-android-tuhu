/*
 * LearnScene.h
 *
 *  Created on: 2015. 12. 13.
 *      Author: Jimmy
 */

#ifndef LEARNSCENE_H_
#define LEARNSCENE_H_

#include "common.h"

class LearnScene : public Layer {
private:
    Size _screenSize;
    float _margin;

    Node* _score;

    Vector<Node*> _scores;
    Vector<Node*> _scoresText;
    Node* _scoreEffect;

    Node* _boy;
    cocostudio::timeline::ActionTimeline* _boyAction;
public:
	LearnScene();
	virtual ~LearnScene();

public:
	virtual bool init();

	static Scene* scene();

	CREATE_FUNC(LearnScene);

	void afterPlay(float dt);

	void onButtonTouch(Ref* pSender, Widget::TouchEventType type);

    virtual bool onTouchBegan(Touch *touch, Event *unused_event);
    virtual void onTouchMoved(Touch *touch, Event *unused_event);
    virtual void onTouchEnded(Touch *touch, Event *unused_event);
};

#endif /* LEARNSCENE_H_ */
