/*
 * LearnScene.cpp
 *
 *  Created on: 2015. 12. 13.
 *      Author: Jimmy
 */

#include "LearnScene.h"
#include "MenuScene.h"

LearnScene::LearnScene() {
	// TODO Auto-generated constructor stub
	_margin = 20.0f;
	_score = nullptr;
	_scoreEffect = nullptr;
	_boyAction = nullptr;
	_boy = nullptr;
}

LearnScene::~LearnScene() {
	// TODO Auto-generated destructor stub
}

Scene* LearnScene::scene() {
	Scene *scene = Scene::create();

	LearnScene *layer = LearnScene::create();
	scene->addChild(layer);

	return scene;
}

bool LearnScene::init() {
	if(!Layer::init())
		return false;

	EventDispatcher* dispatcher = Director::getInstance()->getEventDispatcher();
	auto positionListener = EventListenerTouchOneByOne::create();
	positionListener->setSwallowTouches(true);
	positionListener->onTouchBegan = CC_CALLBACK_2(LearnScene::onTouchBegan, this);
	positionListener->onTouchMoved = CC_CALLBACK_2(LearnScene::onTouchMoved, this);
	positionListener->onTouchEnded = CC_CALLBACK_2(LearnScene::onTouchEnded, this);
	dispatcher->addEventListenerWithSceneGraphPriority(positionListener, this);

	_screenSize = Director::getInstance()->getWinSize();

	auto penta = CSLoader::createNode("Penta/Penta.csb");
	_score = penta->getChildByTag(140);
	_score->setPosition(Vec2(_screenSize.width * 0.5f, _screenSize.height * 0.5f));
	this->addChild(_score, 0);

	auto btnBack = (Button*)penta->getChildByTag(159);
	btnBack->setPosition(Vec2(_margin + btnBack->getContentSize().width * 0.5f, _screenSize.height - btnBack->getContentSize().height * 0.5f - _margin));
	btnBack->addTouchEventListener(CC_CALLBACK_2(LearnScene::onButtonTouch, this));
	this->addChild(btnBack, 1);

	_scores.clear();
	_scores.pushBack(_score->getChildByTag(141));
	_scores.pushBack(_score->getChildByTag(144));
	_scores.pushBack(_score->getChildByTag(147));
	_scores.pushBack(_score->getChildByTag(149));
	_scores.pushBack(_score->getChildByTag(151));
	_scores.pushBack(_score->getChildByTag(153));
	_scores.pushBack(_score->getChildByTag(155));
	_scores.pushBack(_score->getChildByTag(157));

	for(Node* score : _scores) {
		score->getChildByName("over")->setVisible(false);
	}

	for(int i = 164; i <= 171; i++) {
		Node* scoreText = penta->getChildByTag(i);
		scoreText->setPosition(Vec2(_margin * 20 + scoreText->getContentSize().width * 0.5f, _screenSize.height - scoreText->getContentSize().height * 0.5f - _margin));
		this->addChild(scoreText, 2);
		scoreText->setVisible(false);
		_scoresText.pushBack(scoreText);
	}
	_scoreEffect = penta->getChildByTag(172);
	_scoreEffect->setPosition(_scoresText.at(0)->getPosition());
	_scoreEffect->setVisible(false);
	this->addChild(_scoreEffect, 3);

	auto tuhu = CSLoader::createNode("Penta/Tuhu.csb");
	_boy = tuhu->getChildByTag(114);
	_boy->setPosition(_screenSize.width - _boy->getContentSize().width, _boy->getContentSize().height * 0.5f);
	_boyAction = CSLoader::createTimeline("Penta/Tuhu.csb");
	_boy->runAction(_boyAction);
	_boyAction->gotoFrameAndPlay(0, 120, true);
	this->addChild(_boy, 3);

	for(int i = 149; i <= 155; i++) {
		_boy->getChildByTag(i)->setVisible(false);
	}

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();

	return true;
}
void LearnScene::onButtonTouch(Ref* pSender, Widget::TouchEventType type) {
	SimpleAudioEngine::getInstance()->playEffect("Effect/voltar.ogg");
	auto pScene = MenuScene::scene();
	Director::getInstance()->replaceScene(pScene);
}

bool LearnScene::onTouchBegan(Touch* touch, Event* unused_event) {
	return true;
}
void LearnScene::onTouchMoved(Touch* touch, Event* unused_event) {
}
void LearnScene::onTouchEnded(Touch* touch, Event* unused_event) {
	Vec2 pt = _score->convertToNodeSpace(touch->getLocation());
	Node* touchedScore = nullptr;
	int iIndex = 0;
	for(Node* score : _scores) {
		if(score->getBoundingBox().containsPoint(pt)) {
			touchedScore = score;
			break;
		}
		iIndex++;
	}

	if(touchedScore == nullptr) return;

	auto scoreOver = touchedScore->getChildByName("over");
	scoreOver->setVisible(true);
	scoreOver->runAction(Sequence::create(
			FadeIn::create(0.0f),
			FadeOut::create(1.0f),
			nullptr
			));

	auto scoreText = _scoresText.at(iIndex);
	scoreText->setVisible(true);
	scoreText->runAction(Sequence::create(
			FadeIn::create(0.5f),
			FadeOut::create(0.5f),
			nullptr
			));

	_scoreEffect->setVisible(true);
	_scoreEffect->runAction(Sequence::create(
			FadeIn::create(0.0f),
			ScaleTo::create(0.0f, 1.0f),
			Spawn::create(
					ScaleTo::create(1.0f, 1.2f),
					FadeOut::create(1.0f),
					nullptr
			),
			nullptr
			));

	char szSoundName[32], szPianoSound[32];
	const char* szScore = touchedScore->getName().c_str();
	memset(szSoundName, 0, sizeof(char) * 32);
	memset(szPianoSound, 0, sizeof(char) * 32);
	sprintf(szSoundName, "Tuhu/%s.ogg", szScore);
	sprintf(szPianoSound, "%s.ogg", szScore);

	if(strstr(szScore, "do_2")) {
		_boy->getChildByTag(149)->setVisible(true);
	} else {
		_boy->getChildByName(szScore)->setVisible(true);
	}

	_boyAction->gotoFrameAndPlay(120, 180, false);

	SimpleAudioEngine::getInstance()->playEffect(szSoundName);
	SimpleAudioEngine::getInstance()->playEffect(szPianoSound);

	this->unschedule(schedule_selector(LearnScene::afterPlay));
	this->schedule(schedule_selector(LearnScene::afterPlay), 1.5f);
}

void LearnScene::afterPlay(float dt) {
	this->unschedule(schedule_selector(LearnScene::afterPlay));
	for(int i = 149; i <= 155; i++) {
		_boy->getChildByTag(i)->setVisible(false);
	}
	_boyAction->gotoFrameAndPlay(0, 120, true);
}
